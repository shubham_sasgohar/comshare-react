import axios from 'axios';
import { push } from 'react-router-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { GET_VOUCHERLISTING_SUCCESS, GET_VOUCHERLISTING_FAILURE, SET_NOTIFICATION, UNSET_NOTIFICATION,
        GET_COUPONREDEEMED_SUCCESS, GET_COUPONREDEEMED_FAILURE} from './types';
import APP from '../config';


// Notification 
export const setNotification = Notification => {
    return {
        type: SET_NOTIFICATION,
        Notification
    };
};

export const get_voucherlisting_success = voucherList => ({

        type: GET_VOUCHERLISTING_SUCCESS,
        payload: {voucherList}
    });

export const get_voucherlisting_failure = error => ({
        type: GET_VOUCHERLISTING_FAILURE,
        payload: {error}
    });

export function voucherList(data=null) {
    return dispatch => {
        axios.post(`${APP.API_URL}/voucher/getvouchers`,data)
                .then(res => {
                    dispatch(get_voucherlisting_success(res.data));
                })
                .catch(error => {
                    dispatch(get_voucherlisting_failure(error));
                });
    }
}

export function couponRedeemed(data) {

    return dispatch => {

        return axios.post(`${APP.API_URL}/voucher/redeemvoucher`, data)
                .then(res => {
                    return res;
                }).catch(error => {
            return error;
        });
    }

}

export const get_couponredeemed_success = getCouponRedeemed => ({

        type: GET_COUPONREDEEMED_SUCCESS,
        payload: {getCouponRedeemed}
    });

export const get_couponredemed_failure = error => ({
        type: GET_COUPONREDEEMED_FAILURE,
        payload: {error}
    });

export function getCouponRedeemed(data) {
    
    return dispatch => {
       return axios.post(`${APP.API_URL}/voucher/getCouponRedeemed`,data)
                .then(res => {
                    dispatch(get_couponredeemed_success(res.data));
                    return res.data
//                    dispatch(push('/vendor-details'))
                    //this.props.router.push('/vendor-details');
                })
                .catch(error => {
                    dispatch(get_couponredemed_failure(error));
                     return error
                });
    }
}
export function changeRedeemStatus(data) {
    
    return dispatch => {
       return axios.post(`${APP.API_URL}/voucher/changeRedeemStatus`,data)
                .then(res => {
                    return res.data
                })
                .catch(error => {
                     return error
                });
    }
}

export function SavePayment(data) {
    
    return dispatch => {
       return axios.post(`${APP.API_URL}/voucher/SavePayment`,data)
                .then(res => {
                    return res.data
                })
                .catch(error => {
                     return error
                });
    }
}