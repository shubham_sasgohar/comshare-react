import axios from 'axios';
import { push } from 'react-router-redux';

import APP from '../config';
import { SET_POSTED_JOBS, SET_APPLIED_JOBS, SET_AWARDED_JOBS, SET_COMPLETED_JOBS, SET_CLIENT_COMPLETED_JOBS } from './types';


export function setPostJobs(jobs) {
  return {
    type: SET_POSTED_JOBS,
    jobs
  };
};

export function setAppliedJobs(jobs) {
  return {
    type: SET_APPLIED_JOBS,
    jobs
  };
};

export function setAwardedJobs(jobs) {
  return {
    type: SET_AWARDED_JOBS,
    jobs
  }
}

export function setCompletedJobs(jobs) {
  return {
    type: SET_COMPLETED_JOBS,
    jobs
  }
}

export function setClientCompletedJobs(jobs) {
  return {
    type: SET_CLIENT_COMPLETED_JOBS,
    jobs
  }
}


// Posted Job
export const getPostedJob = (data) => {
  return dispatch => {
    return axios.post(`${APP.API_URL}/job/getPostedJob`, { id: data }).then(
      res => {
        dispatch(setPostJobs(res.data));
        return res;
      },
      err => err
    );
  };
};

// Applied Job
export const getAppliedJob = (data) => {
  return dispatch => {
    return axios.post(`${APP.API_URL}/job/getAppliedJob`, { id: data}).then(
      res => {
        dispatch(setAppliedJobs(res.data));
        return res;
      },
      err => err
    );
  };
};


// Awarded Jobs
export function getAwardedJob(data) {
  return dispatch => {
    return axios.post(`${APP.API_URL}/job/getAwardedJob`, { id: data}).then(
      res => {
        dispatch(setAwardedJobs(res.data));
        return res;
      },
      err => err
    )
  }
}

// Completed Jobs
export function getUserCompletedJob(data) {
  return dispatch => {
    return axios.post(`${APP.API_URL}/job/getUserCompletedJob`, { id: data}).then(
      res => {
        dispatch(setCompletedJobs(res.data));
        return res;
      },
      err => err
    )
  }
}

export function getClientCompletedJob(data) {
  return dispatch => {
    return axios.post(`${APP.API_URL}/job/getClientCompletedJob`, { id: data}).then(
      res => {
        dispatch(setClientCompletedJobs(res.data));
        return res;
      },
      err => err
    )
  }
}