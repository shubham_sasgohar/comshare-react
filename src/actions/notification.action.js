import axios from 'axios';

import { UNSET_NOTIFICATION, SET_USER_NOTIFICATION } from './types';
import APP from '../config';

export const unsetNotification = () => {
  return {
    type: UNSET_NOTIFICATION
  };
};

export const setNotify = (Notify) => {
  return {
    type: SET_USER_NOTIFICATION, 
    Notify
  };
};

//Unset Notification
export const UnsetNotification = () => {
	return dispatch => {	
		dispatch(unsetNotification());
	}
}

// User notify
export const getUserNotification = (id) => {
  return dispatch => {
    return axios.post(`${APP.API_URL}/getNotification`, { id }).then(
      res => {
        dispatch(setNotify(res.data));
        return res;
      },
      err => err
    )
  }
}

