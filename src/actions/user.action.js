import axios from 'axios';
import { push } from 'react-router-redux';

import { SET_LOGIN, SET_NOTIFICATION, UNSET_NOTIFICATION, CREATE_USER, GET_ZIP_KEYWORD_SUCCESS, GET_ZIP_KEYWORD_FAILURE
        , GET_USER_REVIEW_SUCCESS, GET_USER_REVIEW_FAILURE, GET_CMSPAGES_SUCCESS, GET_CMSPAGES_FAILURE} from './types';
import APP from '../config';

// Set Loggedin user
export function setLogin(user) {
    return {
        type: SET_LOGIN,
        user
    };
}

// Set User
export const setUser = user => {
    return {
        type: CREATE_USER,
        user
    };
};

// Notification 
export const setNotification = Notification => {
    return {
        type: SET_NOTIFICATION,
        Notification
    };
};

// User Login
export function login(username, password, query_string) {
    return dispatch => {
        axios.post(`${APP.API_URL}/users/signin`, {username, password})
                .then(res => {
                    if (res.data.error == true) {
                        dispatch(setNotification({
                            type: "error",
                            message: res.data.message
                        }));
                    } else {
                        let token = localStorage.setItem('token', JSON.stringify(res.data.token));
                        dispatch(setLogin(res.data));
                        dispatch(setNotification({
                            type: "success",
                            message: "Login Successfully.."
                        }));


                        if (query_string == 'messages') {
                            setTimeout(() => {
                                //dispatch(push('/messages'))
                                window.location="/messages";
                            }, 700);

                        } else {
                            setTimeout(() => {
                                //dispatch(push('/services'))
                                window.location="/services";
                            }, 700);

                        }

                    }
                })
                .catch(error => {
                    dispatch(setNotification({
                        type: "error",
                        message: error.response
                    }));
                });
    }
}

// User signup
export const signup = (user) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/users/signup`, user)
                .then(res => {
                    if (res.data.error == true) {
                        if (res.data.message.email) {
                            dispatch(setNotification({
                                type: "error",
                                message: res.data.message.email
                            }));
                        } else if (res.data.message.username) {
                            dispatch(setNotification({
                                type: "error",
                                message: res.data.message.username
                            }));
                        } else {
                            dispatch(setNotification({
                                type: "error",
                                message: res.data
                            }));
                        }
                    } else {
                        
                        
                        dispatch(setUser(res.data));
                        dispatch(setNotification({
                            type: "success",
                            message: "User created Successfully.."
                        }));
                        
                        setTimeout(() => {
                                //dispatch(push('/final'));
                                window.location="/final";
                        }, 500);
                    }
                })
                .catch(error => {
                    dispatch(setNotification({
                        type: "error",
                        message: "Oops something went wrong!"
                    }));
                });
    };
};

export const usernameCheck = (data) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/user/checkUsername`, data)
                .then(res => {
                    if (res.data.error === true) {
                        return true;
                    } else if (res.data.error === false) {
                        return false;
                    }
                })
                .catch(err => {
                    console.log(err);
                });
    }
}

// Get current user details
export const getUserByToken = (token) => {
    return dispatch => {
        return axios.get(`${APP.API_URL}/me/token?token=` + token)
                .then(res => {
                    if (res.data.token == 'token_expired') {
                        window.location = 'logout'
                    } else {
                        dispatch(setLogin(res.data));
                        return res;
                    }
                });
    };
};

// user profile update
export const profileUpdate = (data) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/user/update`, data)
                .then(res => {
                    dispatch(setLogin(res.data));
                    dispatch(setNotification({
                        type: "success",
                        message: "Profile Updated Successfully!"
                    }));
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// User pic upload
export const profilePicUpload = (data) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/user/profileAvatar`, data)
                .then(res => {
                    dispatch(setLogin(res.data));
                    dispatch(setNotification({
                        type: "success",
                        message: "Profile Picture Uploaded Successfully!"
                    }));
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

export const logout = (data) => {
    return dispatch => {
        dispatch(setLogin(''));
    }
}

// Forget Password
export const forgetPassword = (data) => {
    return dispatch => {
        return axios.post(`${APP.API_URL}/user/forgetpassword`, data).then(
                res => res,
                err => err
        );
    };
}

// Reset Password
export const resetPassword = (data) => {
    return dispatch => {
        return axios.post(`${APP.API_URL}/user/resetpassword`, data).then(
                res => res,
                err => err
        );
    };
}

// Verify Email
export const verifyEmail = (data) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/verify?email=${data.email}&code=${data.code}`)
                .then(res => {
                    if (res.data.code == 200) {
                        dispatch(setNotification({
                            type: "success",
                            message: "Email Verified Successfully!"
                        }));

                        setTimeout(() => {

                            let token = localStorage.setItem('token', JSON.stringify(res.data.token));
                            dispatch(setLogin(res.data));
                            dispatch(setNotification({
                                type: "success",
                                message: "Login Successfully.."
                            }));
//                            dispatch(push('/login'));
//                            dispatch(push('/profile'))
                            window.location="/profile";
                        }, 1500);

                    } else if (res.data.code == 401) {
                        dispatch(setNotification({
                            type: "success",
                            message: "Email Already Verified!"
                        }));

                        setTimeout(() => {
                            window.location="/login";
                           // dispatch(push('/login'));
                        }, 1500);
                    } else {
                        dispatch(setNotification({
                            type: "info",
                            message: res.data.message
                        }));
                    }
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// Get user Info by id
export const getUserInfo = (data) => {
    return dispatch => {
        axios.post(`${APP.API_URL}/getUserByID/${data.id}`)
                .then(res => {
                    console.log(res);
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

//Get zipcode and keywords
export const get_zip_keyword_success = zipKeywordList => ({

        type: GET_ZIP_KEYWORD_SUCCESS,
        payload: {zipKeywordList}
    });

export const get_zip_keyword_failure = error => ({
        type: GET_ZIP_KEYWORD_FAILURE,
        payload: {error}
    });

export function zipKeywordList() {
    return dispatch => {
        return axios.post(`${APP.API_URL}/getzip_keywords`)
                .then(res => {
                    dispatch(get_zip_keyword_success(res.data));
                    return res.data
                })
                .catch(error => {
                    dispatch(get_zip_keyword_failure(error));
                });
    }
}
//Get GET_USER_REVIEW_SUCCESS
export const get_user_review_success = getUserReview => ({

        type: GET_USER_REVIEW_SUCCESS,
        payload: {getUserReview}
    });

export const get_user_review_failure = error => ({
        type: GET_USER_REVIEW_FAILURE,
        payload: {error}
    });

export function getUserReview(id) {
    var data = {
        id: id
    }
    return dispatch => {
        return axios.post(`${APP.API_URL}/getUserReviews`, data)
                .then(res => {
                    dispatch(get_user_review_success(res.data));
                    return res.data
                })
                .catch(error => {
                    dispatch(get_user_review_failure(error));
                });
    }
}

/*For CMS Pages*/

//Get GET_CMSPAGES_SUCCESS
export const get_cmspages_success = getCmsPageData => ({

        type: GET_CMSPAGES_SUCCESS,
        payload: {getCmsPageData}
    });

export const get_cmspages_failure = error => ({
        type: GET_CMSPAGES_FAILURE,
        payload: {error}
    });

export function getCmsPageData(id) {

    return dispatch => {
        return axios.post(`${APP.API_URL}/getCmsPageData`)
                .then(res => {
                    dispatch(get_cmspages_success(res.data));
                    return res.data
                })
                .catch(error => {
                    dispatch(get_cmspages_failure(error));
                });
    }
}