import axios from 'axios';
import { push } from 'react-router-redux';

import { SET_LISTING, SET_LIST_ITEM, SET_APPLY_JOB, GET_HEADER_NOTIFICATION_SUCCESS, GET_HEADER_NOTIFICATION_FAILURE ,SET_HEADER_READMESSAGE_SUCCESS} from './types';
import APP from '../config';


export function setListing(list) {
    return {
        type: SET_LISTING,
        list
    };
}

export function setList(item) {
    return {
        type: SET_LIST_ITEM,
        selected_item: item
    };
}

export function setApplyJob(data) {
    return {
        type: SET_APPLY_JOB,
        applyJob: data
    }
}

// Create Listing
export function createList(data) {
    return dispatch => {
        axios.post(`${APP.API_URL}/list/createList`, data)
                .then(res => {
                    console.log(res);
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// Get all listing
export function getListing() {
    return dispatch => {
        return axios.get(`${APP.API_URL}/list/getAll`)
                .then(res => {
                    console.log(res)
                    console.log(res.data.lists)
                    dispatch(setListing(res.data.lists));
                    return res;
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// Set List 
export const setListItem = (item) => {
    return dispatch => {
        dispatch(setList(item));
    }
}

// get filtered result
export const getFilteredList = (type) => {

    return dispatch => {
        axios.get(`${APP.API_URL}/list/getByType/${type}`)
                .then(res => {
                    dispatch(setListing(res.data.lists));
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// get search result
export const getSearchResult = (query, type) => {
    return dispatch => {
        return axios.get(`${APP.API_URL}/list/getByQuery?query=${query}&type=${type}`)
                .then(res => {
                    dispatch(setListing(res.data.lists));
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

export const getSearchResult2 = (query) => {
    return dispatch => {
        return axios.get(`${APP.API_URL}/list/getByQueryOnly?query=${query}`)
                .then(res => {
                    console.log('res')
                    console.log(res)
                    console.log('res')
                    dispatch(setListing(res.data.lists));
                })
                .catch(error => {
                    console.log(error);
                });
    }
}

// filter by 
export const filterBy = (change, type, query) => {
    return dispatch => {
        if (change && type === '' && query === '') {
            console.log('action 1');
            axios.get(`${APP.API_URL}/list/getAllByFilter/${change}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
                    .catch(error => {
                        console.log(error);
                    });
        } else if (change && type && query === '') {
            console.log('action 2');
            axios.get(`${APP.API_URL}/list/getAllByFilterType?change=${change}&type=${type}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
                    .catch(error => {
                        console.log(error);
                    });
        } else if (change && type && query) {
            console.log('action 3');
            axios.get(`${APP.API_URL}/list/getAllByFilterTypeQuery?change=${change}&type=${type}&query=${query}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
                    .catch(error => {
                        console.log(error);
                    });
        } else if (change && type == '' && query) {
            console.log('action 4');
            axios.get(`${APP.API_URL}/list/getAllByFilterTypeQuery?change=${change}&query=${query}&type=${type}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
                    .catch(error => {
                        console.log(error);
                    });
        }
    }
}

// filter by keywords
export const filterBykeyword = (keywords, type, query, filterBy) => {
    console.log(keywords)
    console.log(type)
    console.log(query)
    console.log(filterBy)
    console.log('filterBykeyword')
    return dispatch => {
        if (keywords && type === '' && query === '' && filterBy === '') {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (keywords && type && query === '' && filterBy === '') {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&type=${type}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (keywords && type && query && filterBy === '') {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&type=${type}&query=${query}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (keywords && type && query && filterBy) {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&type=${type}&query=${query}&filterBy=${filterBy}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (keywords && type == '' && query == '' && filterBy) {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&type=${type}&query=${query}&filterBy=${filterBy}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (keywords && type && query == '' && filterBy) {
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&type=${type}&query=${query}&filterBy=${filterBy}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        }
        /*Add new Filters*/
        else if (type == '' && query && filterBy && keywords) {
            console.log('here');
            axios.get(`${APP.API_URL}/list/getAllByKeywords?keywords=${keywords}&query=${query}&filterBy=${filterBy}`)
                    .then(res => {
                        console.log(res)
                        dispatch(setListing(res.data.lists));
                    })
        }
    }
}

// filter by rating
export const filterByRating = (type, query, filterBy, keywords, rating) => {
    console.log('filterByRating')
    return dispatch => {
        if (type && query && filterBy == '' && keywords == '' && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?type=${type}&query=${query}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type && query && filterBy && keywords == '' && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?type=${type}&query=${query}&filterBy=${filterBy}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type && query && filterBy && keywords && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?type=${type}&query=${query}&filterBy=${filterBy}&keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type == '' && query && filterBy == '' && keywords == '' && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?query=${query}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type && query == '' && filterBy && keywords && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?type=${type}&filterBy=${filterBy}&keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type && query == '' && filterBy && keywords == '' && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?type=${type}&filterBy=${filterBy}&keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type == '' && query == '' && filterBy && keywords && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?filterBy=${filterBy}&keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        } else if (type == '' && query == '' && filterBy == '' && keywords && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        }
        /*Add new filters*/
        else if (type == '' && query && filterBy && keywords && rating) {
            axios.get(`${APP.API_URL}/list/getAllByRating?query=${query}&filterBy=${filterBy}&keywords=${keywords}&rating=${rating}`)
                    .then(res => {
                        dispatch(setListing(res.data.lists));
                    })
        }

    }
}




///*** All In One Filter  ***///
export const filterListBy = (type, query, filterBy, keywords, rating) => {
    return dispatch => {
        dispatch(getListing()).then(
                res => {
                    console.log('==' + type)
                    console.log(query)
                    console.log(filterBy)
                    console.log(keywords)
                    console.log(rating)
                    let data = [];
                    console.log(res.data.lists)
                    res.data.lists.map((list, index) => {
                        // #1 Only one
                        if (type && query === '' && filterBy === '' && keywords === '' && rating === '') { // If only type
                            console.log('filterListBy 1')
                            if (list.type === type) {
                                data.push(list);
                            }
                        } else if (type === '' && query && filterBy === '' && keywords === '' && rating === '') { // If only query
                            console.log('filterListBy 2')
                            let searchQuery = query.toLowerCase();
                            if (list.pincode.indexOf(',') > -1) {
                                var str = list.pincode.split(',');

                                str.forEach((elem) => {
                                    if (elem === searchQuery) {
                                        data.push(list);
                                    }
                                });
                            }
                            //if (list.title === query || list.pincode === query) {//change this line for match string keyword for all
                            let title = list.title.toLowerCase();
                            if (title.match(searchQuery) != null || list.pincode === query) {
                                data.push(list);
                            }
                        } else if (type === '' && query === '' && filterBy === '' && keywords === '' && rating) { // If only rating
                            console.log('filterListBy 3')
                            if (list.userinfo.rating >= rating) {
                                data.push(list);
                            }
                        } else if (type && query === '' && filterBy === '' && keywords === '' && rating) { // If type and rating
                            console.log('filterListBy 4')
                            console.log(list.type + '===>' + type);
                            console.log(list.userinfo.rating + '===>' + rating);
                            //if (list.userinfo.rating === rating && list.type === type) {// change code for rating in less than to user rating
                            if (list.userinfo.rating >= rating && list.type === type) {
                                data.push(list);
                            }
                        } else if (type && query && filterBy === '' && keywords === '' && rating === '') { // If type and query
                            console.log('filterListBy 5')
                            console.log(list.type + '===>' + type)
                            /*Zipcode searching functionality disable*/
                            if (list.type === type) {
                                let searchQuery = query.toLowerCase();
                                let title = list.title.toLowerCase();
                                var list_pincode= list.pincode
                                if (title.match(searchQuery) != null || list_pincode.match(searchQuery) != null) {
                                    data.push(list); 
                                }
                                /* if (list.pincode.indexOf(',') > -1) {
                                 var str = list.pincode.split(',');
                                 
                                 str.forEach((elem) => {
                                 if (elem === query) {
                                 data.push(list);
                                 }
                                 });
                                 }
                                 console.log(data)
                                 if (list.pincode === query) {
                                 data.push(list);
                                 }*/
                            }
                        } else if (type && query === '' && filterBy === '' && keywords && rating) { // If type , keyword and rating
                            console.log('filterListBy 6');
                            var keyw = list.keywords;
                            keyw.forEach((elem) => {
                                if (elem.value === keywords && list.userinfo.rating >= rating && list.type === type) {
                                    data.push(list);
                                }
                            });
                        } else if (type && query && filterBy === '' && keywords && rating) { // If type ,query, keyword and rating
                            console.log('filterListBy 7');
                            var keyw = list.keywords;
                            let searchQuery = query.toLowerCase();
                            let title = list.title.toLowerCase();
                            keyw.forEach((elem) => {
                                if (elem.value === keywords && list.userinfo.rating >= rating && list.type === type && (title.match(searchQuery) != null) || list.pincode === query) {
                                    data.push(list);
                                }
                            });
                        } else if (type == '' && query && filterBy === '' && keywords && rating == '') { // If query , keyword
                            console.log('filterListBy 8');
                            var keyw = list.keywords;
                            let searchQuery = query.toLowerCase();
                            let title = list.title.toLowerCase();
                            keyw.forEach((elem) => {
                                if (elem.value === keywords && (title.match(searchQuery) != null || list.pincode === query)) {
                                    data.push(list);
                                }
                            });
                        } else if (type == '' && query && filterBy === '' && keywords && rating == '') { // If query , keyword and date
                            console.log('filterListBy 9');
                            var keyw = list.keywords;
                            let searchQuery = query.toLowerCase();
                            let title = list.title.toLowerCase();
                            keyw.forEach((elem) => {
                                if (elem.value === keywords && (title.match(searchQuery) != null || list.pincode === query)) {
                                    data.push(list);
                                }
                            });
                        } else if (type == '' && query && filterBy === '' && keywords && rating) { // If query , keyword and rating
                            console.log('filterListBy 10');
                            var keyw = list.keywords;
                            let searchQuery = query.toLowerCase();
                            let title = list.title.toLowerCase();
                            keyw.forEach((elem) => {
                                if (elem.value === keywords && (title.match(searchQuery) != null || list.pincode === query) && list.userinfo.rating >= rating) {
                                    data.push(list);
                                }
                            });
                        } else if (type && query && filterBy === '' && keywords && rating === '') { // if type ,query, keyword
                            console.log('filterListBy 11');
                            var keyw = list.keywords;
                            let searchQuery = query.toLowerCase();
                            let title = list.title.toLowerCase();
                            console.log(query)
                            console.log(list.pincode)
                            console.log(list.type)
                            console.log(type)
                            console.log(list.title)
                            keyw.forEach((elem) => {
                                if (list.type === type && elem.value === keywords && (title.match(searchQuery) != null || list.pincode === query)) {
                                    data.push(list);
                                }
                            });
                        }
                    });
                    if (data.length > 0) {
                        console.log('filterListBy 12')
                        console.log(data)
                        dispatch(setListing(data));
                    } else {
                        /*If data length is zero then no record founds*/
                        dispatch(setListing(data));
                    }
                }
        )
    }
}

// Apply Job
export function applyJob(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/list/applyJob`, data).then(
                res => res,
                err => err
        );
    }
}

// Applied User details
export function getAppliedUserDetail(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/getUserByID?id=${data.id}`).then(
                res => res,
                err => err
        );
    }
}

// Accept User 
export function notifyAction(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/notifyAction`, data).then(
                res => res,
                err => err
        );
    }
}

// Set Applied Job
export function getAppliedJob(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/getApplyStatus`, {id: data.userId, jobId: data.jobId}).then(
                res => {
                    dispatch(setApplyJob(res.data));
                    return res;
                },
                err => err
        )
    }
}

// Hire User 
export function hireUser(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/list/hireUser`, {userId: data.userid, jobId: data.jobid}).then(
                res => res,
                err => err
        );
    }
}

// Close Job
export function closeJob(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/list/closejob`,data).then(
                res => res,
                err => err
        );
    }
}

//GET_HEADER_NOTIFICATION_SUCCESS

export const get_header_notification_success = getHeaderNotifiaction => ({
    
    type:GET_HEADER_NOTIFICATION_SUCCESS,
    payload: {getHeaderNotifiaction}
});

export const get_header_notification_failure = error => ({
   type: GET_HEADER_NOTIFICATION_FAILURE,
   payload:{error}
});

export function getHeaderNotification(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/getHeaderNotification`, data)
                .then(res => {
//                    console.log(res)
                    dispatch(get_header_notification_success(res.data));
                    return res.data
                })
                .catch(error => {
                    dispatch(get_header_notification_failure(error));
        });
    }
}
export function setHeaderReadMessage(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/setHeaderReadMessage`, data)
                .then(res => {
                    return res.data
                })
                .catch(error => {
        });
    }
}

export function setHeaderCountReadMessage(data) {
    return dispatch => {
        return axios.post(`${APP.API_URL}/setHeaderCountReadMessage`, data)
                .then(res => {
                    return res.data
                })
                .catch(error => {
        });
    }
}