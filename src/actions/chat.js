import axios from 'axios';
import { push } from 'react-router-redux';
import APP from '../config';

import {
    FETCH_GETMESSAGES_BEGIN, FETCH_GETMESSAGES_SUCCESS,FETCH_GETMESSAGES_FAILURE,
    FETCH_UNREADMSGCOUNT_SUCCESS,FETCH_UNREADMSGCOUNT_FAILURE,
    SET_MESSAGESTATUS_SUCCESS,SET_MESSAGESTATUS_FAILURE,

} from '../actions/types';
// Save Messages
export function saveMsg(data) {
    return dispatch => { 
      axios.post(`${APP.API_URL}/chat/savemsg`, data)
        .then(res => {
          
        })
        .catch(error => {
           console.log(error);
        });
    }
}



//fetch_getmessages_begin
export const fetch_getmessages_begin = () => ({
  type: FETCH_GETMESSAGES_BEGIN
});

export const fetch_getmessages_success = getMsg => ({

  type:    FETCH_GETMESSAGES_SUCCESS,
  payload: { getMsg }
});

export const fetch_getmessages_failure = error => ({
  type:    FETCH_GETMESSAGES_FAILURE,
  payload: { error }
});


// get Messages

export function getMsg(data) {
    return dispatch => { 
        dispatch(fetch_getmessages_begin());
        axios.post(`${APP.API_URL}/chat/getmessages`,data)
          .then(res => {
             dispatch(fetch_getmessages_success(res.data));
//             return res.data;
          })
          .catch(error => {
             console.log(error);
              dispatch(fetch_getmessages_failure(error));
          });
    }
}


//getUnreadMsgCount
export const fetch_unreadmsgcount_success = getUnreadMsgCount => ({
  type:    FETCH_UNREADMSGCOUNT_SUCCESS,
  payload: { getUnreadMsgCount }
});

export const fetch_unreadmsgcount_failure = error => ({
  type:    FETCH_UNREADMSGCOUNT_FAILURE,
  payload: { error }
});
export function getUnreadMsgCount(data) {
    return dispatch => { 
         axios.post(`${APP.API_URL}/chat/getunreadmsgcount`,data)
          .then(res => {
				dispatch(fetch_unreadmsgcount_success(res.data));
          });
       
    }
}


//SET_MESSAGESTATUS_SUCCESS
export const set_messagestatus_success = setMessageStatus => ({

  type:    SET_MESSAGESTATUS_SUCCESS,
  payload: { setMessageStatus }
});

export const set_messagestatus_failure = error => ({
  type:    SET_MESSAGESTATUS_FAILURE,
  payload: { error }
});
export function setMessageStatus(msg_data) {
    return dispatch => { 
        axios.post(`${APP.API_URL}/chat/setMessageStatus`,msg_data)
          .then(res => {
             dispatch(set_messagestatus_success(res.data));
//             return res.data;
          })
          .catch(error => {
             console.log(error);
              dispatch(set_messagestatus_failure(error));
          });
    }
}


