import React, { Component } from 'react';
import { Provider } from 'react-redux';

import store from './store';
import router from './router';
import translations from './translations';
import { IntlReducer as Intl, IntlProvider } from 'react-redux-multilingual';

class App extends Component {
  render() {
    return (
      <Provider store={ store } >
           <IntlProvider translations={translations} locale='en'>
                { router }
            </IntlProvider>
      </Provider>
    );
  }
}

export default App;
