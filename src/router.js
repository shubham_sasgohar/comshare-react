import React from 'react';
import { Router,
         Route,
         browserHistory } from 'react-router';

import Home from './components/Home';
import Login from './components/auth/login';
import Register from './components/auth/register';
import Profile from './components/dashboard/profile';
import Logout from './components/auth/logout';
import Final from './components/auth/final';
import Verify from './components/auth/verify';
import Search from './components/dashboard/search';
import Searchjob from './components/dashboard/searchjob';

import Listing from './components/dashboard/listing';
import Profileview from './components/dashboard/profileview';
import Messages from './components/dashboard/messages';
import MyJob from './components/dashboard/myjob';
import MyPostedJob from './components/dashboard/mypostedjob';
import MyCompletedJob from './components/dashboard/mycompletedjob';
import AboutUs from './components/dashboard/aboutus';
import Faq from './components/dashboard/faq';
import Myteam from './components/dashboard/myteam';
import howitworks from './components/dashboard/howitworks';
import Contactus from './components/dashboard/contactus';
import Terms from './components/dashboard/terms';
import Privacy from './components/dashboard/privacy';
import Vouchers from './components/dashboard/vouchers';
import couponcode from './components/dashboard/couponcode';
import vendordetails from './components/dashboard/vendordetails';

const router = (
  <Router history={ browserHistory } >
    <Route path='/' component={ Home } />
    <Route path='/login' component={ Login } />
    <Route path='/register' component={ Register } />
    <Route path='/final' component={ Final } />
    <Route path='/verify' component={ Verify } />
    <Route path='/profile' component={ Profile } />
    <Route path='/logout' component={ Logout } />
    <Route path='/userprofile' component={ Profileview} />

    <Route path='/search' component={ Search } />
    <Route path='/searchjob' component={ Searchjob } />
    <Route path='/services' component={ Listing } />
    <Route path='/messages' component={ Messages } />
    <Route path='/myjobs' component={ MyJob } />
    <Route path='/mypostedjobs' component={ MyPostedJob } />
    <Route path='/mycompletedjobs' component={ MyCompletedJob } />
    <Route path='/vouchers' component={ Vouchers } />
    <Route path='/vendor' component={ couponcode } />
    <Route path='/vendor-details' component={ vendordetails } />
    <Route path='/:pagetype' component={ howitworks } />
  </Router>
);

export default router;
