import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';

import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'react-notifications/lib/notifications.css';


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
