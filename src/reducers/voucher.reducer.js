import {
GET_VOUCHERLISTING_SUCCESS, GET_VOUCHERLISTING_FAILURE,GET_COUPONREDEEMED_SUCCESS, GET_COUPONREDEEMED_FAILURE
} from '../actions/types';

const initialState = {
    voucherList: [],
    getCouponRedeemed: [],
    error: null
}

export default function voucherReducer(state = initialState, action) {

    switch (action.type) {
        
        case GET_VOUCHERLISTING_SUCCESS:

            return {
                ...state,
                voucherList : action.payload.voucherList
            };
            
        case GET_VOUCHERLISTING_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                voucherList : []
            };
            
        case GET_COUPONREDEEMED_SUCCESS:

            return {
                ...state,
                getCouponRedeemed : action.payload.getCouponRedeemed
            };
            
        case GET_VOUCHERLISTING_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                getCouponRedeemed : []
            };
            
        default:
            // ALWAYS have a default case in a reducer
        return state;

    }

};