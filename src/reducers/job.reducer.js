import { SET_POSTED_JOBS } from "../actions/types";


const initialState = {
    data: []
};

const setPostedJob = (state, action) => {
    return { ...state, data: action.jobs };
};

const jobReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_POSTED_JOBS: return setPostedJob(state, action);
      default: return state;
    }
};
  
export default jobReducer;