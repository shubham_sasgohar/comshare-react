import { SET_LISTING, SET_LIST_ITEM ,GET_HEADER_NOTIFICATION_SUCCESS,GET_HEADER_NOTIFICATION_FAILURE} from '../actions/types';

const initialState = {
    data: [],
    selected_item: [],
    header_notification:[]
};

const setList = (state, action) => {
    return { ...state, data: action.list};
};

const setListItem = (state, action) => {
    return { ...state, selected_item: action.selected_item }
};

const listingReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_LISTING: return setList(state, action);
      case SET_LIST_ITEM: return setListItem(state, action);
      case GET_HEADER_NOTIFICATION_SUCCESS:
            return {
                ...state,
                header_notification: action.payload.getHeaderNotifiaction
            }
      case GET_HEADER_NOTIFICATION_SUCCESS:
            return {
                ...state,
                error: action.payload.error,
                header_notification: []
            }
      
      default: return state;
    }
};


  
export default listingReducer;