export const SET_FORGET = 'SET_FORGET';

const initialState = {
    isForget: []
};

const setForget = (state, action) => {
    return { ...state, isLogin: action.user };
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_FORGET: return setForget(state, action);
      default: return state;
    }
};
  
export default usersReducer;