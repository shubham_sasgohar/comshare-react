import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import notification from './notificationReducer';
import userReducer  from "./user.reducer";

import listingReducer from "./listing.reducer";
import { IntlReducer as Intl, IntlProvider } from 'react-redux-multilingual'
import chatReducer from "./chat.reducer";
import voucherReducer from "./voucher.reducer";

const rootReducer = combineReducers({
  routing: routerReducer,
  notification,
  userReducer,
  listingReducer,
  chatReducer,
  voucherReducer,
  Intl
});

export default rootReducer;