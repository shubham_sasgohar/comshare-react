import { SET_LOGIN, CREATE_USER, GET_ZIP_KEYWORD_SUCCESS, GET_ZIP_KEYWORD_FAILURE ,GET_USER_REVIEW_SUCCESS,GET_USER_REVIEW_FAILURE,GET_CMSPAGES_SUCCESS,GET_CMSPAGES_FAILURE} from '../actions/types';

const initialState = {
    isLogin: '',
    zipKeywordList: [],
    user_reviews: [],
    error: null
};

const setUser = (state, action) => {
    return {...state, isLogin: action.user};
};

const createUser = (state, action) => {
    return {...state, isUser: action.user};
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LOGIN:
            return setUser(state, action);
        case CREATE_USER:
            return createUser(state, action);
        case GET_ZIP_KEYWORD_SUCCESS:
            return {
                ...state,
                zipKeywordList: action.payload.zipKeywordList
            }

        case GET_ZIP_KEYWORD_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                zipKeywordList: []
            }
            
        case GET_USER_REVIEW_SUCCESS:
            return {
                ...state,
                user_reviews: action.payload.getUserReview
            }
        case GET_USER_REVIEW_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                user_reviews: []
            }
            
        case GET_CMSPAGES_SUCCESS:
            return{
                ...state,
                cms_page_data : action.payload.getCmsPageData
            }
        case GET_USER_REVIEW_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                cms_page_data: []
            }
        default:
            return state;
}
};

export default userReducer;