import {
    FETCH_GETMESSAGES_BEGIN, FETCH_GETMESSAGES_SUCCESS,FETCH_GETMESSAGES_FAILURE,
    FETCH_UNREADMSGCOUNT_SUCCESS,FETCH_UNREADMSGCOUNT_FAILURE,
    SET_MESSAGESTATUS_SUCCESS,SET_MESSAGESTATUS_FAILURE,
    } from '../actions/types';
    
const initialState = {
    getMsg: [],
    error: null
};


export default function chatReducer (state = initialState, action){
    switch (action.type) {
        
        case FETCH_GETMESSAGES_BEGIN:
            return {
                ...state,
                error: null,
            };
            
        case FETCH_GETMESSAGES_SUCCESS:
            
//            console.log('reducer here')
//            console.log(action.payload.getMsg)
            return {
                ...state,
                getMsg : action.payload.getMsg
            };
            
        case FETCH_GETMESSAGES_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                getMsg : []
            };
            
        case FETCH_UNREADMSGCOUNT_SUCCESS:

            return {
                ...state,
                unreadMsgCount : action.payload.getUnreadMsgCount
            };
            
        case FETCH_UNREADMSGCOUNT_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                unreadMsgCount : []
            };
            
        case SET_MESSAGESTATUS_SUCCESS:

            return {
                ...state,
                msg_status : action.payload.setMessageStatus
            };
            
        case SET_MESSAGESTATUS_FAILURE:
            return {
                ...state,
                error: action.payload.error,
                msg_status : []
            };
            
        default:
            // ALWAYS have a default case in a reducer
        return state;
    }
};

//export default chatReducer;