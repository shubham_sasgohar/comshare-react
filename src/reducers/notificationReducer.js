import { SET_NOTIFICATION, UNSET_NOTIFICATION, SET_USER_NOTIFICATION } from '../actions/types';

const initialState = {
  Notification: [],
  Notify: []
};

const setNotification = (state, action) => {
  return { ...state, Notification: action.Notification };
};

const unsetNotification = (state, action) => {
  return { ...state, Notification: [] };
};

const setNotify = (state, action) => {
  return { ...state, Notify: action.Notify }
}

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_NOTIFICATION: return setNotification(state, action);
    case UNSET_NOTIFICATION: return unsetNotification(state, action);
    case SET_USER_NOTIFICATION: return setNotify(state, action)
    default: return state;
  }
};
export default notificationReducer;
