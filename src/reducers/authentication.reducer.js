import { SET_LOGIN } from '../actions/types';

const initialState = {
    isLogin: []
};

const setUser = (state, action) => {
    return { ...state, isLogin: action.user };
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_LOGIN: return setUser(state, action);
      default: return state;
    }
};
  
export default userReducer;