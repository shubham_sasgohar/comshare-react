import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';
import axios from 'axios';
import APP from '../../config';
import { push } from 'react-router-redux';
import  { Redirect } from 'react-router-dom';
import { Link } from 'react-router';


class LoginPopup extends Component {
    
    constructor (props){
        super(props);
        this.state = {
            username: '',
            password: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSignin = this.onSignin.bind(this);
    }
    
  
     
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
       
    }
    
    onSignin(e) {
        e.preventDefault();
        const { username, password } = this.state;

        if(username, password) {
            axios.post(`${APP.API_URL}/users/signin`, { username, password })
            .then(res => {
                if(res.data.error == true) {
                    NotificationManager.error(res.data.message,'',3000);
                } else {
//                    NotificationManager.success("Login Successfully..",'',3000);
                    localStorage.setItem('token', JSON.stringify(res.data.token));
                    window.location.reload();
                    
                }
            })
            .catch(error => {
                console.log(error);
            });
        } else {
            NotificationManager.error('Please enter username & password!','',3000);
        }
    }
    render() { 
        const { 
            username, 
            password } = this.state;
        return(
                <div className="modal fade" id="login-pop">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                              <h4 className="modal-title">LOGIN</h4>
                              <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                            </div>
                            <div className="modal-body">
                               <div>
                                    <p>Please enter your Login details to continue</p>
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            name="username" 
                                            placeholder="Username/Email address" 
                                            value={ username }
                                            onChange={ this.onChange }/>
                                        </div>
                                        <div className="form-group">
                                        <input 
                                            type="password" 
                                            name="password" 
                                            placeholder="Password" 
                                            value={ password }
                                            onChange={ this.onChange }/>
                                        </div>
                                        <div className="form-group btns">
                                        <button onClick={this.onSignin} className="lgn-btn">LOGIN</button>
                                        <Link to="/register" className="register-link">REGISTER</Link>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            )
    }
}

const mapStateToProps = store => {
	return {
        user: store.userReducer,
        notification: store.notification
	};
};

export default connect(mapStateToProps)(LoginPopup);
