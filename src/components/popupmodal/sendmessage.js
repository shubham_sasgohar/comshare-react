import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';



class SendMessage extends Component {
    
    constructor (props){
        super(props)
    }
    
    render() { 
        return(
                <div className="modal fade" id="msg-pop">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                              <h4 className="modal-title">Write a message</h4>
                              <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                            </div>
                            <div className="modal-body">
                              <form action="/action_page.php">
                                <div className="form-group">
                                  <textarea
                                    className="form-control"
                                    rows={5}
                                    id="comment1"
                                    placeholder="Your message" 
                                    defaultValue={""}
                                    onChange = {this.props.onChangepopupSendMsg}
                                    />
                                </div> 
                                <div className="continue-links">
                                    <button type="button" data-dismiss="modal" onClick={ this.props.popupSendMsg } className="lgn-btn" data-toggle="modal" data-target="#registration">SEND MESSAGE</button>
                                </div>
                              </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            )
    }
}

const mapStateToProps = store => {
	return {
        user: store.userReducer,
        notification: store.notification
	};
};

export default connect(mapStateToProps)(SendMessage);
