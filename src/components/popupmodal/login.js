import React, { Component } from 'react';
import { connect } from 'react-redux';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';


class LoginModal extends Component {
    constructor (props) {
        super(props);
    }

 	render() { 
        return (
            <div className="modal fade" id="login-modal">
                <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                    <h4 className="modal-title">Login </h4>
                    <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok"/></button>
                    </div>
                    
                    <div className="modal-body">
                        <form >
                            <div className="form-group">
                                <input 
                                    type="email" 
                                    name="email" 
                                    placeholder="Email address" 
                                    className="form-control" />
                            </div>
                            <div className="form-group">
                                <input 
                                    type="password" 
                                    name="pswd" 
                                    placeholder="Password" 
                                    className="form-control" />
                            </div>
                            <div className="form-group btns">
                                <button className="lgn-btn">LOGIN</button>
                            </div>
                        </form>
                    </div>        
                </div>
                </div>
            </div>
        )
  	}
}

const mapStateToProps = store => {
	return {
        user: store.userReducer,
        notification: store.notification
	};
};

export default connect(mapStateToProps)(LoginModal);
