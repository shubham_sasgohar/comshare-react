import React, { Component } from 'react';
import { withRouter, hashHistory } from 'react-router';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Select from 'react-select';
import APP from '../../../config';

import 'react-select/dist/react-select.css';
import 'react-datepicker/dist/react-datepicker.css';

import { signup, getUserByToken, setLogin ,zipKeywordList} from '../../../actions/user.action';
import { UnsetNotification } from '../../../actions/notification.action';

import store from '../../../store';
import axios from 'axios';

class CreateListModal extends Component {
    
    constructor (props) {
        super(props);

        this.state = {
            step: 1,
            title: '',
            desp: '',
            multi: true,
            options: [],
            pinOptions: [],
            keywords: '',
            keyword: '',
            price: '',
            pincodes: '',
            pincode: '',
            email: '',
            firstname: '',
            lastname: '',
            zipcode: '',
            username: '',
            password: '',
            confirmPassword: '',
            dueDate: moment(),
            isLoading: false,
            logonname: '',
            logonpwd: '',
            type: "HELP",
            from: ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleDate = this.handleDate.bind(this);
        this.handleSigin = this.handleSigin.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.onSignin = this.onSignin.bind(this);
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnPinChange = this.handleOnPinChange.bind(this);
    }

    componentDidMount() {
        var pin_option_array=[];
        var pin_option_keywords=[];
        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token));
        }
        store.dispatch(zipKeywordList()).then(res => {
            
            if(res){
                 res.data.map(function(item){
                if(item.type=='zipcode'){
                    pin_option_array.push({'value':item.value,'label':item.value})
                }else{
                    pin_option_keywords.push({'value':item.value,'label':item.value})
                }
            })
            this.setState({pinOptions: pin_option_array})
            this.setState({options: pin_option_keywords})
            }
           
        });
    }

    componentWillReceiveProps(nextProps) { 
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
              NotificationManager.info(nextProps.notification.Notification.message,'',3000);
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        const { step, 
                title, 
                desp, 
                keywords, 
                price, 
                pincode, 
                dueDate, 
                email, 
                firstname, 
                lastname, 
                zipcode, 
                username, 
                password, 
                confirmPassword, 
                logonname, 
                logonpwd, 
                doRegister } = this.state;
       
        if(title === '') {
            NotificationManager.error('Please Enter title ','',3000);
			return;
        }

        if(desp === '') {
            NotificationManager.error('Please Enter description ','',3000);
			return;
        }

        if(keywords === '') {
            NotificationManager.error('Please Enter keywords','',3000);
			return;
        }

        if(price === '') {
            NotificationManager.error('Please Enter price ','',3000);
			return;
        }
		
        if(dueDate === null) {
                NotificationManager.error('Please Enter dueDate ','',3000);
                return;
        }
       

        if(title && desp && keywords && !localStorage.getItem('token')) {
            this.setState({
                step: 2
            });
        }

        if(!localStorage.getItem('token')) {

            if(email === ''){
                NotificationManager.info('Please Enter email ','',3000);
                return;
            } 
            
            if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
                
                NotificationManager.error('Please Enter valid email ','',3000);
                return;
            }

            if(firstname === '') {
                NotificationManager.error('Please Enter First Name ','',3000);
                return;
            }
    
            
            if(zipcode === ''){
                NotificationManager.error('Please Enter Zipcode ','',3000);
                return;
            }
            
            if(username === ''){
                
                NotificationManager.error('Please Enter username ','',3000);
                return;
            } 
            

            if(password === ''){
                
                NotificationManager.error('Please Enter Password ','',3000);
                return;
            }

            if(confirmPassword === ''){
                
                NotificationManager.error('Please Confirm Password ','',3000);
                return;
            }

            if(password !== confirmPassword) {
                
                NotificationManager.error('Password and Confirm password not match', '', 3000);
                return;
            }
            
            if(email && username && password && step ==2) {
                let user = {
                    email,
                    username,
                    password,
                    firstname,
                    lastname,
                    zipcode
                }

                axios.post(`${APP.API_URL}/users/signup`, user)
                    .then(res => {
                        if(res.data.error == true) {
                            if(res.data.message.email) {
                                NotificationManager.error(res.data.message.email,'',3000);
                            } else if(res.data.message.username) {
                                NotificationManager.error(res.data.message.username,'',3000);
                            } else {
                                NotificationManager.error(res.data,'',3000);
                            }
                        } else {
                            NotificationManager.success("User created Successfully..",'',3000);
                            let userData = res.data;
                            this.setState({
                                from: 'register'
                            });
                            this.createList(userData.user, this.state);

                        }
                    })
                    .catch(error => {
                        NotificationManager.error("Oops something went wrong!",'',3000);
                    });
            } 
        } else {
          if(this.props.user.isLogin.user) {
            let user = this.props.user.isLogin.user;
            
            this.createList(user, this.state);
          }
        }

    }

    onSignin(e) {
        e.preventDefault();
        
        const { username, password } = this.state;


        if(username, password) {
            axios.post(`${APP.API_URL}/users/signin`, { username, password })
            .then(res => {
                if(res.data.error == true) {
                    NotificationManager.error(res.data.message,'',3000);
                } else {
                    localStorage.setItem('token', JSON.stringify(res.data.token));
                    NotificationManager.success("Login Successfully..",'',3000);
                    
                    this.createList(res.data.user, this.state);
                }
            })
            .catch(error => {
                console.log(error);
            });
        } else {
            NotificationManager.error('Please enter username & password!','',3000);
        }
    }
	
    // Create Listing
    createList(user, data) {

        this.setState({ isLoading: true });

        let list = {
            userId: user._id,
            title: data.title,
            description: data.desp,
            keywords: data.keywords,
            pincode: data.pincodes,
			price: data.price,
            dueDate: data.dueDate._d,
            type: this.state.type
        }

        axios.post(`${APP.API_URL}/list/createList`, list)
            .then(res => {

                if(res && this.state.from === "register") {
                    
                    setTimeout(() => {
                         window.location="/final";
                    }, 700);
                    //store.dispatch(push('/final'));
                    //this.setState({ isLoading: false, step: 3 });
                } else {
                    
                    setTimeout(() => {
                        window.location="/services";
                    }, 700);
                    //this.setState({ isLoading: false, step: 3});
                    //store.dispatch(push('/services'));
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleDate(date) {
        this.setState({
            dueDate: date
        });
    }

    handleSigin(e) {
        this.setState({
            step: 4
        });
    }

    handleOnChange (value) {
		const { multi, keywords, keyword } = this.state;
		
		if (multi) {
			this.setState({ keywords: value });
		} else {
			this.setState({ keyword: value });
		}
    }
    
    handleOnPinChange (value) {
        const { multi, pincodes, pincode } = this.state;

        let data = [];
        value.forEach(element => {
            data.push(element.value);
        });
		
		if (multi) {
			this.setState({ pincodes: data });
		} else {
			this.setState({ pincode: data });
		}
    }
	
	handleBackStep(step) {
		if(step == 2 || step === 3) {
			let currentStep = step;
			let newStep = step - 1;
			
			this.setState({
				step: newStep
			});
		}
		
		if(step == 4) {
			let currentStep = step;
			let newStep = step - 2;
			
			this.setState({
				step: newStep
			});
		}
	}


    resetForm(e) {
        this.setState({
            step: 1,
            title: '',
            desp: '',
            keywords: '',
            keyword: '',
            price: '',
            pincodes: '',
            pincode: '',
            email: '',
            firstname: '',
            lastname: '',
            zipcode: '',
            username: '',
            password: '',
            confirmPassword: '',
            dueDate: moment(),
            isLoading: false,
            type: "HELP"
        });
    }

  

 	render() { 
            
        const minDate = new Date(Date.now());
        const { step, 
                title, 
                desp, 
                multi, 
                options, 
                keyword, 
                keywords, 
                price, 
                pinOptions,
                pincodes,
                pincode, 
                dueDate, 
                email, 
                firstname, 
                lastname, 
                zipOptions,
                zipcodes,
                zipcode, 
                username, 
                password, 
                confirmPassword, 
                isLoading, 
                logonname, 
                logonpwd,
                } = this.state;
		return (
			<div>
                <NotificationContainer/>
				<div className="modal fade" id="create-listing">
                    <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            { step == 1  &&
                                <h4 className="modal-title">
                                    Create Listing - <span>Step 1 of 3</span>
                                </h4>
                            }
                            { step == 2  &&
                                <h4 className="modal-title">
                                    Registration - <span>Step 2 of 3</span>
                                </h4>
                            }
                            { step == 3 &&
                                <h4 className="modal-title">
                                    Recheck - <span>Step 3 of 3</span>
                                </h4>
                            }
                            { step == 4 &&
                                <h4 className="modal-title">
                                    LOGIN
                                </h4>
                            }
                            <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok"/></button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={this.onSubmit}>
                            { step == 1 &&
                                <div>
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            className="form-control" 
                                            id="title" 
                                            placeholder="Title*" 
                                            name="title"
                                            value={title} 
                                            onChange={this.onChange}/>
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div>
                                    <div className="form-group">
                                        <textarea 
                                            className="form-control" 
                                            rows="5" 
                                            id="desp" 
                                            placeholder="Description**" 
                                            name="desp" 
                                            value={desp} 
                                            onChange={this.onChange}></textarea>
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div> 
                                    <div className="form-group">
                                        <Select
                                            multi={multi}
                                            options={options}
                                            onChange={this.handleOnChange}
                                            value={multi ? keywords : keyword}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <input 
                                            type="number" 
                                            className="form-control" 
                                            id="price" 
                                            placeholder="price (Willing to pay)*" 
                                            name="price"
                                            value={ price }
                                            onChange={ this.onChange }/>
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div>	
                                    <div className="date-picker">
                                    <div className="form-group">
                                        <Select
                                            multi={multi}
                                            options={pinOptions}
                                            onChange={this.handleOnPinChange}
                                            value={multi ? pincodes : pincode}
                                        />
                                    </div>
                                        <div className="form-group">
                                            <DatePicker
                                            selected={this.state.dueDate}
                                            onChange={this.handleDate}
                                            placeholderText="Enter Due Date"
                                            minDate={minDate}
                                            name="dueDate" />
                                            <i className="fa fa-calendar"></i>	
                                        </div>
                                    </div>	
                                </div>
                            }

                            { step == 2 &&
                                <div>
                                    <div className="form-group">
                                        <input 
                                            type="email" 
                                            name="email" 
                                            placeholder="Email address" 
                                            id="email" 
                                            className="form-control"
                                            value={email}
                                            onChange={this.onChange} />
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div>
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            name="firstname" 
                                            placeholder="First Name" 
                                            id="firstname" 
                                            className="form-control"
                                            value={firstname}
                                            onChange={this.onChange} />
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div>
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            name="lastname" 
                                            placeholder="Last Name" 
                                            id="lastname" 
                                            className="form-control"
                                            value={lastname}
                                            onChange={this.onChange} />
                                        <span className="pull-right text-danger"><small></small></span>
                                    </div>
                                   <div className="form-group">
                                         <select name="zipcode" onChange={this.onChange}>
                                        <option value="">Please choose a zipcode</option>
                                        {
                                            this.props.user.zipKeywordList.data !=undefined &&
                                                this.props.user.zipKeywordList.data.map((data,index)=> {
                                                    if(data.type=='zipcode'){
                                                        return(
                                                            <option key={index} value={data.value}>{data.value}</option>
                                                        )
                                                    }
                                            })
                                        }   
                                        </select>
                                    </div>	
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            name="username" 
                                            placeholder="Username" 
                                            id="username" 
                                            className="form-control" 
                                            value={username}
                                            onChange={this.onChange}/>
                                    </div>		
                                    <div className="form-group">
                                        <input 
                                            type="password" 
                                            name="password" 
                                            placeholder="Password" 
                                            id="password" 
                                            className="form-control" 
                                            value={password}
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="form-group">
                                        <input 
                                            type="password" 
                                            name="confirmPassword" 
                                            placeholder="Confirm password" 
                                            id="confirmPassword" 
                                            className="form-control" 
                                            value={this.confirmPassword}
                                            onChange={this.onChange}/>
                                    </div>

                                    <p className="already-user">Already a customer? <a onClick={ this.handleSigin }> Sign In</a></p>	
                                </div>
                            }

                            { step == 3 &&
                                <div className="center">
                                    <img src="images/recheck-tick.png" alt="Recheck" width="50" />
                                    <p>Add more listing or search listing</p>
                                    <div className="continue-links">
                                        <a onClick={ this.resetForm } >create new offer</a>
                                    </div>
                                </div>
                            } 

                            { step == 4 &&
                                 
                                <div>
                                    <p>Please enter your Login details to continue</p> 
                                    <div className="form-group">
                                        <input 
                                            type="text" 
                                            name="username" 
                                            placeholder="Username/Email address" 
                                            id="username" 
                                            value={ username }
                                            onChange={ this.onChange }/>
                                        </div>
                                        <div className="form-group">
                                        <input 
                                            type="password" 
                                            name="password" 
                                            placeholder="Password" 
                                            id="password" 
                                            value={ password }
                                            onChange={ this.onChange }/>
                                        </div>
                                        <div className="form-group btns">
                                        <button onClick={this.onSignin} className="lgn-btn">LOGIN</button>
                                    </div>
                                </div>   
                            }
							
                            <div className="continue-links">
                                <div>
                                    <a  hidden={ this.state.step == 1 }  onClick={ this.handleBackStep.bind(this, this.state.step) }> <img src="images/arrow-left.png" alt="path ok"/> BACK </a>&nbsp;
                                    <a  onClick={this.onSubmit} hidden={ this.state.step == 3 || this.state.step == 4} > Continue <img src="images/arrow-right.png" alt="path ok"/></a>
                                    {isLoading &&
                                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </div>
                            </div>
                            </form>
                        </div>        
                        </div>
                    </div>
                </div>
			</div>
		);
  	}
}

const mapStateToProps = store => {
	return {
        user: store.userReducer,
        notification: store.notification
	};
};

export default connect(mapStateToProps)(CreateListModal);
