import React, { Component } from 'react';
import { connect } from 'react-redux';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';


class ApplyJob extends Component {
    constructor (props) {
        super(props);
    }

 	render() { 
        return (
            <div class="modal fade show" id="accept-pop" style="display: block; padding-right: 17px;">
                <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Accept Service</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="images/cross.png" alt="path ok" />
                        </button>
                    </div>
                    
                    <div class="modal-body">
                    <h2 class="pop-h">Are you sure to accept service ?</h2>
                    <p class="pop-p"><img src="images/coin.png" alt="Cost" />  Not enough coins </p> 
                    <form action="/action_page.php">
                            <div class="form-group forms-btn btns">
                                <button type="button" class="lgn-btn" data-toggle="modal" data-target="#buy-coin">BUY COINS</button>
                            </div>
                        </form>
                    </div>        
                </div>
                </div>
            </div>
        )
  	}
}

const mapStateToProps = store => {
	return {
        user: store.userReducer,
        notification: store.notification
	};
};

export default connect(mapStateToProps)(LoginModal);
