import React, { Component } from "react";
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import PropTypes from 'prop-types';
import store from '../../store';
import { login } from '../../actions/user.action';
import { UnsetNotification } from '../../actions/notification.action';
import ForgetModal  from './forgetModal';
import { push } from 'react-router-redux';

import Header from '../Header';
import Footer from '../Footer';


class Login extends Component {
    constructor(props,context) {
        super(props);
        this.context=context;
        this.state = {
            username: '',
            password: '',
            submitted: false
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

	 
    componentWillMount() {
        var query_string = this.props.location.query.redirect!=undefined ? this.props.location.query.redirect : '' ;
            let token = JSON.parse(localStorage.getItem('token'));

            if(token && query_string == "messages") {
                console.log(query_string)
			this.props.router.push('messages');
        }	
    }

    componentWillReceiveProps(nextProps) { 
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                break;
              default:
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }
    

    onSubmit(e) { 
      e.preventDefault();
      var query_string = this.props.location.query.redirect!=undefined ? this.props.location.query.redirect : '' ;
      this.setState({ submitted: true });
      const { username, password } = this.state;
    
      if(username && password) {
        store.dispatch(login(username, password,query_string));
      } else {
        NotificationManager.error('Please enter username & password!','',3000);
        return;
      }
    }

    onChange(e) {
      this.setState({ [e.target.name]: e.target.value });
    }


    render() {
        const { username, password, submitted } = this.state;
        return (
            <div className="main">
            <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">{this.context.translate('Home')}</Link>
                            <span className="breadcrumb-item active">{this.context.translate('Login')}</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content rating-main">
                    <div className="container login-inner">
                        <div className="login-main">
                            <div className="logo-main">
                                <Link to=""><img src="images/logo.png" alt="logo" /></Link>
                            </div>
                            <h1>{this.context.translate('Login')}</h1>
                            <p>{this.context.translate('Please enter your Login details to continue')}</p>
                            <NotificationContainer/>                            
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                <input 
                                    type="text" 
                                    name="username" 
                                    placeholder={this.context.translate('Username / Email Address')}
                                    id="username" 
                                    className={'form-control' + (submitted && !username ? ' is-invalid' : '')}
                                    value={username}
                                    onChange={this.onChange} />
                                    {submitted && !username &&
                                        <div className="invalid-feedback">Username is required</div>
                                    }
                                </div>
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="password" 
                                    placeholder={this.context.translate('Password')}
                                    id="pwd" 
                                    className={'form-control' + (submitted && !password ? ' is-invalid' : '')}
                                    value={password}
                                    onChange={this.onChange}/>
                                    {submitted && !password &&
                                        <div className="invalid-feedback">Password is required</div>
                                    }
                                </div>
                                <div className="form-group btns">
                                <button type="submit" className="lgn-btn">{this.context.translate('LOGIN')}</button>
                                <Link to="/register" className="register-link">{this.context.translate('REGISTER')}</Link>
                                <Link className="frgt-pswd"  data-toggle="modal" data-target="#reset">{this.context.translate('Forgot password')}?</Link>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
                <ForgetModal />
            <Footer />
            </div>
        );
    }
}


const mapStateToProps = store => {
    return {
        user: store.userReducer,
        locale: store.locale,
        notification: store.notification
    };
};
Login.contextTypes = {
  translate: PropTypes.func 
}
export default connect(mapStateToProps)(Login);