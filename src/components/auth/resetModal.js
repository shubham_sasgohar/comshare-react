import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { resetPassword } from '../../actions/user.action';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';

import { UnsetNotification } from '../../actions/notification.action';

class ResetModal extends Component {
    
        constructor (props) {
            super(props);
            
            this.state = {
                id: '',
                password: '',
                newPassword: '',
                confirmNewPassword: '',
                submitted: false,
                isLoading: false
            }

            this.onSubmit = this.onSubmit.bind(this);
            this.onChange = this.onChange.bind(this);
            this.reloadForm = this.reloadForm.bind(this);
		}

		componentWillReceiveProps(nextProps) { 
            if(nextProps.user.isLogin) {
                this.setState({
                    id: nextProps.user.isLogin.user._id || ''
                });
            }

            if(nextProps.notification.Notification.hasOwnProperty('type')){
                switch (nextProps.notification.Notification.type) {
                  case 'info':
                    NotificationManager.info('Info message');
                    break;
                  case 'success':
                    NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                    break;
                  case 'warning':
                    NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                    break;
                  case 'error':
                    NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                    break;
                }
                store.dispatch(UnsetNotification()); 
            }
		}
	
		
		onSubmit(e) {
			e.preventDefault();
            const { password, newPassword, confirmNewPassword, submitted, isLoading } = this.state;
            
            if(password === ''){
                NotificationManager.error('Please Enter Old Password ','',3000);
                return;
            }
            
            if(newPassword === ''){ 
                NotificationManager.error('Please Enter New Password ','',3000);
                return;
            }

            if(confirmNewPassword === ''){
                NotificationManager.error('Please Enter Confirm Password ','',3000);
                return;
            }
    
            if(newPassword !== confirmNewPassword) {
                NotificationManager.error('New Password and Confirm password not match', '', 3000);
                return;
            }

            if(password && newPassword) {
                this.setState({
                    isLoading: true
                });

                store.dispatch(resetPassword(this.state))
                    .then(res => {
                        if(res.data.error === true) {
                            NotificationManager.error(res.data.message, '', 3000);
                            this.setState({
                                isLoading: false
                            })
                        } else if(res.data.error === false) {
                            NotificationManager.success(res.data.message, '', 3000);
                            this.setState({
                                submitted: true,
                                isLoading: false
                            });
                        } else {
                            NotificationManager.error("Something went wrong try again later!", '', 3000);
                        }
                    });
            }
		} 

		onChange(e) {
			this.setState({ [e.target.name ]: e.target.value });
        }
        
        reloadForm(e) {
			this.setState({
				submitted: false
			});
		}
  
  render() {
		const { password, newPassword, confirmNewPassword, submitted, isLoading } = this.state;
        return (
            <div>
                <div className="modal fade" id="resetpassword">
                    <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Reset password</h4>
                            <button 
                                type="button" 
                                className="close" 
                                data-dismiss="modal"
                                onClick={ this.reloadForm }><img src="images/cross.png" alt="path ok"/></button>
                        </div>
                        { !submitted && 
                        <div className="modal-body">
                            <h2 className="pop-h">RESET YOUR PASSWORD</h2>
                            <p className="pop-p">In the fields below, enter your new password</p>
                            <form onSubmit={this.onSubmit} >
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="password" 
                                    placeholder="Old password" 
                                    className="form-control" 
                                    value={password}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="newPassword" 
                                    placeholder="New password" 
                                    className="form-control" 
                                    value={newPassword}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="confirmNewPassword" 
                                    placeholder="Confirm New password"  
                                    className="form-control"
                                    value={confirmNewPassword}
                                    onChange={this.onChange} />
                                </div>	
                                <div className="form-group forms-btn btns">
                                    <button type="submit" className="lgn-btn">RESET PASSWORD</button>
                                </div>
                                { isLoading &&
                                    <div>
                                        <p>
                                            Please wait... 
                                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                        </p> 
                                    </div>
                                }
                            </form>
                        </div>      
                        }  

                        { submitted && 
                            <div className="modal-body">
                            <h2 className="pop-h">Thank You</h2>
                            <p className="pop-p">Your password is reset successfully!</p>
                            </div>      
                        }
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = store => {
	return {
			user: store.userReducer,
			notification: store.notification
	};
};

export default connect(mapStateToProps)(ResetModal);
