import React, { Component } from "react";
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import { UnsetNotification } from '../../actions/notification.action';
import store from '../../store';

import Header from '../Header';
import Footer from '../Footer';


class FinalSignup extends Component {

    componentWillReceiveProps(nextProps) { 
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                break;
              default:
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }
    

   
    render() {
        return (
        <div className="main">
            <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <a className="breadcrumb-item" href="/">Home</a>
                            <span className="breadcrumb-item active">Register</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content rating-main">
                    <div className="container login-inner">
                    
                        <div className="register-main">
                            <h2 className="profile-heading">Verify Account </h2>
                            <p >
                                Please check your email account and verify account!
                            </p>
                            <NotificationContainer/>     
                        </div>
                    </div>
                </section>
            <Footer />
        </div>
        )
    }
}


const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(FinalSignup);