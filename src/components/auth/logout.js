import React, { Component } from 'react';
import { connect } from 'react-redux';

import  store from '../../store';
import { logout } from '../../actions/user.action';

    
class Logout extends Component {

  componentDidMount(){
        localStorage.removeItem('token');
        store.dispatch(logout());
        this.props.router.push('/');
		window.location.reload();
  }

  render() {
    return (
      <h1 className="loading-text">
        Logging out...
      </h1>
    );
  }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Logout);