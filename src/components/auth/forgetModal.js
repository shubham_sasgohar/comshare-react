import React, { Component } from 'react';
import { connect } from 'react-redux';
import { forgetPassword } from '../../actions/user.action';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';

import { UnsetNotification } from '../../actions/notification.action';

class ForgetModal extends Component {
    
    constructor (props) {
				super(props);
				
				this.state = {
					email: '',
					submitted: '',
					isLoading: false,
				}

				this.onSubmit = this.onSubmit.bind(this);
				this.onChange = this.onChange.bind(this);
				this.reloadForm = this.reloadForm.bind(this);
		}

		componentWillReceiveProps(nextProps) {
			if(nextProps.notification.Notification.hasOwnProperty('type')){
				switch (nextProps.notification.Notification.type) {
				  case 'info':
					NotificationManager.info('Info message');
					break;
				  case 'success':
					NotificationManager.success(nextProps.notification.Notification.message,'',3000);
					break;
				  case 'warning':
					NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
					break;
				  case 'error':
					NotificationManager.error(nextProps.notification.Notification.message,'',3000);
					break;
				}
				store.dispatch(UnsetNotification()); 
			}
		}
	
		onSubmit(e) {
			e.preventDefault();
			const { email, submitted, isLoading } = this.state;
			

			if(email === ''){
				NotificationManager.error('Please Enter Registered email ','',3000);
				return;
			} 
			if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
				NotificationManager.error('Please Enter valid email ','',3000);
				return;
			}

			if(email) {
				this.setState({ 
					isLoading: true
				});
				store.dispatch(forgetPassword({ email: email}))
					.then(res => {
						if(res.data.error === true) {
							this.setState({ 
								isLoading: false
							});
							NotificationManager.error('User is not registered with this email!');
						} else if(res.data.error === false) {
							this.setState({
								submitted: true,
								isLoading: false
							});
						}
					});
				
			}

		} 

		onChange(e) {
			this.setState({ [e.target.name ]: e.target.value });
		}

		reloadForm(e) {
			this.setState({
				submitted: false
			});
		}
  
 	render() {
		const { email, submitted, isLoading } = this.state;
		return (
			<div>
				<div className="modal fade" id="reset">
					<div className="modal-dialog modal-dialog-centered">
						<div className="modal-content">
							<div className="modal-header">
								<h4 className="modal-title">Forgot password</h4>
								<button 
									type="button" 
									className="close" 
									data-dismiss="modal"
									onClick={ this.reloadForm }><img src="images/cross.png" alt="path ok" /></button>
							</div>
							<div className="modal-body">
							{ !submitted && 
								<div>
									<h2 className="pop-h">Enter your email</h2>
									<p className="pop-p">In the fields below, enter your registered email address</p>
									<form onSubmit={this.onSubmit}>
										<div className="form-group">
											<input 
												type="email" 
												name="email" 
												placeholder="john@exmple.com"  
												className="form-control" 
												onChange={this.onChange}
												value={email}/>
										</div>
										<div className="form-group forms-btn btns">
											<button type="submit" className="lgn-btn">
												SEND PASSWORD
											</button>
										</div>
										{ isLoading &&
											<div>
												<p>
													Please wait... 
													  <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
												</p> 
											</div>
										}
									</form>
								</div>
							}

							{ submitted && 
								<div>
									<h2 className="pop-h">Thank You</h2>
									<p className="pop-p">Please check your mail</p>
									
								</div>
							}
							</div>        
						</div>
					</div>
				</div>
			</div>
		);
  	}
}

const mapStateToProps = store => {
	return {
			user: store.userReducer,
			notification: store.notification
	};
};

export default connect(mapStateToProps)(ForgetModal);
