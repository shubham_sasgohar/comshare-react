import React, { Component } from "react";
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import store from '../../store';
import { verifyEmail } from '../../actions/user.action';
import { UnsetNotification } from '../../actions/notification.action';

import Header from '../Header';
import Footer from '../Footer';


class Verify extends Component {
    componentDidMount() {
        // console.log(this.props.location.query);
        let email = this.props.location.query.email;
        let code = this.props.location.query.code;

        store.dispatch(verifyEmail({ email, code }));
    }


    componentWillReceiveProps(nextProps) { 
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                break;
              default:
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }


    render() {
        return (
            <div className="main">
            <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" href="index.html">Home</Link>
                            <span className="breadcrumb-item active">Login</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content rating-main">
                    <div className="container login-inner">
                        <div className="login-main">
                            <div className="logo-main">
                                <Link href="index.html"><img src="images/logo.png" alt="Footer US" /></Link>
                            </div>
                            <div>Please wait ...    </div>
                            <NotificationContainer/>                            
                        </div>
                    </div>
                </section>
            <Footer />
            </div>
        );
    }
}


const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Verify);