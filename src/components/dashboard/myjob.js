import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { render } from 'react-dom';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import ReactStars from 'react-stars';
import moment from 'moment';
import store from '../../store';
import { getUserByToken } from '../../actions/user.action';
import { getPostedJob, getAppliedJob, getAwardedJob,getClientCompletedJob ,getUserCompletedJob} from '../../actions/job.action';
import { getAppliedUserDetail, notifyAction, hireUser, closeJob ,applyJob} from '../../actions/listing.action';


import { UnsetNotification } from '../../actions/notification.action';
import ResetModal  from '../auth/resetModal';

import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';

class MyJob extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            imageURL: '',
            firstname: '',
            lastname: '',
            appliedJobs: [],
            awardedJobs: [],
            rating:'',
            job_title:'',
            job_status:'',
            job_description:'',
            client_user:'',
            client_user_email:'',
            client_rating:'',
            client_img:'',
            postedJobs: [],
            userCompletedJobs: [],
            clientCompletedJob: [],
            feedback: '',
            rating1: 0,
            activeChat: '',
            job_id:'',
            display:'',
            coins:'',
            jobcoins:"",
            hireduserid:''
            
        }
        this.showpopup = this.showpopup.bind(this);
        this.closeRequest = this.closeRequest.bind(this);
        this.onChange = this.onChange.bind(this);
        this.setjobid = this.setjobid.bind(this);
        this.onFeedbackSubmit = this.onFeedbackSubmit.bind(this);


    }

    componentDidMount() {
        document.title = "Comshare | MyJob Page";

        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token));
        } else {
            this.props.router.push('/login');
        }
        
        if(this.props.user.isLogin.user) {
            store.dispatch(getAwardedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    this.setState({
                        awardedJobs: res.data
                    });
                }
            );

            store.dispatch(getAppliedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    this.setState({
                        appliedJobs: res.data
                    });
                }
            );
    
            store.dispatch(getPostedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    
                    this.setState({
                        postedJobs: res.data
                    });
                }
            );
    
            store.dispatch(getUserCompletedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    this.setState({
                        userCompletedJobs: res.data
                    });
                }
            );
            store.dispatch(getClientCompletedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    this.setState({
                        clientCompletedJob: res.data
                    });
                }
            );
        } else {
            this.props.router.push('/profile');
        }
    }

    componentDidUpdate(prevProps) {
    }

    componentWillReceiveProps(nextProps) {  
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }

        if(nextProps.user.isLogin) {
            this.setState({
                id: nextProps.user.isLogin.user._id || '',
                imageURL: nextProps.user.isLogin.user.profileImg || 'images/dummy-img.png',
                firstname: nextProps.user.isLogin.user.firstname || '',
                lastname: nextProps.user.isLogin.user.lastname || '',
                rating: nextProps.user.isLogin.user.rating || '',
                coins: nextProps.user.isLogin.user.coins || '',
            });
        }
    }
    showpopup(...data){
        this.setState({
                job_title: data[0],
                job_status: data[1],
                job_description:data[2],
                client_user:data[3],
                client_user_email:data[4],
                client_rating:parseInt(data[5]),
                client_img:data[6]
        });
    }
    
    closeRequest(param_hiredId,param_appliedId,param_jobId,param_job_title,param_applied_username,param_applied_email){
        var msg = param_applied_username+' request to you closed the '+param_job_title+ ' job!';
        let hiredId = param_hiredId;
        let appliedId = param_appliedId;
        let jobId = param_jobId;
        let close_request_status = true;
        let close_job_title = param_job_title;
        
        let close_request_msg = msg;
        let data = {
                hiredId : hiredId,
                appliedId : appliedId,
                jobId : jobId,
                close_request_status:true,
                close_request_msg:close_request_msg,
                username:param_applied_username,
                email:param_applied_email,
                close_job_title:close_job_title,
                call_type: 'close_request'
            }
            
            store.dispatch(applyJob(data))
                .then(res => {
                    if(res) {
                        NotificationManager.success("your request has been submitted!", '', 2000);
                    } 
                });
        
      
    }
    
    onChange(e) {
        this.setState({ 
            feedback: e.target.value 
        });
    }
    ratingChanged(newRating) {
        this.setState({
            rating1: newRating
        });
    }
    
    onFeedbackSubmit(e) {
        
       
         e.preventDefault();
        if(this.state.rating1 === 0) {
            NotificationManager.error("Please provide rating", '', 2000);
        }
        if(this.state.feedback === '') {
            NotificationManager.error("Please provide Feedback", '', 2000);
        }
    
        if(this.state.coins < this.state.jobcoins){
            NotificationManager.error("Please buy more coins to close this job!", '', 2000);
            return false;
        }

        if(this.state.feedback && this.state.rating1) {
            let data = {
                jobId: this.state.job_id,
                rating: this.state.rating1,
                feedback: this.state.feedback,
                current_user_id :this.state.userId,
                current_userfrom_id : this.state.hireduserid,
                jobcoins:this.state.jobcoins
                
            }

            store.dispatch(closeJob(data)).then(
                res => {
                    
                    if(res.data.error === false) {
                        
                        NotificationManager.success("Successfully Closed!", '', 2000);
                        store.dispatch(getAwardedJob(this.props.user.isLogin.user._id)).then(
                            res => {
                                this.setState({
                                    awardedJobs: res.data
                                });
                            }
                        );

                        store.dispatch(getAppliedJob(this.props.user.isLogin.user._id)).then(
                            res => {
                                this.setState({
                                    appliedJobs: res.data
                                });
                            }
                        );

                        store.dispatch(getPostedJob(this.props.user.isLogin.user._id)).then(
                            res => {

                                this.setState({
                                    postedJobs: res.data
                                });
                            }
                        );

                        store.dispatch(getUserCompletedJob(this.props.user.isLogin.user._id)).then(
                            res => {
                                this.setState({
                                    userCompletedJobs: res.data
                                });
                            }
                        );
                        store.dispatch(getClientCompletedJob(this.props.user.isLogin.user._id)).then(
                            res => {
                                this.setState({
                                    clientCompletedJob: res.data
                                });
                            }
                        );
                    this.setState({display:'none'})
                    }
                }
            )
        }
    }
    
//    setjobid = (e)=>{
    setjobid (jobid,jobcoins,userid,hireduserid){
        this.setState({
            job_id:jobid,
            jobcoins:jobcoins,
            userId:userid,
            hireduserid:hireduserid
            
        });
    }

    render() {
        
        let parentObj = this;
        const { firstname, lastname, imageURL, postedJobs, appliedJobs, awardedJobs,userCompletedJobs ,clientCompletedJob,rating,id} = this.state;
        console.log(postedJobs)
        return (
            <div className="main">
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">Home</Link>
                            <span className="breadcrumb-item active">MyJobs</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content">
                    <div className="container">
                        <div className="outer">
                            <h2 className="profile-heading">DASHBOARD   <button  className="mrt5" data-toggle="modal" data-target="#buy-coin">BUY COINS</button><button className="reedme-vou">REDEEM VOUCHERS</button></h2>
                           <ProfileSidebar 
                                image={ imageURL }
                                fName={ firstname }
                                lName={ lastname }
                                rating={ rating }
                                id={ id }
                                />
                            <NotificationContainer/>  
                            <div className="jobs-main">
                                <h5>My Awarded Job</h5>
                                <div className="job-table">
                                        { awardedJobs.length === 0 &&
                                            <div className="center">
                                                You don't have any job yet!
                                            </div>
                                        }
                                    <table className="table">
                                        { awardedJobs.length > 0 &&
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sr. No</th>
                                                    <th scope="col">Job Title</th>
                                                    <th scope="col">Start date</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                        }
                                        <tbody>
                                            {  awardedJobs &&
                                                awardedJobs.map((data, index) => {
                                                    return (
                                                        <tr key={ index }>
                                                            <td scope="row">{ index + 1 }</td>
                                                            <td>{ data.title }</td>
                                                            <td>{  moment(data.dueDate).format('MMMM Do YYYY')  || moment(data.startDate).format('MMMM Do YYYY')  }</td>
                                                            <td>{ data.status }</td>
                                                            <td className="view-button pointer"><span  data-toggle="modal" data-target="#create-job"  onClick={()=>parentObj.showpopup(data.title,data.status,data.description,data.clientinfo.username,data.clientinfo.email,data.clientinfo.rating,data.clientinfo.profileImg)} className="badge badge-success active-badge">Active</span></td>
                                                            <td className="view-button pointer"><span    onClick={()=>parentObj.closeRequest(data.userId,data.hiredUserId,data._id,data.title,data.userinfo.username,data.clientinfo.email)}  className="badge badge-danger active-badge">Close Request</span></td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="jobs-main">
                                <h5>My Applied Job</h5>
                                <div className="job-table">
                                    { appliedJobs.length === 0 &&
                                        <div className="center">
                                            You don't have any job yet!
                                        </div>
                                    }
                                    <table className="table">
                                        { appliedJobs.length > 0 &&
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sr. No</th>
                                                    <th scope="col">Job Title</th>
                                                    <th scope="col">Start date</th>
                                                    <th scope="col">Client</th>
                                                </tr>
                                            </thead>
                                        }
                                       
                                        <tbody>
                                            { appliedJobs && 
                                                appliedJobs.map((data, index) => {
                                                if(data.hired!='hired'){
                                                    return (
                                                        <tr key={ index }>
                                                            <td scope="row">{ index + 1 }</td>
                                                            <td>{ data.jobInfo.title }</td>
                                                            <td>{  moment(data.jobInfo.dueDate).format('MMMM Do YYYY')  || moment(data.jobInfo.startDate).format('MMMM Do YYYY')  }</td>
                                                            <td>{ data.clientInfo.firstname + ' ' + data.clientInfo.lastname }</td>
                                                            <td className="view-button pointer"><span  data-toggle="modal" data-target="#create-job"  onClick={()=>parentObj.showpopup(data.jobInfo.title,data.jobInfo.status,data.jobInfo.description,data.clientInfo.username,data.clientInfo.email,data.clientInfo.rating,data.clientInfo.profileImg)} className="badge badge-success active-badge">Applied</span></td>
                                                        </tr>
                                                    )
                                                }
                                                }) 
                                                
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <div className="jobs-main">
                                <h5>My Posted Job</h5>
                                <div className="job-table">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">Sr. No</th>
                                            <th scope="col">Job Title</th>
                                            <th scope="col">Due date</th>
                                            <th scope="col">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { postedJobs &&
                                                postedJobs.map((data, index) => {
													
                                                    return (
                                                        <tr key={index} hidden={ data.status == "CLOSE" }>
                                                            <td scope="row"> { index + 1 }</td>
                                                            <td>{ data.title }</td>
                                                            <td>{ moment(data.dueDate).format('MMMM Do YYYY') }  </td>
                                                            <td>$ { data.price }</td>
                                                            { data.hiredUserId && 

                                                                <td>
                                                                    <span data-toggle="modal" onClick={()=>parentObj.showpopup(data.title,data.status,data.description,data.userinfo[0].username,data.userinfo[0].email,data.userinfo[0].rating,data.userinfo[0].profileImg)}   data-target="#create-job" className="badge badge-success active-badge pointer  margin4">Active Job</span>
                                                                    <span data-toggle="modal" onClick={()=>parentObj.setjobid(data._id,data.price,data.userId,data.hiredUserId)} data-target="#close-job" className="badge badge-danger active-badge pointer">Close Job</span> 

                                                                </td>
                                                            }
                                                            
                                                            { !data.hiredUserId && data.clientinfo[0].coins >= data.price &&
                                                                    
                                                                <td className="view-button"><span className="badge green active-badge">Open</span></td>
                                                            }
                                                            { !data.hiredUserId && data.clientinfo[0].coins < data.price &&
                                                                    
                                                                <td className="view-button">
                                                                    <span data-toggle="modal"   data-target="#create-job"  data-toggle="modal" data-target="#buy-coin" className="badge badge-success active-badge pointer  margin4">Buy Coin</span>
                                                                </td>
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <div className="jobs-main">
                                <h5>My Completed Jobs</h5>
                                <div className="job-table">
                                        { clientCompletedJob.length === 0 &&
                                            <div className="center">
                                                You don't have any job yet!
                                            </div>
                                        }
                                    <table className="table">
                                        { clientCompletedJob.length > 0 &&
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sr. No</th>
                                                    <th scope="col">Job Title</th>
                                                    <th scope="col">Rating</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                        }
                                        <tbody>
                                            {  clientCompletedJob &&
                                                clientCompletedJob.map((data, index) => {
                                                    return (
                                                        <tr key={ index }>
                                                            <td scope="row">{ index + 1 }</td>
                                                            <td>{ data.title }</td>
                                                            <td>{ data.rating  }</td>
                                                            <td>$ { data.price }</td>
                                                            <td>{ data.status }</td>
                                                            <td className="view-button pointer"><span  data-toggle="modal" data-target="#create-job"  onClick={()=>parentObj.showpopup(data.title,data.status,data.description,data.userinfo.username,data.userinfo.email,data.userinfo.rating,data.userinfo.profileImg)} className="badge badge-success active-badge">Completed</span></td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    
                            <div className="jobs-main">
                                <h5>Completed Jobs For My Clients</h5>
                                <div className="job-table">
                                        { userCompletedJobs.length === 0 &&
                                            <div className="center">
                                                You don't have any job yet!
                                            </div>
                                        }
                                    <table className="table">
                                        { userCompletedJobs.length > 0 &&
                                            <thead>
                                                <tr>
                                                    <th scope="col">Sr. No</th>
                                                    <th scope="col">Job Title</th>
                                                    <th scope="col">Rating</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                        }
                                        <tbody>
                                            {  userCompletedJobs &&
                                                userCompletedJobs.map((data, index) => {
                                                    return (
                                                        <tr key={ index }>
                                                            <td scope="row">{ index + 1 }</td>
                                                            <td>{ data.title }</td>
                                                            <td>{ data.rating  }</td>
                                                            <td>$ { data.price }</td>
                                                            <td>{ data.status }</td>
                                                            <td className="view-button pointer"><span  data-toggle="modal" data-target="#create-job"  onClick={()=>parentObj.showpopup(data.title,data.status,data.description,data.clientinfo.username,data.clientinfo.email,data.clientinfo.rating,data.clientinfo.profileImg)} className="badge badge-success active-badge">Completed</span></td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                     <div className="modal fade" id="create-job">
                    <div className="modal-dialog modal-dialog-centered">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h4 className="modal-title">INFO</h4>
                          <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                        </div>
                        <div className="modal-body modal-body-job-info">
                        <div className="job_info">
                            <div className="alert alert-success ">
                                <strong>JOB DESCRIPTION</strong> 
                            </div>
                            <div className="jobinfo-inner-div">
                            <div>
                                <h6 className="camel_font">Job Title : { this.state.job_title }</h6>
                            </div>
                            <div className="task-detail1">
                            Description
                            <h6>{this.state.job_description}</h6>
                             </div>
                             <div className="task-detail1">
                            Status
                            <h6>{this.state.job_status}</h6>
                             </div>
                             <div className="task-detail1">
                              Coins
                                <h6>100 <img src="images/coin.png" alt="Cost"/></h6>
                             </div>
                             </div>
                        </div>
                         <div className="user_info">
                         <div className="alert alert-success ">
                                <strong>Client Info</strong> 
                            </div>
                          <div className="job-table jobtable-custom">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Rating</th>
                                            <th scope="col">Image</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                        <td className="camel_font">{this.state.client_user}</td>
                                        <td className="camel_font">{this.state.client_user_email}</td>
                                        <td >
                                        { this.state.client_rating == 5 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>								
                                        </span>
                                        }
                                        { this.state.client_rating== 4 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 3 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star" aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 2 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 1 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        {
                                           this.state.client_rating==0 && this.state.client_rating=='' &&
                                            <div className="rating four">
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                            </div>
                                        }
                                        </td>
                                        <td><img src={ (this.state.client_img == '' || !this.state.client_img ) ? 'images/dummy-img.png': this.state.client_img} height="50" width="50" alt="Cost"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>        
                      </div>
                    </div>
                  </div>
                  
                  
                    <div className={this.state.display + ' modal fade show '} id="close-job">
                        <div className="modal-dialog modal-dialog-centered">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4 className="modal-title">Close Job</h4>
                                    <button type="button" className="close" data-dismiss="modal">
                                        <img src="images/cross.png" alt="path ok" />
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <h2 className="pop-h">Are you sure to close the job ?</h2>
                                    <form onSubmit={ this.onFeedbackSubmit }>
                                        <div className="form-group">
                                            <textarea 
                                                placeholder="Feedback form" 
                                                id="comment" 
                                                rows="5" 
                                                className="form-control"
                                                name="feeback"
                                                value= {this.state.feedback }
                                                onChange={ this.onChange.bind(this) }></textarea>
                                        </div> 
                                        <div className="form-group">
                                        <ReactStars count={5} onChange={this.ratingChanged.bind(this)} size={48} value={this.state.rating1} color2={'#ffd700'} name="rating" />
                                        </div>
                                        <div className="form-group btns">
                                            <button type="submit"  className="lgn-btn">Submit</button>
                                        </div>
                                    </form>
                                </div>        
                            </div>
                        </div>
                    </div>
                </section>
                <ResetModal />
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        jobs: store.jobReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(MyJob);