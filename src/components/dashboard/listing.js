import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Pagination from "react-js-pagination";
import { push } from 'react-router-redux';
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import PropTypes from 'prop-types';
import moment from 'moment';
import store from '../../store';
import { saveMsg} from '../../actions/chat';
import { getListing, 
         setListItem, 
         getFilteredList, 
         getSearchResult, 
         getSearchResult2,
         filterBy, 
         filterBykeyword, 
         filterByRating,
         applyJob, 
         filterListBy,
         getAppliedJob} from '../../actions/listing.action';
import { getUserByToken ,zipKeywordList} from '../../actions/user.action';
import { UnsetNotification } from '../../actions/notification.action';

import Header from '../Header';
import Footer from '../Footer';
import ListDetail from "./listdetail";
import $ from 'jquery';

import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile,
    isFirefox,
    deviceDetect
  } from "react-device-detect";

import 'url-search-params-polyfill';
import LoginPopup  from '../popupmodal/loginpopup';
import SendMessage  from '../popupmodal/sendmessage';

class Listing extends Component {
    constructor(props,context) {
        super(props);
        this.context=context;
        this.state = {
            active: false,
            activePage: 1,
            filterType:  '',
            filterTypeName: '',
            filterByDate: '',
            filterkeyword: '',
            filterByRating: '',
            query: '',
            isChecked: false,
            modal: false,
            userId: '',
            userName: '',
            jobs: '',
            fliterActive: true,
            jobOwnerid:'',
            jobId:'',
            popupmessage:'',
            senMsgUsername:'',
            senMsgEmail:'',
            jobtitle:'',
            searchByZip: false,
            login: false,
            button: <button to="" onClick={ this.toggleClass } data-toggle="modal" data-target="#accept-job">Apply for Job</button>,
            button2: <button type="button" data-dismiss="modal" className="lgn-btn" onClick={this.handleApply}>Yes, I'm sure</button>,
            buttonsendmsg:<button data-dismiss="modal" data-target="#msg-pop1" data-toggle="modal">send message</button>,
            apply_notif:'',
            job_title:''
        };
        
        this.toggleClass = this.toggleClass.bind(this);
        this.onClick = this.onClick.bind(this); 
        this.handlePageChange = this.handlePageChange.bind(this);
        
        this.onSearchChange = this.onSearchChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onDateFilter = this.onDateFilter.bind(this);
        this.onKeywordFilter = this.onKeywordFilter.bind(this);
        this.onRatingFilter = this.onRatingFilter.bind(this);
        this.popupSendMsg = this.popupSendMsg.bind(this);
        this.setJobOwnerId = this.setJobOwnerId.bind(this);
        this.onChangepopupSendMsg = this.onChangepopupSendMsg.bind(this);
        this.handleApply = this.handleApply.bind(this);
    }
	
    componentWillMount(){
         store.dispatch(zipKeywordList()); 
    }
    componentDidMount() {
        document.title = "Comshare | Services";
        let fType = '';
        if(this.props.location.query.query) {
            if(this.props.location.query.type === "HELP") {
                this.setState({
                    query: this.props.location.query.query,
                    filterType: "JOB",
                    filterTypeName:"HELP"
                });
                fType = "JOB"
//                fType = "HELP"
            } else if(this.props.location.query.type === "JOB") {
                this.setState({
                    query: this.props.location.query.query,
                    filterType: "HELP",
//                    filterType: "JOB",
                    filterTypeName:"JOB"
                });
//                fType = "JOB"
                fType = "HELP"
            }
            store.dispatch(getSearchResult(this.props.location.query.query, fType));
        } else {
            store.dispatch(getListing());
//            this.setState({ searchByZip: true });
        }

        if(isBrowser) {
            this.setState({ fliterActive: false });
        }

        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            
            this.setState({ login: true});
            store.dispatch(getUserByToken(token));
        } 
    } 


    componentDidUpdate(prevProps) {
		/* let oldProps = prevProps.user.isLogin.user;
		let newProps = this.props.user.isLogin.user;
		
		if((oldProps == undefined) && newProps) {
			console.log(oldProps, newProps);
			this.setState({ query: newProps.zipcode });
			store.dispatch(filterListBy('', newProps.zipcode, '', '', ''));
		}  */
		 
    }
	
    componentWillReceiveProps(nextProps) {  
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }

        let token = JSON.parse(localStorage.getItem('token'));
        if(token && nextProps.user.isLogin) {
            
            if(this.state.userId==''){
                store.dispatch(getUserByToken(token));
                if(nextProps.user.isLogin.user) {
                    this.setState({
                        userId: nextProps.user.isLogin.user._id || '',
                        userName: nextProps.user.isLogin.user.username || ''
                    }); 
                } 
            }
        }
		
//        let newProps = nextProps.user.isLogin.user;
//        let oldProps = this.props.user.isLogin.user;
		
		/*if((oldProps === undefined) && newProps) {
			this.setState({ query: newProps.zipcode });
			store.dispatch(filterListBy('', newProps.zipcode, '', '', ''));
		} */
    }
 
    // 2 func 
    onClick(i,userid,jobid,email,title) {
        
        let apply_notif_msg = this.state.userName+' applied for '+title+' job!';
        this.setState({
                jobId: jobid,
                jobOwnerid: userid,
                senMsgEmail: email,
                apply_notif:apply_notif_msg,
                job_title : title
        })
        if(userid && jobid){
            


            store.dispatch(getAppliedJob({ userId: this.state.userId, jobId: jobid })).then(
                res => {
           
                    if(res){
                        if(res.data.length > 0) {
                        
                            if(res.data[0].msg_status==1){
                                this.setState({
                                    button:  <button to="" onClick={ this.toggleClass } data-toggle="modal" data-target="#accept-job">Apply for Job</button>,
                                    button2: <button type="button" data-dismiss="modal" className="lgn-btn" onClick={this.handleApply}>Yes, I'm sure</button>
                                    //buttonsendmsg: <h6 className="center">Already Send Message!</h6>
                                })      
                            }else{
                                this.setState({
                                    button: <h5 className="center">Already Applied!</h5>,
                                    button2: <h5 className="center">Already Applied!</h5>
                                    //buttonsendmsg: ''
                                })
                            }
                            
                        } else {
                            this.setState({
                                button:  <button to="" onClick={ this.toggleClass } data-toggle="modal" data-target="#accept-job">Apply for Job</button>,
                                button2: <button type="button" data-dismiss="modal" className="lgn-btn" onClick={this.handleApply}>Yes, I'm sure</button>,
                                buttonsendmsg: <button data-dismiss="modal" onClick={ this.toggleClass } data-target="#msg-pop1" data-toggle="modal">send message</button>
                            })
                        }
                    }
                    
                }
            ) 
        }else{
             this.setState({
                button:  <button to="" onClick={ this.toggleClass } data-toggle="modal" data-target="#accept-job">Apply for Job</button>,
                button2: <button type="button" data-dismiss="modal" className="lgn-btn" onClick={this.handleApply}>Yes, I'm sure</button>,
                buttonsendmsg: <button data-dismiss="modal" onClick={ this.toggleClass } data-target="#msg-pop1" data-toggle="modal">send message</button>
            })
        }
        this.toggleClass();
        this.setItem(i);
    }
    
    handleApply(){
        let jobId       = this.state.jobId;
        let ownerId     = this.state.jobOwnerid;
        let ownerEmail  = this.state.senMsgEmail;
        let msg         = this.state.apply_notif;
        let userId      = this.state.userId;
        let userName   = this.state.userName;
        let job_title   = this.state.job_title;
        let msg_status   = 0;
        
        if(userId === ownerId) {
            NotificationManager.error("You can't apply for your own job!", '', 2000);
        } else if (userId === '') {
            //NotificationManager.error("Please login into account!", '', 2000);
            this.props.dispatch(push('/login'));
            window.location.reload();
        } else {
            let data = {
                userId,
                userName,
                jobId,
                ownerEmail, 
                ownerId,
                msg,
                job_title,
                msg_status
            }
            
            store.dispatch(applyJob(data))
                .then(res => {
                    if(res) {
                        this.setState({
                            button: <h5  className="center">Thanks for Apply!</h5>,
                            button2: <h5 className="center">Thanks for Apply!</h5>
                            //buttonsendmsg: <h5 className="center">Thanks for Apply!</h5>
                        })
                        store.dispatch(getListing());
                    } 
                    NotificationManager.success("Thanks for Apply!", '', 2000);
                    
                });
        }
    }
    
    // Toggle Class
    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    }

    // Set Item
    setItem(i) {
        
        store.dispatch(setListItem(this.props.listing.data[i]));  
    }
    // Pagination   
    handlePageChange(pageNumber) {
        this.setState({ activePage: pageNumber });
    }

    // filterBy
    handleFilter(type) {
        const { filterType, query, filterByDate, filterkeyword, filterByRating } = this.state;

        if(type === "HELP") {
//            this.setState({ activePage: 1, filterType: "JOB", filterTypeName: "HELP" });
            this.setState({ activePage: 1, filterType: "HELP", filterTypeName: "HELP" });
            //this.filterBy({ fType: "JOB" });
            this.filterBy({ fType: "HELP" });
        } else if(type === "JOB") {
//            this.setState({ activePage: 1, filterType: "HELP", filterTypeName: "JOB" });
            this.setState({ activePage: 1, filterType: "JOB", filterTypeName: "JOB" });
            //this.filterBy({ fType: "HELP" });
            this.filterBy({ fType: "JOB" });
        }
       
    }

    onSearchChange(e) {
        this.setState({
            query: e.target.value
        });
    }

    onSearch(e) {
        e.preventDefault();
        const { filterType, query, filterByDate, filterkeyword, filterByRating } = this.state;
        this.setState({
            activePage: 1
        });

        this.filterBy({});
    }

    onDateFilter(e) {
        let change = e.target.value;
        this.setState({
            filterByDate: change,
            activePage: 1
        });

        this.filterBy({ fDate: change });
    }

    onKeywordFilter(e) {
        e.preventDefault();
        let keywords = e.target.value;

        this.setState({
            filterkeyword: keywords,
            activePage: 1
        });
        
        this.filterBy({ fKeyword: keywords });
    }

    onRatingFilter(e) {
        e.preventDefault();

        let rating = e.target.value;
        this.setState({
            filterByRating: rating,
            activePage: 1
        });

        this.filterBy({ fRating: rating });
    }

    filterBy(...data) {
        let args = data[0];
        let fByType = this.state.filterType;
        let fByQuery = this.state.query;
        let fByDate = this.state.filterByDate;
        let fByKeyword = this.state.filterkeyword;
        let fByRating = this.state.filterByRating;


        if(args.fType) {
            fByType = args.fType;
        } else if(args.fDate) {
            fByDate = args.fDate;
        } else if(args.fKeyword) {
            fByKeyword = args.fKeyword;
        } else if(args.fRating) {
            fByRating = args.fRating;
        }
        
        console.log('fByType =>'+fByType)
        console.log('fByQuery =>'+fByQuery)
        console.log('fByDate =>'+fByDate)
        console.log('fByKeyword =>'+fByKeyword)
        console.log('fByRating =>'+fByRating)
        
        if(fByType && fByQuery == '' && fByDate == '' && fByKeyword == '' && fByRating == '') { // If only Type
            
            console.log(' case 1');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
           

        } else if(fByType == '' && fByQuery && fByDate == '' && fByKeyword == '' && fByRating == '') { // If only Query
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
            console.log(' case 2');	
        } else if(fByType && fByQuery && fByDate == '' && fByKeyword == '' && fByRating == '') { // If Type, Query
            
            console.log(' case 3');
            //store.dispatch(getSearchResult(fByQuery, fByType));
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
			 
        } else if(fByType == '' && fByQuery && fByDate && fByKeyword == '' && fByRating == '') { // If filter Query
            console.log(' case 4');
            store.dispatch(filterBy(fByDate, fByType, fByQuery));

        } else if(fByType == '' && fByQuery == '' && fByDate  && fByKeyword == '' && fByRating == '') { // If filter By date
            console.log(' case 5');
            store.dispatch(filterBy(fByDate, fByType, fByQuery));

        } else if(fByType && fByQuery == '' && fByDate  && fByKeyword == '' && fByRating == '') { // If filter by date and type
            console.log(' case 6');
            store.dispatch(filterBy(fByDate, fByType, fByQuery));

        } else if(fByType && fByQuery && fByDate  && fByKeyword == '' && fByRating == '') { // If filter by date, query and type
            console.log(' case 7');
            store.dispatch(filterBy(fByDate, fByType, fByQuery));
        
        } else if(fByType == '' && fByQuery == '' && fByDate == ''  && fByKeyword  && fByRating == '') { // If keyword
            console.log(' case 8');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate));
        
        } else if(fByType && fByQuery == '' && fByDate == ''  && fByKeyword  && fByRating == '') { // if Keyword & type
             console.log(' case 9');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate));
        
        } else if(fByType == '' && fByQuery == '' && fByDate  && fByKeyword  && fByRating == '') { // if keywaord & date
             console.log(' case 10');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate));
        
        } else if(fByType && fByQuery == '' && fByDate  && fByKeyword  && fByRating == '') { // if keyword & type & date
             console.log(' case 11');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate));
            
        } else if(fByType && fByQuery && fByDate  && fByKeyword  && fByRating == '') { // if type & query & keyword & date
            console.log(' case 12');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate));
        
        } else if(fByType == '' && fByQuery  == '' && fByDate == '' && fByKeyword == '' && fByRating) { // if only rating
            console.log(' case 13');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        
        } else if(fByType && fByQuery == '' && fByDate == '' && fByKeyword == '' && fByRating) { // if rating, type 
            console.log(' case 14');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        
        } else if(fByType && fByQuery && fByDate == '' && fByKeyword == '' && fByRating) { // if rating query type
            console.log(' case 15');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        
        } else if(fByType && fByQuery && fByDate && fByKeyword == '' && fByRating) { // if rating query date type
            console.log(' case 16');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        
        }  else if(fByType && fByQuery && fByDate && fByKeyword  && fByRating) { // if rating, query, date, type
            console.log(' case 17');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
            
        }  else if(fByType === '' && fByQuery && fByDate === '' && fByKeyword == ''  && fByRating) { // if rating, query
             console.log(' case 18');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
            
        }  else if(fByType && fByQuery === '' && fByDate  && fByKeyword  && fByRating) { // if type, date, rating, keyWords
            console.log(' case 19');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
            
        } else if(fByType && fByQuery === '' && fByDate  && fByKeyword === ''  && fByRating) { // if type, date, rating
            console.log(' case 20');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
            
        } else if (fByType === '' && fByQuery === '' && fByDate  && fByKeyword  && fByRating) { // if date, keyword, rating
            console.log(' case 21');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));

        } else if (fByType === '' && fByQuery === '' && fByDate == ''  && fByKeyword  && fByRating) { // if keyword, rating
            console.log(' case 22');
            store.dispatch(filterByRating(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        /*add new filters */
        else if (fByType && fByQuery === '' && fByDate == ''  && fByKeyword  && fByRating) { // if type , keyword, rating
            console.log(' case 23');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        else if (fByType && fByQuery && fByDate == ''  && fByKeyword  && fByRating) { // if type ,query, keyword, rating
            console.log(' case 24');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        else if (fByType =='' && fByQuery && fByDate == ''  && fByKeyword  && fByRating=='') { // if query, keyword
            console.log(' case 25');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        else if (fByType =='' && fByQuery && fByDate && fByKeyword  && fByRating=='') { // if query, date , keyword
            console.log(' case 26');
            store.dispatch(filterBykeyword(fByKeyword, fByType, fByQuery, fByDate, ));
        }
        else if (fByType =='' && fByQuery && fByDate && fByKeyword  && fByRating) { // if query, date , keyword,rating
            console.log(' case 27');
            store.dispatch(filterByRating(fByType,fByQuery,fByDate, fByKeyword, fByRating));
        }
        else if (fByType =='' && fByQuery && fByDate == '' && fByKeyword  && fByRating) { // if query , keyword,rating
            console.log(' case 28');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        else if (fByType && fByQuery && fByDate == ''  && fByKeyword  && fByRating=='') { // if type ,query, keyword
            console.log(' case 29');
            store.dispatch(filterListBy(fByType, fByQuery, fByDate, fByKeyword, fByRating));
        }
        
    }


    onFilterBtn() {
        let currentState = this.state.fliterActive;
        this.setState({ fliterActive: !currentState });
    }

    onClearFilter() {
        store.dispatch(getListing());
        this.setState({
            query: '',
            fByType: '',
            fByQuery: '',
            fByDate: '',
            fByKeyword: '',
            filterByRating: '',
            filterTypeName: '',
        });

//        $("#recentFilter").addClass("active");
//        $("#jobMounting").addClass("active");
//        $('#fiveRate').addClass("active");
       
        $("#recentFilter").removeClass("active");
        $("#jobMounting").removeClass("active");
        $('#fiveRate').removeClass("active");
        
        $("#newFilter").removeClass("active");
        $("#oldFilter").removeClass("active");

        $("#jobMovers").removeClass("active");
        $("#jobFurniture").removeClass("active");
        $("#jobPlumbing").removeClass("active");
        $("#jobGeneral").removeClass("active");
        $("#jobLifting").removeClass("active");
        $("#fourRate").removeClass("active");
        $("#oneRate").removeClass("active");
        $("#twoRate").removeClass("active");
        $("#threeRate").removeClass("active");
        $(".switch-input").removeClass("active");
        
        $(".inpt_active ").removeClass("active");
	}

    popupSendMsg(){
        if(this.state.userId==''){
            this.props.dispatch(push('/login'));
            window.location.reload();
        } else {
            if(this.state.jobOwnerid==this.state.userId){
                 NotificationManager.error("You can't send message for your own job!", '', 2000);
            }else{
                /* Send Message in chat table */
                if(this.state.popupmessage.trim()==''){
                    NotificationManager.error("You can't send blank message", '', 2000);
                }else{
                    
                    /*Send Message in chat table*/
                    let data = {
                                userId      :   this.state.userId,
                                clientId    :   this.state.jobOwnerid,
                                jobId       :   this.state.jobId,
                                msg         :   this.state.popupmessage,
                                image       :   ''
                        }
                    store.dispatch(saveMsg(data));
                    let messsage = this.state.senMsgUsername+' send message for '+this.state.jobtitle+' job!';
                    let applydata = {
                        userId : this.state.userId,
                        userName:this.state.senMsgUsername,
                        jobId: this.state.jobId,
                        ownerEmail:this.state.senMsgEmail, 
                        ownerId : this.state.jobOwnerid,
                        msg : messsage,
                        msg_status:1
                    }

                    store.dispatch(applyJob(applydata))
                    .then(res => {
                        NotificationManager.success("Thanks for Sending Message!", '', 2000);
                        store.dispatch(getListing());
                    });
                }
                
            }
        }
    }
    
    setJobOwnerId(jobOwnerid,jobId,senMsgUsername,senMsgEmail,jobtitle){
        this.setState({
            jobOwnerid: jobOwnerid,
            jobId: jobId,
            senMsgUsername: senMsgUsername,
            senMsgEmail: senMsgEmail,
            jobtitle: jobtitle
        });
    }
    
    onChangepopupSendMsg(e){
        this.setState({ 
            popupmessage: e.target.value 
        });
    }

    filterByMyZip(e) {
		// window.location.reload();
		if(Object.prototype.toString.call(this.props.user.isLogin) === '[object Object]') {
			
			let zipcode = this.props.user.isLogin.user.zipcode;
		
			this.setState({ query: zipcode });
			store.dispatch(filterListBy('', zipcode, '', '', ''));
		} else {
			this.props.router.push('/login');
		}
	}
   
    render() {  
        
        let parentObj=this;
        /** Jquery Hack to fix active class issue filter section */

//        $("#helpClick").click(function() {
//            $('#help').addClass("active");
//            $('#job').removeClass("active");
//        });
//
//        $("#jobClick").click(function() {
//            $('#job').addClass("active");
//            $('#help').removeClass("active");
//        });

        $("#recent").click(function() {
            $("#recentFilter").addClass("active");
            $("#newFilter").removeClass("active");
            $("#oldFilter").removeClass("active");
        });

        $("#old").click(function() {
            $("#oldFilter").addClass("active");
            $("#recentFilter").removeClass("active");
            $("#newFilter").removeClass("active");
        });

        $("#new").click(function() {
            $("#newFilter").addClass("active");
            $("#recentFilter").removeClass("active");
            $("#oldFilter").removeClass("active");
        });
        
        $(".inpt_active").click(function() {
             $(".inpt_active").removeClass("active");
             $(this).addClass('active');
        });

        $("#movers").click(function() {
            $("#jobMounting").removeClass("active");
            $("#jobMovers").addClass("active");
            $("#jobFurniture").removeClass("active");
            $("#jobPlumbing").removeClass("active");
            $("#jobGeneral").removeClass("active");
            $("#jobLifting").removeClass("active");
        });

        $("#furniture").click(function() {
            $("#jobMounting").removeClass("active");
            $("#jobMovers").removeClass("active");
            $("#jobFurniture").addClass("active");
            $("#jobPlumbing").removeClass("active");
            $("#jobGeneral").removeClass("active");
            $("#jobLifting").removeClass("active");
        });

        $("#general").click(function() {
            $("#jobMounting").removeClass("active");
            $("#jobMovers").removeClass("active");
            $("#jobFurniture").removeClass("active");
            $("#jobPlumbing").removeClass("active");
            $("#jobGeneral").addClass("active");
            $("#jobLifting").removeClass("active");
        });

        $("#plumping").click(function() {
            $("#jobMounting").removeClass("active");
            $("#jobMovers").removeClass("active");
            $("#jobFurniture").removeClass("active");
            $("#jobPlumbing").addClass("active");
            $("#jobGeneral").removeClass("active");
            $("#jobLifting").removeClass("active");
        });

        $("#lifting").click(function() {
            $("#jobMounting").removeClass("active");
            $("#jobMovers").removeClass("active");
            $("#jobFurniture").removeClass("active");
            $("#jobPlumbing").removeClass("active");
            $("#jobGeneral").removeClass("active");
            $("#jobLifting").addClass("active");
        });


        $("#fivestar").click(function() {
            $('#fiveRate').addClass("active");
            $("#fourRate").removeClass("active");
            $("#oneRate").removeClass("active");
            $("#twoRate").removeClass("active");
            $("#threeRate").removeClass("active");
        });

        $("#fourstar").click(function() {
            $('#fiveRate').removeClass("active");
            $("#oneRate").removeClass("active");
            $("#twoRate").removeClass("active");
            $("#threeRate").removeClass("active");
            $("#fourRate").addClass("active");
        });

        $("#threestar").click(function() {
            $('#fiveRate').removeClass("active");
            $("#fourRate").removeClass("active");
            $("#oneRate").removeClass("active");
            $("#twoRate").removeClass("active");
            $("#threeRate").addClass("active");
        });

        $("#twoStar").click(function() {
            $('#fiveRate').removeClass("active");
            $("#fourRate").removeClass("active");
            $("#threeRate").removeClass("active");
            $("#oneRate").removeClass("active");
            $("#twoRate").addClass("active");
        });

        $("#oneStar").click(function() {
            $('#fiveRate').removeClass("active");
            $("#fourRate").removeClass("active");
            $("#threeRate").removeClass("active");
            $("#twoRate").removeClass("active");
            $("#oneRate").addClass("active");
        });

        /** End Jquery */

        const { query, active, userId, userName, profileImg, isChecked, jobs, fliterActive, filterTypeName } = this.state;

        let activePage = this.state.activePage;
        let total = 0;
        let perpage = 9;
        let startfrom = ((activePage * perpage) - perpage);
        let endfrom = startfrom + perpage;
        let _list = 0;
        
        if(this.props.listing.data) {
            this.props.listing.data.map((list, index) => {
                total++;
            });
        } else {
            total = 0;
        }

        let filterBtn = '';
        if(isMobile) {
            filterBtn = <button type="button" className="btn btn-filter btn-block"  onClick={ this.onFilterBtn.bind(this) }>Filter { this.state.fliterActive ? "Open" : "Close" } </button>
        }
        return (
            <div className="main">
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">{this.context.translate('Home')}</Link>
                            <span className="breadcrumb-item active">{this.context.translate('Market place')}</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="main-listing">
                    <div className="container">
                        <div className="row ">
                            <div className="col-md-3" >
                                <div>{ filterBtn }</div>
                                <div className="sidebar" hidden={ fliterActive }>
                                    <div className="toggle-search">
                                        <div className="switch">
                                            <input 
                                                type="radio" 
                                                className={ this.state.filterType == "HELP" ? 'switch-input active': 'switch-input' }
                                                name="view" 
                                                value="help" 
                                                id="help"/>
                                            <label 
                                                className="switch-label switch-label-off" 
                                                id="helpClick" 
                                                value="HELP" 
                                                onClick={ this.handleFilter.bind(this, "HELP")}>{this.context.translate('LOOKING FOR HELP')}</label>
                                            <input 
                                                type="radio" 
                                                className={ this.state.filterType == "JOB" ? 'switch-input active': 'switch-input' }
                                                name="view" 
                                                value="job" 
                                                id="job"/>
                                            <label 
                                                className="switch-label switch-label-on" 
                                                id="jobClick" 
                                                value="JOB" 
                                                onClick={ this.handleFilter.bind(this, "JOB")}>{this.context.translate('LOOKING FOR JOB')}</label>
                                            <span className="switch-selection"></span>
                                        </div>
                                    </div>
                                    <div className="search-field">
                                        <form onSubmit={ this.onSearch }>
                                            <input 
                                                type="text" 
                                                placeholder="Keyword / Zipcode"
                                                name="search"
                                                value={ query }
                                                onChange={this.onSearchChange}/>
                                            <button type="submit"><i className="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    <div className="filters">
                                        <h3 className="filter-head">{this.context.translate('Filter by')}</h3>
                                        <ul>
                                            <li id="recent">
                                                <input  
                                                    type="radio" 
                                                    name="filter" 
                                                    value="recent"
                                                    className=""
                                                    id="recentFilter"
                                                    onClick={ this.onDateFilter }/>
                                                <label></label>
                                                {this.context.translate('Recent')}
                                            </li>
                                            <li id="old">
                                                <input 
                                                    type="radio" 
                                                    name="filter" 
                                                    value="old"
                                                    id="oldFilter"
                                                    className=""
                                                    onClick={ this.onDateFilter }/>
                                                <label ></label>
                                                Old to new
                                            </li>
                                            <li id="new">
                                                <input 
                                                    type="radio" 
                                                    name="filter" 
                                                    value="new"
                                                    className=""
                                                    id="newFilter"
                                                    onClick={ this.onDateFilter }/>
                                                <label ></label>
                                                {this.context.translate('New to old')}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="filters">
                                        <h3 className="filter-head">{this.context.translate('Type of Job')}</h3>
                                        <ul>
                                            {
                                                this.props.user.zipKeywordList.data !=undefined &&
                                                    this.props.user.zipKeywordList.data.map((data,index)=> {
                                                        if(data.type=='keyword'){
                                                            return(
                                                                    <li className="mounting camel_font" key={index}>
                                                                        <input 
                                                                            type="radio" 
                                                                            name="job" 
                                                                            value={data.value}
                                                                            className="inpt_active"
                                                                            id="jobMounting"
                                                                            onClick={this.onKeywordFilter}/>
                                                                        <label></label>
                                                                        {data.value}
                                                                    </li>
                                                            )
                                                        }
                                                })
                                            } 
                                            
                                        </ul>
                                    </div>
                                    <div className="filters">
                                        <h3 className="filter-head">{this.context.translate('Sort by Rating')}</h3>
                                        <ul>
                                            <li className="rating five" id="fivestar">
                                                <input 
                                                    type="radio" 
                                                    name="Rating"
                                                    value="5"
                                                    className=""
                                                    onClick={ this.onRatingFilter }
                                                    id="fiveRate"/>
                                                <label></label>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                            </li>
                                            <li className="rating four" id="fourstar">
                                                <input 
                                                    type="radio" 
                                                    name="Rating" 
                                                    value="4"
                                                    onClick={ this.onRatingFilter }
                                                    id="fourRate" />
                                                <label></label>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star"></i>
                                            </li>
                                            <li className="rating three" id="threestar">
                                                <input 
                                                    type="radio" 
                                                    name="Rating" 
                                                    value="3"
                                                    onClick={ this.onRatingFilter }
                                                    id="threeRate"/>
                                                <label></label>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                            </li>
                                            <li className="rating two" id="twoStar">
                                                <input 
                                                    type="radio"  
                                                    name="Rating" 
                                                    value="2"
                                                    onClick={ this.onRatingFilter }
                                                    id="twoRate"/>
                                                <label></label>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                            </li>
                                            <li className="rating one" id="oneStar">
                                                <input 
                                                    type="radio" 
                                                    name="Rating" 
                                                    value="1"
                                                    onClick={ this.onRatingFilter }
                                                    id="oneRate"/>
                                                <label></label>
                                                <i className="fa fa-star star_color"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-9 ">
                                <div className="listing">
                                <NotificationContainer/>  
                                    <div>
                                        { this.props.listing.data &&
                                            <div>
                                                <p className="float-left">{this.context.translate('Showing results')} {/*({  this.props.listing.data.length }) */} of <em><b>- { this.state.filterTypeName || 'All'} Type</b></em></p>
                                                <span className="float-right">
                                                    { this.state.userId!='' ? <button className="btn btn-sm mr-3" onClick={ this.filterByMyZip.bind(this) }>Filter With My Zipcode</button>:'' }
                                                    
                                                    <button className="btn btn-sm float-right" onClick={ this.onClearFilter.bind(this) }>Clear Filter</button>
                                                </span>
                                            </div>
                                        }
                                        
                                        { this.props.listing.data && 
                                            this.props.listing.data.map((list, index)  => {
                                                if(this.state.login){
                                                    var send_msg_val = <button onClick={()=>parentObj.setJobOwnerId(list.userId,list._id,this.state.userName,list.userinfo.email,list.title)}  data-target="#msg-pop" data-toggle="modal">send message</button>;
                                                    var detailed_button = <button id="detail-popup"  onClick={ this.onClick.bind(this, index,list.userId,list._id,list.userinfo.email,list.title) }>{this.context.translate('detailed View')}</button>

                                                }else{
                                                    var send_msg_val = <button data-target="#login-pop" data-toggle="modal">send message</button>;
                                                    var detailed_button = <button id="detail-popup"  onClick={ this.onClick.bind(this, index ,list.userId,list._id) }>{this.context.translate('detailed View')}</button>
                                                }
                                                _list++;
                                                if((startfrom < _list && _list <= endfrom) && (list.userinfo.coins >= list.price) ){
                                                    return (
                                                        <div key={index}>
                                                            <div className="listing-result">
                                                                <div className="list-pic">
                                                                    <img src={ list.userinfo.profileImg || 'images/dummy-img.png' } alt="profile" />
                                                                    <h6 className="camel_font"><Link to={ `/userprofile?id=${list.userinfo._id}` }> { list.userinfo.username }</Link></h6>
                                                                        { list.userinfo.rating == '5' &&
                                                                        <div className="rating five">
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>  
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                        </div>
                                                                    }
                                                                    { list.userinfo.rating == '4' &&
                                                                        <div className="rating four">
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                        </div>
                                                                    }
                                                                    { list.userinfo.rating == '3' &&
                                                                        <div className="rating three">
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                        </div>
                                                                    }
                                                                    { list.userinfo.rating == '2' &&
                                                                        <div className="rating two">
                                                                            <i className="fa fa-star"></i>
                                                                            <i className="fa fa-star"></i>
                                                                        </div>
                                                                    }
                                                                    { list.userinfo.rating == '1' &&
                                                                        <div className="rating one">
                                                                            <i className="fa fa-star"></i>
                                                                        </div>
                                                                    }
                                                                    
                                                                    <span>{list.mycount !=undefined ?list.mycount : '0'} {this.context.translate('Reviews')}</span>
                                                                </div>
                                                                <div className="list-details">
                                                                    <h4 className="camel_font">{ list.title }</h4>
                                                                    <p><i className="fa fa-user"></i>{list.mycount !=undefined ?list.mycount : '0'} {this.context.translate('Jobs completed')}</p>
                                                                    <p><img src="images/coin.png" alt="Cost" />
                                                                    <img src="images/info.png" alt="Cost" title={ list.price  +" "+ this.context.translate('coins required')+ "!" ||  +  "80 "+ this.context.translate('coins required')+"!"} /></p>
                                                                </div>
                                                                <div className="list-btns">
                                                                    {
                                                                      list.notifications !=undefined && list.notifications.length > 0 &&
                                                                            list.notifications.map((list_notif, index1)  => {
                                                                            if(list_notif.fromUserId == this.state.userId && list_notif.jobId == list._id && list_notif.msg_status==1){ 
                                                                                {/*send_msg_val = <h6 className="center">Already Send Message!</h6>;*/}
                                                                                detailed_button = <button id="detail-popup"  onClick={ this.onClick.bind(this, index ,list_notif.userId,list._id,list.userinfo.email,list.title) }>{this.context.translate('detailed View')}</button>
                                                                            }
                                                                            if(list_notif.fromUserId == this.state.userId && list_notif.jobId == list._id && list_notif.msg_status!=1){ 
                                                                            {/*send_msg_val = <h6 className="center">Already Applied!{list.msg_status}</h6>*/}
                                                                                detailed_button = <button id="detail-popup"  onClick={ this.onClick.bind(this, index,list_notif.userId,list._id,list.userinfo.email,list.title) }>{this.context.translate('detailed View')}</button>
                                                                            }
                                                                        })
                                                                    }   
                                                                    { send_msg_val } 
                                                                    { detailed_button }
                                                                </div>
                                                            </div>
                                                           
                                                            <div className={"details-view1 " + (this.state.active ? ' active-pop':  '') }>
                                                                <ListDetail  
                                                                    key={index}
                                                                    toggleClass={this.toggleClass}
                                                                    userId={userId} 
                                                                    userName={userName}
                                                                    button={this.state.button}
                                                                    button2={this.state.button2}
                                                                    buttonsendmsg={this.state.buttonsendmsg}/>
                                                            </div>
                                                        </div>
                                                    )
                                                }
                                            })
                                        }

                                        { this.props.listing.data.length === 0 && 
                                            <p className="no_list_found" > No List found.</p>
                                        }
                                    </div> 
                                    { total > 8 &&  
                                        <div>
											<Pagination
											  activePage={activePage}
											  itemsCountPerPage={perpage}
											  totalItemsCount={total}
											  pageRangeDisplayed={5}
											  onChange={this.handlePageChange}
											/>
                                          </div>
                                    }
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </section>
               <SendMessage 
                    popupSendMsg={this.popupSendMsg}
                    onChangepopupSendMsg={this.onChangepopupSendMsg}
                />
               <LoginPopup/>
                <Footer />
            </div>
        );
    }
}

Listing.contextTypes = {
  translate: PropTypes.func
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification,
        listing: store.listingReducer
    };
};

export default connect(mapStateToProps)(Listing);