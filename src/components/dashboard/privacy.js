import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../Header';
import Footer from '../Footer';
import store from '../../store';

import { getCmsPageData} from '../../actions/user.action';
class Privacy extends Component {
    
    constructor(props,context) {
        super(props);
    }
    componentWillMount(){
        store.dispatch(getCmsPageData())
    }
    render() {
        return (
            <div className="main">
            
            <Header />
            
            <div className="container">
		<div className="row">
                    <div className="col-md-12">
                            <nav className="breadcrumb">
                              <a className="breadcrumb-item" href="/">Home</a>
                              <span className="breadcrumb-item active">Privacy Policy</span>
                            </nav>
                    </div>
		</div>
            </div>
            <section className="profile-content">
                <div className="container">
                    <div className="pages-content">
                            <h2 className="section-head">Privacy Policy</h2>
                            {
                                this.props.user.cms_page_data != undefined  &&
                                    this.props.user.cms_page_data.data.map((cms_data,index) => {
                                      if(cms_data.page_type=='privacy'){
                                            return <div key={index} dangerouslySetInnerHTML={{__html: cms_data.content}} />;
                                      }  
                                })
                            }
                    </div>
                </div>
            </section>
            <Footer />    
            </div>
        )
    }
}
const mapStateToProps = store => {
    return {
        user: store.userReducer
    };
};
export default connect(mapStateToProps)(Privacy);