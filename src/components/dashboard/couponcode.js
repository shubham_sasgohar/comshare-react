import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';
import { UnsetNotification } from '../../actions/notification.action';
import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';
import { voucherList, couponRedeemed, getCouponRedeemed} from '../../actions/voucher.action';



class Couponcode extends Component {

    constructor(props) {
        super(props);

        this.state = {
            code: ''
        }

        this.onChange = this.onChange.bind(this);
        this.submit = this.submit.bind(this);

    }

    componentDidMount() {

        store.dispatch(voucherList());
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    submit() {
        if(this.state.code == ''){
           NotificationManager.error("Coupon Code is required!", '', 2000); 
           return false;
        }        
        let data = {
            code: this.state.code
        }
        store.dispatch(getCouponRedeemed(data)).then(
                res1 => {
                    if (res1.data.length > 0) {
                        this.props.router.push({pathname: '/vendor-details', state: {code: this.state.code}})
                    } else {
                         NotificationManager.error("Coupon Code is Not valid!", '', 2000);
                    }
                })

    }

    render() {
        return(
                <div className="main">
                    <Header />
                    <NotificationContainer/> 
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="breadcrumb">
                                    <Link className="breadcrumb-item" to="/">Home</Link>
                                    <span className="breadcrumb-item active">vendor</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section className="profile-content rating-main new-login-main1">
                        <div className="container login-inner">
                            <div className="login-main new-login-main">
                                <div className="logo-main ">
                                    <a href="index.html"><img src="images/logo.png" alt="Footer US" /></a>
                                </div>
                                <p>Please enter your Vendor Code to continue</p>
                                <form action="/action_page.php">
                                    <div className="form-group">
                                        <input type="text" name="code" onChange={this.onChange} placeholder="Vendor Code " id="code" className="form-control" />
                                    </div>
                                    <div className="form-group btns">
                                        <a href="javascript:void(0)" onClick={this.submit} className="lgn-btn">SUBMIT</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                    <Footer />
                </div>
                )
    }
}

const mapStateToProps = store => {
    return {
        vouchers: store.voucherReducer
    };
};

export default connect(mapStateToProps)(Couponcode);