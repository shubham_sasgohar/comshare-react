import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';
import { UnsetNotification } from '../../actions/notification.action';
import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';
import { voucherList, couponRedeemed, getCouponRedeemed , changeRedeemStatus} from '../../actions/voucher.action';
import APP from '../../config';



class Vendordetails extends Component {

    constructor(props) {
        super(props);
         this.state = {
            redeem: {
                data:[]
            }
         }
        this.redeem = this.redeem.bind(this);
        
    }
    componentWillReceiveProps(nextProps) {
//        console.log('this.props');
//        console.log(this.props.location.state.code);
//        
//        console.log('this.props');
    }
    componentDidMount() {
        if (this.props.location.state != undefined) {
            let data = {
                code: this.props.location.state.code
            }
            store.dispatch(getCouponRedeemed(data)).then(
                res1 => {
                    
                    if (res1.data.length > 0) {
                        this.setState({redeem:res1})
                    }
            })
        }else{
            this.props.router.push('/')
        }

    }
    
    redeem(code){
        
        var data ={
            code : code
        }
        store.dispatch(changeRedeemStatus(data)).then(
            res1 => {
            if (res1) {
                
                let data1 = {
                    code: code
                }
                store.dispatch(getCouponRedeemed(data1)).then(
                res2 => {
                    NotificationManager.success("Thanks for Redeem!", '', 2000);
                    if (res2.data.length > 0) {
                        this.setState({redeem:res2})
                    }
            })
            }
        })
        
    }

    render() {

        var coupon_data = this.state.redeem.data;
        console.log(coupon_data)
        let parentObj =this;
        return(
                <div className="main">
                    <Header />
                    <NotificationContainer/> 
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="breadcrumb">
                                    <Link className="breadcrumb-item" to="/">Home</Link>
                                    <span className="breadcrumb-item active">Vendor details</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section className="profile-content">
                        <div className="container">
                           {
                             coupon_data.length > 0  &&
                                coupon_data.map((data,index)=>{
                                return ( 
                                    <div className="outer" key={index}>
                                        <h2 className="profile-heading"> VOUCHER DETAILS </h2>
                                        <div className="inner">
                                            <div className="col-md-4 col-sm-6 float-left">
                                                <div className="vouchers">
                                                    <div className="vou-logo">
                                                        <img src={data.voucherInfo.image=='' ? "images/logo-1.png" : `${APP.API_BASEURL}/vouchers_images/${data.voucherInfo.image}` } alt="Vouchers" />
                                                        <span>{data.voucherInfo.points} Points</span>
                                                    </div>
                                                    <h6>{data.voucherInfo.name}</h6>
                                                    <p>Zipcoce: {data.voucherInfo.zipcode}</p>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6 float-left">
                                                <div className="vouchers user-prof">
                                                    <div className="vou-logo">
                                                    
                                                        <img src={data.userinfo.profileImg=='' ? "images/profile.jpg" : data.userinfo.profileImg } alt="Vouchers" />

                                                    </div>
                                                    <h6>{data.userinfo.username}</h6>
                                                    <p>{data.userinfo.address1}</p>
                                                    <p>{data.userinfo.city}</p>
                                                </div>
                                            </div>
                                            <div className="col-md-4 col-sm-6 float-left">
                                                <div className="vouchers vou-status">
                                                    <i className="fa fa-check-circle" /> 
                                                    <p><strong>{data.valid_status}</strong></p>
                                                    { (data.valid_status == 'Valid' && (data.redeemedStatus == false || data.redeemedStatus == ''))  ? <button data-toggle="modal" onClick={()=>parentObj.redeem(data.code)}>REDEEM</button>: data.valid_status == 'Valid' ? <button className="pointerNone" >ALREADY REDEEMED</button> : '' }
                                                </div>
                                            </div>						
                                        </div>
                                    </div>
                                )
                            })
                        }

                        </div>
                    </section>
                    <div className="modal fade show" id="redeem-coupon-popup">
                        <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Redeem</h4>
                                <button type="button" className="close" data-dismiss="modal">
                                    <img src="images/cross.png" alt="path ok" />
                                </button>
                            </div>

                            <div className="modal-body">
                                <h2 className="pop-h">Are you sure you want to redeem this coupon ?</h2>
                                <form action="/action_page.php">
                                <div className="form-group forms-btn btns">
                                   <button type="button" onClick={this.redeem} data-dismiss="modal" className="lgn-btn">Yes, I'm sure</button>
                                </div>

                                </form>
                            </div>        
                        </div>
                        </div>
                    </div>
                    <Footer />
                </div>
                )
    }
}

const mapStateToProps = store => {
    return {
        vouchers: store.voucherReducer
    };
};

export default connect(mapStateToProps)(Vendordetails);