import React, { Component } from 'react';
import { connect } from 'react-redux';
import ResetModal  from '../auth/resetModal';
import { Link } from 'react-router'
import ReactStars from 'react-stars';
import { render } from 'react-dom';

import store from '../../store';
import { getUserByToken } from '../../actions/user.action';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { getAppliedUserDetail, notifyAction, hireUser, closeJob ,applyJob} from '../../actions/listing.action';
import { UnsetNotification, getUserNotification } from '../../actions/notification.action';
import { saveMsg,getMsg,getUnreadMsgCount,setMessageStatus } from '../../actions/chat';

import moment from 'moment';
import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';
import $ from 'jquery';
//import $ from 'jquery';
import io from 'socket.io-client'
import { VERIFY_USER,USER_CONNECTED } from '../../Events'
import { COMMUNITY_CHAT, MESSAGE_SENT, MESSAGE_RECIEVED, TYPING } from '../../Events'


const socketUrl = "http://node.brstdev.com:5013/";

class Messages extends Component {
    data = [];

    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            imageURL: '',
            firstname: '',
            lastname: '',
            fromUser: '',
            status: '',
            active: false,
            chatMessanger: true,
            appliedUdetail: '',
            activeChat: '',
            hiredBtn: false,
            feedback: '',
            rating1: 0,
            socket:null,
            current_user_id:'',
            current_userfrom_id:'',
            current_job_id:'',
            textareavalue:'',
            socket:null,
            user:null,
            nickname:"",
            error:"",
            chats:[],
            activeChat1:[],
            message:"",
            isTyping:false,
            curr_user_name:'',
            index_active_msg:0,
            login_user_img:'',
            loading: true,
            rating:'',
            coins:'',
            jobcoins:''
        }

        this.toggleClass = this.toggleClass.bind(this);
        this.showChat = this.showChat.bind(this);
        this.onFeedbackSubmit = this.onFeedbackSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.sendmsg = this.sendmsg.bind(this);
        this.handleChange = this.handleChange.bind(this);
        
        this.scrollDown = this.scrollDown.bind(this)
      
    }
    
    componentWillMount() {
     
        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token)).then(
                res1 => {
                    this.setState({login_user_img:res1.data.user.profileImg})
                    this.setState({curr_user_name:res1.data.user.username});
                        this.initSocket(token);
                        this.stopCheckingTyping()
                        const { socket } = this.state;
                        socket.emit(COMMUNITY_CHAT, this.resetChat);
                })
        }
        
        store.dispatch(getUnreadMsgCount());// get unread count messages
   
    }
    
    componentDidMount() {
        let id = this.props.location.query.id;
        this.scrollDown()
        document.title = "Comshare | Message Page";
        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token)).then(
                res1 => {
                    store.dispatch(getUserNotification(res1.data.user._id)).then(
                        res => {
                            
                            this.data = res.data; 
                            this.setState({
                                    userId: res1.data.user._id
                            });
                            
                        } 
                    )
                }
            );
        } else {
            this.props.router.push('/login');
        }
		
		 
    }
    

    componentDidUpdate(prevProps) { 
//        let oldProps = this.props.chat_record.unreadMsgCount;
//        let newProps = prevProps.chat_record.unreadMsgCount;
//        
//        console.log('prevProps')
//        console.log(prevProps)
//        console.log('prevProps')
//        console.log('this.props')
//        console.log(this.props)
//        console.log('this.props')
//        if(oldProps == newProps) { 
//            
//           // console.log('here io amzsd')
////            store.dispatch(getUnreadMsgCount());// get unread count messages
//                 
//        }
        
        this.scrollDown()
    }

    componentWillReceiveProps(nextProps) {  
        
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }

        let token = JSON.parse(localStorage.getItem('token'));
        if(token && nextProps.user.isLogin) {
            if(nextProps.user.isLogin.user != undefined){
                
                this.setState({
                    userId: nextProps.user.isLogin.user._id || '',
                    imageURL: nextProps.user.isLogin.user.profileImg || 'images/dummy-img.png',
                    firstname: nextProps.user.isLogin.user.firstname || '',
                    lastname: nextProps.user.isLogin.user.lastname || '',
                    rating: nextProps.user.isLogin.user.rating || '',
                    coins: nextProps.user.isLogin.user.coins || '',
                });
            }
        }

        let oldProps = this.props.notification.Notify;
        let newProps = nextProps.notification.Notify;

        if(oldProps !== newProps) { 
            if(this.state.userId) {
                store.dispatch(getUserNotification(this.state.userId)).then(
                    res => {
                        this.data = res.data; 
                    }
                );
            }
        }
        
        let oldMsg = this.props.chat_record.unreadMsgCount;
        let newMsg = nextProps.chat_record.unreadMsgCount;
		
        if(oldMsg !== newMsg) {
			//console.log(oldMsg, newMsg);
            store.dispatch(getUnreadMsgCount())
        }
        
    }
    
    scrollDown(){
        const { container } = this.refs
        //container.scrollTop = container.scrollHeight
    }

     
    /*
    *	Connect to and initializes the socket.
    */
    initSocket = (token)=>{
        
            const socket = io(socketUrl)
            socket.on('connect', ()=>{
                console.log("Connected");
            })
            this.setState({socket})
            const { nickname } = this.state
            socket.emit(VERIFY_USER, this.state.curr_user_name+token, this.setUser)
    }
    
    setUser = ({user, isUser})=>{
        if(isUser){
            this.setError("User name taken")
        }else{
            this.setError("")
            this.setUserProps(user)
        }
    }
        
    setError = (error)=>{
        this.setState({error})
    }
    
    /*
    * 	Sets the user property in state 
    *	@param user {id:number, name:string}
    */	
    setUserProps = (user)=>{
        const { socket } = this.state;
        socket.emit(USER_CONNECTED, user);
        this.setState({user})
    }
    
    /*
    *	Reset the chat back to only the chat passed in.
    * 	@param chat {Chat}
    */
    resetChat = (chat)=>{
        return this.addChat(chat, true)
    }
    /*
    *	Adds chat to the chat container, if reset is true removes all chats
    *	and sets that chat to the main chat.
    *	Sets the message and typing socket events for the chat.
    *	
    *	@param chat {Chat} the chat to be added.
    *	@param reset {boolean} if true will set the chat as the only chat.
    */
    addChat = (chat, reset)=>{
            const { socket } = this.state
            const { chats } = this.state
            const newChats = reset ? [chat] : [...chats, chat]
            this.setState({chats:newChats, activeChat1:reset ? chat : this.state.activeChat1})
            const messageEvent = `${MESSAGE_RECIEVED}-${chat.id}`
            const typingEvent = `${TYPING}-${chat.id}`
             
            socket.on(typingEvent, this.updateTypingInChat(chat.id))
            socket.on(messageEvent, this.addMessageToChat(chat.id))
    }
    /*
    *	Updates the typing of chat with id passed in.
    *	@param chatId {number}
    */
    updateTypingInChat = (chatId) =>{
        return ({isTyping, user})=>{
            if(user !== this.props.user.name){
                const { chats } = this.state
                let newChats = chats.map((chat)=>{
                        if(chat.id === chatId){
                                if(isTyping && !chat.typingUsers.includes(user)){
                                        chat.typingUsers.push(user)
                                }else if(!isTyping && chat.typingUsers.includes(user)){
                                        chat.typingUsers = chat.typingUsers.filter(u => u !== user)
                                }
                        }
                        return chat
                })
                this.setState({chats:newChats})
            }
        }
    }
    
    /*
    * 	Returns a function that will 
    *	adds message to chat with the chatId passed in. 
    *
    * 	@param chatId {number}
    */
    addMessageToChat = (chatId)=>{
        return message => {
            
              console.log('here add message to chat')
                const { chats } = this.state
                let newChats = chats.map((chat)=>{
                if(chat.id === chatId)
                    chat.messages.push(message)
                    return chat
                })
            this.setState({chats:newChats})
        }
    }

    

    toggleClass(e) {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    handleAction(e, action) {
        if(e && action) {
           let data = {
               id: e._id,
               jobId: e.jobId,
               userId: e.userId,
               fromUserId: e.fromUserId,
               action: action
           }    
           store.dispatch(notifyAction(data)).then(
               res => {
                    if(res.data.action == "accept") {
                        
                        store.dispatch(getUserNotification())
                        this.setState({
                            status: res.data.action
                        });
                        NotificationManager.success(res.data.message,'',2000);
                        
                    } else if (res.data.action == "decline") {
                        this.setState({
                            status: res.data.action
                        });
                        NotificationManager.info(res.data.message,'',2000);
                    }
               }
           )
        }
    }

    showChat() {
        const currentState = this.state.chatMessanger;
        this.setState({ chatMessanger: !currentState });
    }

    handleChat(data,index) {
        
        if(this.state.userId == data.userId){
           var msg_data = {
                clientId    :   data.fromUserId,
                jobId       :   data.jobId,
            }
       }else{
            var msg_data = {
                clientId    :   data.userId,
                jobId       :   data.jobId,
            }
        }
        store.dispatch(setMessageStatus(msg_data));
        
        this.setState({
            index_active_msg:index,
            jobcoins:data.jobInfo.price
        });
        var current_user_id = data.userId;
        var fromUserId = data.fromUserId;
        var jobid = data.jobId;
        
        var get_msg_data = {
            userId      :   current_user_id,
            clientId    :   fromUserId,
            jobId       :   jobid
        } 
        
//        if(this.state.current_job_id != data.jobId){
            this.state.activeChat1.messages=[];
            this.props.dispatch(getMsg(get_msg_data));
//        }
       
        
        
        store.dispatch(getUnreadMsgCount());// get unread count messages
        this.setState({ current_user_id: current_user_id });
        this.setState({ current_userfrom_id: fromUserId });
        this.setState({ current_job_id: jobid });
        const currentState = this.state.active;
       
        this.setState({
            activeChat: data,
            active: !currentState
        })
        
    }

    handleHire(userid, jobid) {
        let data = {
            userid,
            jobid
        }
        
        store.dispatch(hireUser(data)).then(
            res => {
                if(res.data.error === false) {
                    NotificationManager.success(res.data.message, '', 2000);
                        store.dispatch(getUserNotification(this.state.userId)).then(
                            res => {
                            this.data = res.data; 
                            this.props.router.push('/myjobs');
                        }
                        );
					
                }
            }
        )
    }

    ratingChanged(newRating) {
        this.setState({
            rating1: newRating
        });
    }


    onFeedbackSubmit(e) {
        e.preventDefault();
        
        if(this.state.rating1 === 0) {
            NotificationManager.error("Please provide rating", '', 2000);
        }
        if(this.state.feedback === '') {
            NotificationManager.error("Please provide Feedback", '', 2000);
        }
        
        if(this.state.coins < this.state.jobcoins){
            NotificationManager.error("Please buy more coins to close this job!", '', 2000);
            return false;
        }
              
        if(this.state.feedback && this.state.rating1) {
            let data = {
                jobId: this.state.activeChat.jobInfo._id,
                rating: this.state.rating1,
                feedback: this.state.feedback,
                current_user_id :this.state.current_user_id,
                current_userfrom_id : this.state.current_userfrom_id,
                jobcoins:this.state.jobcoins
                
            }
            store.dispatch(closeJob(data)).then(
                res => {
                    if(res.data.error === false) {
                        this.setState({
                            activeChat: [],
                            modal: false
                        });
                        NotificationManager.success("Successfully Closed!", '', 2000);
                        setTimeout(() => {
                            this.props.router.push('/myjobs');
                        }, 700);
                    }
                }
            )
        }
    }

    
    onChange(e) {
        this.setState({ 
            feedback: e.target.value 
        });
    }
    
    sendmsg(){
   
        this.setState({message:this.state.textareavalue})
        this.sendMessage1(this.state.textareavalue)
       

    }
    
    handleSubmit = (e)=>{
        e.preventDefault()
        this.sendMessage1()
        this.setState({message:""})
        $(".thread-container").animate({scrollTop: $(document).height() + $(".thread-container").height()});
    }
    
    sendMessage1 = (msg)=>{
        
        /*Starts Save Messages in Db */   
        
        if(this.state.userId == this.state.current_user_id){
           var data = {
                userId      :   this.state.current_user_id,
                clientId    :   this.state.current_userfrom_id,
                jobId       :   this.state.current_job_id,
                msg         :   this.state.message,
                image       :   this.state.login_user_img
            }
       }else{
            var data = {
                userId      :   this.state.current_userfrom_id,
                clientId    :   this.state.current_user_id,
                jobId       :   this.state.current_job_id,
                msg         :   this.state.message,
                image       :   this.state.login_user_img
            }
        }
        store.dispatch(saveMsg(data));
        /*End Save Messages in Db */  
        this.sendMessage(this.state.message)
    }
    /*
    *	Adds a message to the specified chat
    *	@param chatId {number}  The id of the chat to be added to.
    *	@param message {string} The message to be added to the chat.
    */
    sendMessage = (message)=>{
     
        if (this.state.userId == this.state.current_user_id){
            var userId = this.state.current_user_id;
            var clientId = this.state.current_userfrom_id;
            var job_id = this.state.current_job_id;
            var profile_image = this.state.login_user_img;
        }
        else{
            var userId = this.state.current_userfrom_id;
            var clientId = this.state.current_user_id;
            var job_id = this.state.current_job_id;
            var profile_image = this.state.login_user_img;
        }
        
        let chatId = this.state.activeChat1.id;
        const { socket } = this.state
       
        socket.emit(MESSAGE_SENT, {chatId, message, userId, clientId, job_id,profile_image})
    }
    
    
    sendTyping = () => {
        
        if(this.state.userId == this.state.current_user_id){
               var msg_data = {
                    clientId    :   this.state.current_userfrom_id,
                    jobId       :   this.state.current_job_id,
                }
        }else{
                var msg_data = {
                    clientId    :   this.state.current_user_id,
                    jobId       :   this.state.current_job_id,
                }
        }
        store.dispatch(setMessageStatus(msg_data));
        
        this.lastUpdateTime = Date.now()
        
        if (!this.state.isTyping){
            this.setState({isTyping:true})
            this.sendTypingProps(true)
            this.startCheckingTyping()
        }
    }

    /*
     *	startCheckingTyping
     *	Start an interval that checks if the user is typing.
     */
    startCheckingTyping = () => {
        console.log("Typing");
        this.typingInterval = setInterval(() => {
            if ((Date.now() - this.lastUpdateTime) > 300){
                this.setState({isTyping:false})
                this.stopCheckingTyping()
            }
        }, 300)
    }

    /*
     *	stopCheckingTyping
     *	Start the interval from checking if the user is typing.
     */
    stopCheckingTyping = () => {
        console.log("Stop Typing");
                if (this.typingInterval){
                clearInterval(this.typingInterval)
                this.sendTypingProps(false)
        }
    }
    
    /*
    *	Sends typing status to server.
    *	chatId {number} the id of the chat being typed in.
    *	typing {boolean} If the user is typing still or not.
    */
    sendTypingProps = (chatId, isTyping)=>{
        const { socket } = this.state
        socket.emit(TYPING, {chatId, isTyping})
    }

//    setActiveChatProps = (activeChat1)=>{
//        this.setState({activeChat1})
//    }
    
    handleChange(event) {
        this.setState({textareavalue: event.target.value});
    }
    
    onEnterPressFromSubmit = (e) => {
      if(e.keyCode == 13 && e.shiftKey == false) {
        e.preventDefault()
        this.sendMessage1()
        this.setState({message:""});
            $(".thread-container").animate({scrollTop: $(document).height() + $(".thread-container").height()});
      }
    }
    
    closeRequest(param_hiredId,param_appliedId,param_jobId,param_job_title,param_applied_username,param_applied_email){
        var msg = param_applied_username+' request to you closed the '+param_job_title+ ' job!';
        let hiredId = param_hiredId;
        let appliedId = param_appliedId;
        let jobId = param_jobId;
        let close_request_status = true;
        let close_job_title = param_job_title;
        
        let close_request_msg = msg;
        let data = {
                hiredId : hiredId,
                appliedId : appliedId,
                jobId : jobId,
                close_request_status:true,
                close_request_msg:close_request_msg,
                username:param_applied_username,
                email:param_applied_email,
                close_job_title:close_job_title,
                call_type: 'close_request'
            }
            
            store.dispatch(applyJob(data))
                .then(res => {
                    if(res) {
                        NotificationManager.success("your request has been submitted!", '', 2000);
                    } 
                });
      
    }
    
    msgapplyJob(...data){
        
        let apply_msg = data[4]+' applied for '+data[5]+' job!';
        let jobId       = data[0];
        let ownerId     = data[1];
        let ownerEmail  = data[2];
        let userId      = data[3];
        let userName    = data[4];
        let job_title   = data[5];
        let index_msg   = data[6];
        let msg_status  = 0; 
        let msg         = apply_msg;
        let job_applied_status   = 1;
        
        let apply_job_data = {
                userId,
                userName,
                jobId,
                ownerEmail, 
                ownerId,
                msg,
                job_title,
                msg_status,
                job_applied_status
            }
            
            store.dispatch(applyJob(apply_job_data))
                .then(res => {
                    if(res) {
                       NotificationManager.success("Thanks for Apply!", '', 2000);
                      
                         setTimeout(() => {
//                                window.location="/messages";
                                this.props.router.push('/myjobs')
                        }, 2000);
                       
                    } 
            });
            
            
            
       
        
       
            

    }

    render() {
        //const { messages, user, typingUsers } = this.props;
        if(this.state.activeChat1.messages !=undefined || this.state.activeChat1.messages !=''){
            var messages = this.state.activeChat1.messages;
        }
        var typingUsers = this.state.activeChat1.typingUsers;
        var user = this.state.user;
        const { message } = this.state
        const { error , Reducer } =this.props;
     
        
        if(this.props.chat_record.getMsg.data != undefined || this.props.chat_record.getMsg.data!=''){
            var chatmessages = this.props.chat_record.getMsg.data;
        }
        const { firstname, lastname, imageURL, hiredBtn ,rating,userId} = this.state;
        
    return(
            <div className="main">
                <Header />
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="breadcrumb">
                                <Link className="breadcrumb-item" to="/">Home</Link>
                                <span className="breadcrumb-item active">Messages</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section className="profile-content">
                        <div className="container">
                            <div className="outer">
                                <h2 className="profile-heading">DASHBOARD   
                                    <button data-toggle="modal" data-target="#buy-coin">BUY COINS</button>
                                </h2>
                                <ProfileSidebar 
                                    image={ imageURL }
                                    fName={ firstname }
                                    lName={ lastname }
                                    rating={ rating }
                                    id={ userId }/>
                                
                                <div className="inbox">
                                    <NotificationContainer/>  
                                    <div className="inbox-main">
                                        <h5>Inbox</h5>
                                        <ul>
                                            { this.props.notification.Notify.length === 0 && 
                                                <div>
                                                   <div>
                                                        <p>There is no message to show!</p>
                                                    </div>
                                                </div>
                                            }
                                            { this.props.notification.Notify.length > 0 && 
                                                this.props.notification.Notify.map((list, index)  => {
                                                    if(list.status === "unread" && list.jobInfo.status === "OPEN" && list.close_request_status !=true && (this.state.userId == list.userId) ) {
                                                        return (
                                                            <li 
                                                                className="notification"
                                                                key={index}>
                                                                <img src={  list.appliedUser.profileImg || "images/dummy-img.png" } alt="Profile Pic" /> 
                                                                <div className="notify-details camel_font">
                                                                    <h5> <span></span></h5>
                                                                    <p>{ list.message }</p>
                                                                </div>
                                                                <div className="actions">
                                                                    <button
                                                                        className="btn btn-success  btn-sm  btn-accept" 
                                                                        onClick={ this.handleAction.bind(this, list, "accept") }>Accept</button>
                                                                    <button 
                                                                        className="btn btn-danger  btn-sm btn-accept"
                                                                        onClick={ this.handleAction.bind(this, list, "decline") }>Decline</button>
                                                                </div>
                                                            </li>
                                                        );
                                                    }
                                                })
                                            }
                                            { this.props.notification.Notify.length > 0 && 
                                                this.props.notification.Notify.map((list, index)  => {
                                                    if(this.state.userId==list.userId){
                                                        if(list.status === "accept" && list.jobInfo.status === "OPEN" && (this.state.userId == list.userId || this.state.userId == list.fromUserId)) {
                                                            return (
                                                                <li 
                                                                    key={index}
                                                                    onClick={ this.handleChat.bind(this, list,index) }
                                                                    className={ index == this.state.index_active_msg ? "active-msgs" : "" }>
                                                                    <img src={ list.appliedUser.profileImg || "images/dummy-img.png" } alt="Profile Pic" />
                                                                    <div className="msg-details">
                                                                        <h5 className="camel_font">{ list.appliedUser.username } <span> { moment(list.createdAt).format('MMMM Do YYYY') }   </span></h5>
                                                                        <p>{ list.jobInfo.title }</p>
                                                                        {
                                                                            this.props.chat_record.unreadMsgCount.data != undefined && this.props.chat_record.unreadMsgCount.data.length > 0 &&
                                                                                this.props.chat_record.unreadMsgCount.data.map((unread_message_data, index) => {
                                                                                    if(list.jobInfo._id == unread_message_data._id.jobId && this.state.userId != unread_message_data._id.userId ){
                                                                                        return (<span key={index} className="badge badge-danger" >{unread_message_data.count}</span>)
                                                                                    }
                                                                          })
                                                                        }  
                                                                    </div>
                                                                </li>
                                                            );  
                                                        }   
                                                    }
                                                })
                                            }
                                            { this.props.notification.Notify.length > 0 && 
                                                this.props.notification.Notify.map((list, index)  => {
                                                    if(this.state.userId==list.fromUserId){
                                                        if(list.status === "accept" && list.jobInfo.status === "OPEN" && (this.state.userId == list.userId || this.state.userId == list.fromUserId)) {
                                                            return (
                                                                <li 
                                                                    key={index}
                                                                    onClick={ this.handleChat.bind(this, list,index) }
                                                                    className={ index == this.state.index_active_msg ? "active-msgs" : "" }>
                                                                    <img src={ list.userPost.profileImg || "images/dummy-img.png" } alt="Profile Pic" />
                                                                    <div className="msg-details">
                                                                        <h5 className="camel_font">{ list.userPost.username } <span> { moment(list.createdAt).format('MMMM Do YYYY') }   </span></h5>
                                                                        <p>{ list.jobInfo.title }</p>
                                                                        {
                                                                            this.props.chat_record.unreadMsgCount.data != undefined && this.props.chat_record.unreadMsgCount.data.length > 0 &&
                                                                                this.props.chat_record.unreadMsgCount.data.map((unread_message_data, index) => {
                                                                                    if(list.jobInfo._id == unread_message_data._id.jobId && this.state.userId != unread_message_data._id.userId ){
                                                                                        return (<span key={index} className="badge badge-danger" >{unread_message_data.count}</span>)
                                                                                    }
                                                                          })
                                                                        } 
                                                                    </div>
                                                                </li>
                                                            );
                                                        }   
                                                    }
                                                })
                                            }
                                            
                                            { this.props.notification.Notify.length > 0 && 
                                                this.props.notification.Notify.map((list, index)  => {
                                                    if(this.state.userId==list.userId){
                                                        if(list.status === "accept" && list.jobInfo.status === "CLOSE" && (this.state.userId == list.userId || this.state.userId == list.fromUserId)) {
                                                            return (
                                                                <li 
                                                                    key={index}
                                                                    onClick={ this.handleChat.bind(this, list,index) }
                                                                    className={ index == this.state.index_active_msg ? "active-msgs" : "" }>
                                                                    <img src={ list.appliedUser.profileImg || "images/dummy-img.png" } alt="Profile Pic" />
                                                                    <div className="msg-details">
                                                                        <h5 className="camel_font">{ list.appliedUser.username } <span> { moment(list.createdAt).format('MMMM Do YYYY') }   </span></h5>
                                                                        <p>{ list.jobInfo.title }</p>
                                                                        {
                                                                            this.props.chat_record.unreadMsgCount.data != undefined && this.props.chat_record.unreadMsgCount.data.length > 0 &&
                                                                                this.props.chat_record.unreadMsgCount.data.map((unread_message_data, index) => {
                                                                                    if(list.jobInfo._id == unread_message_data._id.jobId && this.state.userId != unread_message_data._id.userId ){
                                                                                        return (<span key={index} className="badge badge-danger" >{unread_message_data.count}</span>)
                                                                                    }
                                                                          })
                                                                        }  
                                                                    </div>
                                                                </li>
                                                            );  
                                                        }   
                                                    }
                                                })
                                            }
                                            { this.props.notification.Notify.length > 0 && 
                                                this.props.notification.Notify.map((list, index)  => {
                                                    if(this.state.userId==list.fromUserId){
                                                        if(list.status === "accept" && list.jobInfo.status === "CLOSE" && (this.state.userId == list.userId || this.state.userId == list.fromUserId)) {
                                                            return (
                                                                <li 
                                                                    key={index}
                                                                    onClick={ this.handleChat.bind(this, list,index) }
                                                                    className={ index == this.state.index_active_msg ? "active-msgs" : "" }>
                                                                    <img src={ list.userPost.profileImg || "images/dummy-img.png" } alt="Profile Pic" />
                                                                    <div className="msg-details">
                                                                        <h5 className="camel_font">{ list.userPost.username } <span> { moment(list.createdAt).format('MMMM Do YYYY') }   </span></h5>
                                                                        <p>{ list.jobInfo.title }</p>
                                                                        {
                                                                            this.props.chat_record.unreadMsgCount.data != undefined && this.props.chat_record.unreadMsgCount.data.length > 0 &&
                                                                                this.props.chat_record.unreadMsgCount.data.map((unread_message_data, index) => {
                                                                                    if(list.jobInfo._id == unread_message_data._id.jobId && this.state.userId != unread_message_data._id.userId ){
                                                                                        return (<span key={index} className="badge badge-danger" >{unread_message_data.count}</span>)
                                                                                    }
                                                                          })
                                                                        } 
                                                                    </div>
                                                                </li>
                                                            );
                                                        }   
                                                    }
                                                })
                                            }
                                        </ul>
                                    </div>
                                    { this.state.activeChat && this.state.activeChat.hasOwnProperty('jobInfo') &&
                                            
                                        <div className="messages-main">
                                            <h5>New Messages
                                                <div className="actions">
                                                    
                                                    { (this.state.activeChat.jobInfo.hiredUserId ===  this.state.activeChat.appliedUser._id && this.state.activeChat.jobInfo.status!='CLOSE') &&
                                                        <div>
                                                            {
                                                                (this.state.activeChat.jobInfo.hiredUserId == this.state.userId) &&
                                                                    <button className="btn btn-danger btn-sm btton-close" onClick={this.closeRequest.bind(this,this.state.activeChat.userId,this.state.activeChat.appliedUser._id,this.state.activeChat.jobId,this.state.activeChat.jobInfo.title,this.state.activeChat.appliedUser.username,this.state.activeChat.userPost.email)} >Close Request</button>
                                                            }  
                                                            {
                                                                (this.state.activeChat.jobInfo.hiredUserId != this.state.userId) &&
                                                                    <button className="btn btn-danger btn-sm btton-close" data-toggle="modal" data-target="#close-job">Close Job</button>
                                                            }
                                                        </div>
                                                    }
                                                    
                                                     { ((this.state.activeChat.jobInfo.hiredUserId == null || this.state.activeChat.jobInfo.hiredUserId == '') && this.state.activeChat.jobInfo.status!='CLOSE') &&
                                                        <div>
                                                            {
                                                                (this.state.activeChat.appliedUser._id == this.state.userId && this.state.activeChat.job_applied_status == '0') &&
                                                                    <button className="btn btn-danger btn-sm btton-close" onClick={this.msgapplyJob.bind(this,this.state.activeChat.jobId,this.state.activeChat.userId,this.state.activeChat.userPost.email,this.state.activeChat.appliedUser._id,this.state.activeChat.appliedUser.username,this.state.activeChat.jobInfo.title,this.state.index_active_msg)} >Apply Job</button>
                                                            }  
                                                            {
                                                                (this.state.activeChat.appliedUser._id == this.state.userId && this.state.activeChat.job_applied_status == '1' ) &&
                                                                <div>
                                                                    <span className="badge badge-success">Already Applied</span>
                                                                </div>
                                                            }  
                                                             
                                                        </div>
                                                    }
                                                    
                                                    { !this.state.activeChat.jobInfo.hasOwnProperty('hiredUserId') && this.state.userId != this.state.activeChat.appliedUser._id  && this.state.activeChat.jobInfo.status!='CLOSE' && this.state.activeChat.msg_status!=1 &&
                                                        <div>
                                                            <button className="btn btn-success btn-sm" onClick={ this.handleHire.bind(this, this.state.activeChat.appliedUser._id, this.state.activeChat.jobInfo._id,this.state.activeChat.userId) } >Hire</button>
                                                        </div>
                                                    }
                                                    { this.state.activeChat.jobInfo.status == 'CLOSE' &&
                                                        <div>
                                                            <span className="badge badge-danger">Job Closed</span>
                                                        </div>
                                                    }
                                                  
                                                </div>
                                            </h5>    
                                            <div className="msg-inner">
                                                <div className="msg-main-inner">
                                                    <div className="chat-content">
                                                         <h6><b>Job Title: { this.state.activeChat.jobInfo.title }</b></h6>
                                                    </div>
                                                </div>
                                                
                                            <div ref='container'className="thread-container">
                                                <div className="thread">
                                                    
                                                    {
                                                        chatmessages != undefined && chatmessages.length > 0 &&
                                                        chatmessages.map((message_data, index) => {
                                                            
                                                            if(this.state.current_job_id==message_data.jobId){
                                                                if(this.state.userId == message_data.userId){
                                                                     return (
                                                                            <div key={index} className='message-container1 right'>
                                                                                <div className="direct-chat-msg">
                                                                                    <div className="direct-chat-text">
                                                                                      {message_data.msg}
                                                                                    </div>
                                                                                </div>
                                                                              
                                                                            </div>
                                                                            )
                                                                }
                                                                if(this.state.userId == message_data.clientId){
                                                                    return (
                                                                            <div key={index} className='message-container1'>
                                                                                <div className="direct-chat-msg">
                                                                                    <div className="direct-chat-text">
                                                                                      {message_data.msg}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                        )
                                                                }
                                                            }
                                                        })
                                                    }
                                                    
                                                    {
                                                        this.state.activeChat1.messages != undefined  &&
                                                            this.state.activeChat1.messages.map((message_data,index)=>{
                                                                if(this.state.current_job_id==message_data.job_id){
                                                                    return ( 
                                                                        <div key={index} className={`message-container1  ${message_data.sender == user.name ? 'right' : 'left'}`}>
                                                                                    <div className="direct-chat-msg">
                                                                                    <div className="direct-chat-text">
                                                                                     <div className="message">{message_data.message}</div>
                                                                                    </div>
                                                                                </div>
                                                                              
                                                                        </div>
                                                                        
                                                                    )
                                                                }
                                                            })
                                                    }
                                                    {
                                                        typingUsers != undefined && typingUsers > 0 &&
                                                        typingUsers.map((name)=>{
                                                            return (
                                                                    <div key={name} className="typing-user">
                                                                            {`${name} is typing . . .`}
                                                                    </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                                { this.state.activeChat.jobInfo.status != 'CLOSE' &&
                                                    <div className="message-input 11">
                                                        <form onSubmit={ this.handleSubmit } ref={el => this.myFormRef = el} className="message-form">
                                                            <textarea 
                                                                id = "message"
                                                                ref = {"messageinput"}
                                                                type = "text"
                                                                className = "form-control"
                                                                value = { message }
                                                                autoComplete = {'off'}
                                                                placeholder = "New Message"
                                                                onKeyDown={this.onEnterPressFromSubmit}
                                                                onKeyUp = { e => { e.keyCode !== 13 && this.sendTyping() }}
                                                                onChange = {({target})=>{this.setState({message:target.value})}}

                                                            />
                                                            <button disabled = { message.length < 1 } type = "submit" className = "send"><i className="fa fa-paper-plane"></i></button>
                                                        </form>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                       
                        
                        <div className="modal fade show" id="close-job">
                            <div className="modal-dialog modal-dialog-centered">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h4 className="modal-title">Close Job</h4>
                                        <button type="button" className="close" data-dismiss="modal">
                                            <img src="images/cross.png" alt="path ok" />
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <h2 className="pop-h">Are you sure to close the job ?</h2>
                                        <form onSubmit={ this.onFeedbackSubmit }>
                                            <div className="form-group">
                                                <textarea 
                                                    placeholder="Feedback form" 
                                                    id="comment" 
                                                    rows="5" 
                                                    className="form-control"
                                                    name="feeback"
                                                    value= {this.state.feedback }
                                                    onChange={ this.onChange.bind(this) }></textarea>
                                            </div> 
                                            <div className="form-group">
                                            <ReactStars count={5} onChange={this.ratingChanged.bind(this)} size={48} value={this.state.rating1} color2={'#ffd700'} name="rating" />
                                            </div>
                                            <div className="form-group btns">
                                                <button type="submit" className="lgn-btn">Submit</button>
                                            </div>
                                        </form>
                                    </div>        
                                </div>
                            </div>
                        </div>
                    </section>
                    <ResetModal />
                <Footer />
            </div>
        );
    }
    
}

const mapStateToProps = store => {
    return {
        
        chat_record: store.chatReducer,
        user: store.userReducer,
        notification: store.notification,
        
    };
};
export default connect(mapStateToProps)(Messages);