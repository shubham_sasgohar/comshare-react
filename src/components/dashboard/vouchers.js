import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { voucherList , couponRedeemed , SavePayment} from '../../actions/voucher.action';
import moment from 'moment';
import store from '../../store';
import { getUserByToken } from '../../actions/user.action';
import { getPostedJob, getAppliedJob, getAwardedJob } from '../../actions/job.action';
import { UnsetNotification } from '../../actions/notification.action';
import ResetModal  from '../auth/resetModal';

import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';
import APP from '../../config';

class Vouchers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            imageURL: '',
            firstname: '',
            lastname: '',
            appliedJobs: [],
            awardedJobs: [],
            rating:'',
            voucher_id : '',
            email : '',
            query: '',
            vouchername: '',
            voucher_avail:'',
            points:'',
            coins:''
        }
        this.coupon = this.coupon.bind(this);
        this.redeem = this.redeem.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentDidMount() {
        document.title = "Comshare | Vouchers Page";

        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token));
        } else {
            this.props.router.push('/login');
        }
        store.dispatch(voucherList());
    }

    componentWillReceiveProps(nextProps) {  
        
       

        if(nextProps.user.isLogin) {
            this.setState({
                id: nextProps.user.isLogin.user._id || '',
                email: nextProps.user.isLogin.user.email || '',
                imageURL: nextProps.user.isLogin.user.profileImg || 'images/dummy-img.png',
                firstname: nextProps.user.isLogin.user.firstname || '',
                lastname: nextProps.user.isLogin.user.lastname || '',
                rating: nextProps.user.isLogin.user.rating || '',
                coins: nextProps.user.isLogin.user.coins || ''
            });
        }
    }
    
    coupon(id,name,points,startdate,enddate){
        let avail_date = startdate+' - '+enddate;
        this.setState({
            voucher_id : id,
            vouchername:name,
            voucher_avail:avail_date,
            points:points
        })
        
    }
    redeem(){
        if(this.state.coins < this.state.points){
            NotificationManager.error("please buy more coins to redeem this coupon!", '', 2000);
            return false;
        }
        let data = {
            voucher_id : this.state.voucher_id,
            userid : this.state.id,
            email : this.state.email,
            vouchername:this.state.vouchername,
            voucher_avail:this.state.voucher_avail,
            points:this.state.points,
        }
        
        store.dispatch(couponRedeemed(data)).then(
            res1 => {
            NotificationManager.success("Coupon redemeed successfully!", '', 2000);
            store.dispatch(voucherList());
            window.location="/vouchers";
        })
    }
    onSearchChange(e) {
        this.setState({
            query: e.target.value
        });
    }
    
     onSearch(e) {
        e.preventDefault();
        let data ={
            query : this.state.query
        }
        store.dispatch(voucherList(data));
    }
    

    render() {
        console.log(this.props.vouchers.voucherList.data);   
        let parentObj = this;
        const { firstname, lastname, imageURL, postedJobs, appliedJobs, awardedJobs ,rating,id } = this.state;
        return (
            <div className="main">
                <Header />
                <NotificationContainer/>        
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">Home</Link>
                            <span className="breadcrumb-item active">Vouchers</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content">
                    <div className="container">
                        <div className="outer">
                            <h2 className="profile-heading">DASHBOARD   <button  className="mrt5" data-toggle="modal" data-target="#buy-coin">BUY COINS</button><button className="reedme-vou">REDEEM VOUCHERS</button></h2>
                            <ProfileSidebar 
                                image={ imageURL }
                                fName={ firstname }
                                lName={ lastname }
                                rating={ rating }
                                id={ id }
                                />
                            
                            <div className="jobs-main vouchers-main">
                            
                                <h5>Vouchers</h5>
                                    <div className="search-field col-md-4 search_voucher">
                                        <form onSubmit={ this.onSearch }>
                                            <input 
                                                type="text" 
                                                placeholder="search.."
                                                name="search"
                                                value={ this.state.query }
                                                onChange={this.onSearchChange}
                                                />
                                            <button type="submit"><i className="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    
                                    { this.props.vouchers.voucherList.data != undefined && 
                                        this.props.vouchers.voucherList.data.map((list, index)  => {
                                              return(
                                                <div className="col-md-4 col-sm-6 float-left" key={index}>
                                                    <div className="vouchers">
                                                        <div className="vou-logo">
                                                          <img src={list.image=='' ? "images/logo-1.png" : `${APP.API_BASEURL}/vouchers_images/${list.image}` } alt="Vouchers" />
                                                          <span>{list.points} Points</span>
                                                        </div>
                                                        <h6>{list.name}</h6>
                                                        <p>Zipcoce: {list.zipcode}</p>
                                                        <p dangerouslySetInnerHTML={{__html: list.desc}} ></p>
                                                        <p>{moment(list.startDate).format('ll')} - {moment(list.endDate).format('ll')}</p>
                                                        <button data-toggle="modal" onClick={()=>parentObj.coupon(list._id,list.name,list.points,moment(list.startdate).format('ll'),moment(list.endDate).format('ll'))} data-target="#redeem-coupon-popup">REDEEM</button>        
                                                    </div>
                                                </div>
                                            )  
                                        })
                                   }
                            </div>
                        </div>
                    </div>
                </section>
                <div className="modal fade show" id="redeem-coupon-popup">
                    <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Redeem</h4>
                            <button type="button" className="close" data-dismiss="modal">
                                <img src="images/cross.png" alt="path ok" />
                            </button>
                        </div>
                        
                        <div className="modal-body">
                            <h2 className="pop-h">Are you sure you want to redeem this coupon ?</h2>
                            <form action="/action_page.php">
                            <div className="form-group forms-btn btns">
                               <button type="button" onClick={this.redeem} data-dismiss="modal" className="lgn-btn">Yes, I'm sure</button>
                            </div>
                                    
                            </form>
                        </div>        
                    </div>
                    </div>
                </div>
                <ResetModal />
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        jobs: store.jobReducer,
        notification: store.notification,
        vouchers: store.voucherReducer
    };
};

export default connect(mapStateToProps)(Vouchers);