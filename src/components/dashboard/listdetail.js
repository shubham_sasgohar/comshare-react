import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { push } from 'react-router-redux';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import moment from 'moment';

import store from '../../store';
import { getUserByToken } from '../../actions/user.action';
import { applyJob, getAppliedJob } from '../../actions/listing.action';
import { UnsetNotification, getUserNotification } from '../../actions/notification.action';
import { saveMsg} from '../../actions/chat';
import Header from '../Header';
import Footer from '../Footer'; 
import LoginModal from "../popupmodal/login";
import LoginPopup  from '../popupmodal/loginpopup';
import { getListing } from '../../actions/listing.action';
const nl2br = require('react-nl2br');

class ListDetail extends Component {
    constructor(props) {
        super(props);

        //this.handleApply = this.handleApply.bind(this);
        this.toggle = this.toggle.bind(this);
        this.popupSendMsg1 = this.popupSendMsg1.bind(this);
        this.onChangepopupSendMsg1 = this.onChangepopupSendMsg1.bind(this);
        this.state = {
            modal: false,
            userId: '',
            userName: '',
            checkStatus: [],
            popupmessage:''
        }
    } 


    componentDidMount() {
        document.title = "Comshare | Services Detail";
        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token)).then(
                    (res) => {
                            this.setState({
                                userId: res.data.user._id,
                                userName: res.data.user.username
                            })
                        }
                    )
        }else{
            this.setState({
                button:  <button data-target="#login-pop" onClick={ this.props.toggleClass } data-toggle="modal">Apply for Job</button>,   
                buttonsendmsg:  <button data-target="#login-pop" onClick={ this.props.toggleClass } data-toggle="modal">send message</button>    
            })
        }
    }
    
    componentWillReceiveProps(nextProps) { 

        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    
    popupSendMsg1(){
        const { userId, userName } = this.state;
        let jobId = this.props.listing.selected_item._id;
        let ownerId = this.props.listing.selected_item.userinfo._id;
        let ownerEmail = this.props.listing.selected_item.userinfo.email;
        let title = this.props.listing.selected_item.title;
        let messsage = userName+' send message for '+title+' job!';
        let popupmsg = this.state.popupmessage;
		
        if(ownerId == userId){
            NotificationManager.error("You can't send message for your own job!", '', 2000);
        }else{
            if(this.state.popupmessage.trim()==''){
                    NotificationManager.error("You can't send blank message", '', 2000);
            }else{
                /*Send Message in chat table*/
                let data = {
                        userId      :   userId,
                        clientId    :   ownerId,
                        jobId       :   jobId,
                        msg         :   popupmsg,
                        image       :   ''
                }
                store.dispatch(saveMsg(data));
                
                let applydata = {
                    userId : userId,
                    userName:userName,
                    jobId: jobId,
                    ownerEmail:ownerEmail, 
                    ownerId : ownerId,
                    msg : messsage,
                }

                store.dispatch(applyJob(applydata))
                .then(res => {
					/* if(res) {
                        this.setState({
                            button: <h5  className="center">Thanks for Apply!</h5>,
                            button2: <h5 className="center">Thanks for Apply!</h5>,
                            modal: false
                        })
                    } */
                    NotificationManager.success("Thanks for Sending Message!", '', 2000);
                    window.location.reload();
                });
            }
        }
    }
    
    onChangepopupSendMsg1(e){
        this.setState({ 
            popupmessage: e.target.value 
        });
    }

    render() {
        const { userId, checkApplied, checkStatus } = this.state;
        const {  button, button2 , buttonsendmsg} = this.props;
        if(this.props.listing.selected_item.userinfo) {
         
            var { _id, title, startDate, endDate, dueDate, description ,mycount,pincode } = this.props.listing.selected_item;
            var { _id, username, firstname, bio, lastname, profileImg ,rating} = this.props.listing.selected_item.userinfo;
            
            if(firstname && lastname) {
                var fullName = firstname + ' ' + lastname;
            }
        }
        return (
            <div>
                <div className="details-view ">
                    <div className="detail-header">
                        Detailed View
                        <button id="cls"  onClick={ this.props.toggleClass }><img src="images/cross.png"  alt="Close" /></button>
                    </div>
                    <div className="details-body">
                        <div className="det-imag">
                            <img src={ profileImg || 'images/dummy-img.png' } alt="user pic" />
                        </div>
                        <div className="det-name">
                            <h6 className="camel_font"><a href={'userprofile?id='+this.state.userId}>{ firstname + ' ' + lastname  || username } </a></h6>
                            { rating == '5' &&
                                <div className="rating five">
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>  
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                </div>
                            }
                            { rating == '4' &&
                                <div className="rating four">
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                </div>
                            }
                            { rating == '3' &&
                                <div className="rating three">
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                </div>
                            }
                            { rating == '2' &&
                                <div className="rating two">
                                    <i className="fa fa-star star_color"></i>
                                    <i className="fa fa-star star_color"></i>
                                </div>
                            }
                            { rating == '1' &&
                                <div className="rating one">
                                    <i className="fa fa-star star_color"></i>
                                </div>
                            }
                        </div>
                        <span className="det-review">{mycount !=undefined ?mycount : '0'} Reviews</span>
                        <div className="detail-description">
                            { bio }
                            <p><i className="fa fa-user"></i>{mycount !=undefined ?mycount : '0'} Jobs completed</p>
                            <p>{pincode}</p>
                        </div>
                        <div className="task-detail">
                            <h6>{ title }</h6>
                            {nl2br(description)}
                        </div>
                        <div className="task-detail">
                            { dueDate && 
                                <div>
                                <h6>Due Date</h6>
                                { moment(dueDate).format('MMMM Do YYYY') }  
                                </div>
                            }
                            { startDate && 
                                <div>
                                <h6>Start Date</h6>
                                { moment(startDate).format('MMMM Do YYYY')    }
                                <br/>
                                </div>
                            }
                            { endDate && 
                                <div>
                                <h6>End Date</h6>
                                { moment(endDate).format('MMMM Do YYYY')    }
                                </div>
                            }
                        </div>
                        <div className="details-btns">
                        <NotificationContainer/>  
                            {buttonsendmsg}
                            { button }
                        </div>
                    </div>
                </div>
                <div className="modal fade show" id="accept-job">
                    <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Apply For Job</h4>
                            <button type="button" className="close" data-dismiss="modal">
                                <img src="images/cross.png" alt="path ok" />
                            </button>
                        </div>
                        
                        <div className="modal-body">
                            <h2 className="pop-h">Are you sure to accept service ?</h2>
                            {/*<p className="pop-p"><img src="images/coin.png" alt="Cost" />  Not enough coins </p> */}
                            <form action="/action_page.php">
                            <div className="form-group forms-btn btns">
                                { button2 }
                            </div>
                                    
                            </form>
                        </div>        
                    </div>
                    </div>
                </div>
                
                <div className="modal fade" id="msg-pop1">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                              <h4 className="modal-title">Write a message</h4>
                              <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                            </div>
                            <div className="modal-body">
                              <form action="/action_page.php">
                                <div className="form-group">
                                  <textarea
                                    className="form-control"
                                    rows={5}
                                    placeholder="Your message" 
                                    defaultValue={""}
                                    onChange = {this.onChangepopupSendMsg1}
                                    />
                                </div> 
                                <div className="continue-links">
                                    <button type="button" data-dismiss="modal" onClick={ this.popupSendMsg1 } className="lgn-btn" data-toggle="modal" data-target="#registration">SEND MESSAGE</button>
                                </div>
                              </form>
                            </div> 
                        </div>
                    </div>
                </div>
                <LoginPopup/>
            </div>
        );
    }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification,
        listing: store.listingReducer
    };
};

export default connect(mapStateToProps)(ListDetail);