import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import axios from 'axios';

import { getUserInfo } from '../../actions/user.action';

import store from '../../store';
import Header from '../Header';
import Footer from '../Footer';
import APP from '../../config';
import moment from 'moment';

class Profileview extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            firstname: '',
            lastname: '',
            bio: '',
            rating: '',
            feedback_data: [],
            reviews_count:0
        }
    }

    componentDidMount() {
        document.title = `Comshare | ${this.state.username} profile`;
        let id = this.props.location.query.id;
        var reviews =0;
        if(id) {
            axios.post(`${APP.API_URL}/getUserByID?id=${id}`)
            .then(res => {
               this.setState({
                    username: res.data.user.username,
                    firstname: res.data.user.firstname,
                    lastname:  res.data.user.lastname,
                    bio: res.data.user.bio,
                    imageURL: res.data.user.profileImg,
                    rating: res.data.user.rating,
                    coins: res.data.user.coins,
               });
            })
            .catch(error => {
                console.log(error);
            });
          
            axios.post(`${APP.API_URL}/getUserByIDCloseJob?id=${id}`)
            .then(res1 => {
                reviews= res1.data.length
                this.setState({
                        feedback_data:res1.data,
                        reviews_count:reviews
                    });
            })
            .catch(error => {
                console.log(error);
            });
        }
     }

    componentWillUpdate(prevProps) {
        let oldProps = prevProps.location.query.id;
        let newProps = this.props.location.query.id;

        if(oldProps !== newProps) {
        }
    }

    render() {
        console.log(this.state.feedback_data)
        const { username , firstname, lastname, bio, rating, imageURL , coins} = this.state;
        return (
            <div className="main">
                <Header />
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <nav className="breadcrumb">
                                <Link className="breadcrumb-item" to="/">Home</Link>
                                <span className="breadcrumb-item active">Profile</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <section className="profile-content">
                        <div className="container">
                            <div className="outer">
                            <h2 className="profile-heading"> PROFILE </h2>
                                <div className="inner">
                                <div className="p-cover">
                                    <div className="p-profile">
                                        <img src={ imageURL=='' ? "images/dummy-img.png" :imageURL || "images/dummy-img.png" } width="140" height="140" alt="Profile"/>
                                        
                                    </div>
                                    </div>
                                    <div className="content-profile">
                                        <h1 className="camel_font"> { username } </h1>
                                        { rating == 5 &&
                                                 <div className="star">
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow"></i>
                                                        
                                                </div>
                                        }
                                        { rating == 4 &&
                                                 <div className="star">
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star"></i>
                                                </div>
                                        }
                                        { rating == 3 &&
                                                 <div className="star">
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star"></i>
                                                </div>
                                        }
                                        { rating == 2 &&
                                                 <div className="star">
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star"></i>
                                                </div>
                                        }
                                        { rating == 1 &&
                                                 <div className="star">
                                                        <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star" aria-hidden="true"></i>
                                                        <i className="fa fa-star"></i>
                                                </div>
                                        }
                                        <span className="coins">  <img alt="Cost" src="images/coin.png" /> {coins} Coins  </span>
                                        <p className="info ">
                                            { bio } 
                                        </p>
                                        <p className="job-complete"><i className="fa fa-user"></i> {this.state.reviews_count} Jobs completed </p>
                                        <p className="job-complete"><i className="fa fa-check-circle"></i> Vetted and  background checked </p>
                                    </div>
                                    <div className="review">
                                        <span className="r-view"> {this.state.reviews_count} Reviews </span>
                                        <span className="verify"><i className="fa fa-check-circle"></i>  All reviews from verified customers </span>
                                    </div>
                                    
                                  { this.state.feedback_data.length > 0 && 
                                        this.state.feedback_data.map((list, index)  => {
                                        if(list.status=='CLOSE'){
                                            return(
                                                    <div className="single-info" key={index}>
                                                        <div className="img-list">
                                                            <img src={ (list.clientInfo.profileImg =='' || list.clientInfo.profileImg ==undefined) ? "images/dummy-img.png" : list.clientInfo.profileImg || "images/dummy-img.png" } alt="Comshare" height="50" width="50"/>
                                                        </div>
                                                        <div className="detail">
                                                            <div className="name-star">
                                                                <span>{list.clientInfo.username} </span>
                                                                { parseInt(list.rating) == 5 &&
                                                                <span className="rating">
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>								
                                                                </span>
                                                                }
                                                                { parseInt(list.rating) == 4 &&
                                                                <span className="rating">
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star"></i>									
                                                                </span>
                                                                }
                                                                { parseInt(list.rating) == 3 &&
                                                                <span className="rating">
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star" aria-hidden="true"></i>
                                                                    <i className="fa fa-star"></i>									
                                                                </span>
                                                                }
                                                                { parseInt(list.rating) == 2 &&
                                                                <span className="rating">
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star " aria-hidden="true"></i>
                                                                    <i className="fa fa-star " aria-hidden="true"></i>
                                                                    <i className="fa fa-star"></i>									
                                                                </span>
                                                                }
                                                                { parseInt(list.rating) == 1 &&
                                                                <span className="rating">
                                                                    <i className="fa fa-star yellow" aria-hidden="true"></i>
                                                                    <i className="fa fa-star " aria-hidden="true"></i>
                                                                    <i className="fa fa-star " aria-hidden="true"></i>
                                                                    <i className="fa fa-star " aria-hidden="true"></i>
                                                                    <i className="fa fa-star"></i>									
                                                                </span>
                                                                }
                                                                
                                                            </div>
                                                            <p className="date">{ moment(list.updatedAt).format('MMMM Do YYYY') } </p> 
                                                            <p className="description">	{list.feedback} </p>
                                                        </div>
                                                    </div>
                                                )
                                            }
                                            })
                                    } 
                                </div>
                            </div>
                        </div>
                    </section>
                    
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Profileview);