import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import PropTypes from 'prop-types';
import store from '../../store';
import { getUserByToken, profileUpdate, profilePicUpload ,zipKeywordList,getUserReview } from '../../actions/user.action';
import { UnsetNotification, getUserNotification } from '../../actions/notification.action';
import ResetModal  from '../auth/resetModal';

import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';


class Profile extends Component {
    constructor(props,context) {
        super(props);
        this.context=context;
        this.state = {
            id: '',
            imageURL: '',
            firstname: '',
            lastname: '',
            phone: '',
            bio: '',
            address1: '',
            address2: '',
            city: '',
            state: '',
            zipcode: '',
            submitted: false,
            rating:'',
        }
        
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onFileUpload = this.onFileUpload.bind(this);
    }
    
    componentWillMount(){
        store.dispatch(zipKeywordList()); 
    }

    componentDidMount() {
//        store.dispatch(getUserReview(this.state.id)); 
        document.title = "Comshare | Profile Page";
        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token));
        } else {
            this.props.router.push('/login');
        }
    }

    componentDidUpdate(prevProps) {
       
    }

    componentWillReceiveProps(nextProps) {  
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }

        if(nextProps.user.isLogin) {
            
            this.setState({
                id: nextProps.user.isLogin.user._id || '',
                imageURL: nextProps.user.isLogin.user.profileImg || 'images/dummy-img.png',
                firstname: nextProps.user.isLogin.user.firstname || '',
                lastname: nextProps.user.isLogin.user.lastname || '',
                bio: nextProps.user.isLogin.user.bio || '',
                zipcode: nextProps.user.isLogin.user.zipcode || '',
                address1: nextProps.user.isLogin.user.address1 || '',
                address2: nextProps.user.isLogin.user.address2 || '',
                city: nextProps.user.isLogin.user.city || '',
                state: nextProps.user.isLogin.user.state || '',
                phone: nextProps.user.isLogin.user.phone || '',
                rating: nextProps.user.isLogin.user.rating || ''
            });
        }
    }


    onSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        const { firstname, lastname, email, bio, zipcode, phone, address1, address2, submitted,city,state } = this.state;
        console.log(this.state);
        store.dispatch(profileUpdate(this.state));
    }


    onFileUpload(e) {
        let id = this.state.id;
        
        let file = e.target.files[0];

        if(file) {
            
            var FileSize = file.size / 1024 / 1024; // in MB
            if(file.type === "image/png" || file.type === "image/jpeg") {
                    if (FileSize > 3) {
                        NotificationManager.error('Images size exceeds 3 MB! ','',3000);
                        return;
                        
                    }else{
                        const data = new FormData();
                        data.append('file', e.target.files[0]);
                        data.append('filename', e.target.files[0].name);
                        data.append('_id', id);

                        store.dispatch(profilePicUpload(data));
                    }
            } else {
                NotificationManager.error('Only images are allowed! ','',3000);
                return;
            }
        }   
       
      
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        
        const { id, firstname, lastname, imageURL, phone, bio, address1, address2, city, state, zipcode,rating } = this.state;
        return (
            <div className="main">
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">Home</Link>
                            <span className="breadcrumb-item active">Profile</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content">
                    <div className="container">
                        <div className="outer">
                            <h2 className="profile-heading">{this.context.translate('MY PROFILE')}<button data-toggle="modal" data-target="#buy-coin">{this.context.translate('BUY COINS')}</button></h2>
                            <ProfileSidebar 
                                image={ imageURL }
                                fName={ firstname }
                                lName={ lastname }
                                rating={ rating }
                                id={ id }
                                />
                            <div className="create-profile">
                                <div className="col-md-12">
                                    <h4>{this.context.translate('Update your profile')}</h4>
                                    <p>{this.context.translate('Please enter your profile details')}</p>
                                    <NotificationContainer/>  
                                </div>
                                <form onSubmit={this.onSubmit}>
                                    <div className="col-md-6 form-group  float-left active-input">
                                        <input 
                                            type="text" 
                                            placeholder="First name" 
                                            name="firstname"
                                            value={firstname}
                                            maxLength="10"
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-6 form-group float-left  active-input">
                                        <input 
                                            type="text" 
                                            placeholder="Last name" 
                                            name="lastname" 
                                            value={lastname}
                                             maxLength="10"
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-12 form-group">
                                        <input 
                                            type="text"
                                            placeholder="Phone Number*" 
                                            name="phone"
                                            value={phone}
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-12 form-group">
                                        <textarea 
                                            type="text" 
                                            placeholder="Introduction"  
                                            rows="4"
                                            name="bio"
                                            value={bio}
                                            onChange={this.onChange}></textarea>
                                    </div>
                                    <div className="col-md-12 form-group">
                                        <input 
                                            type="text" 
                                            placeholder="Address*" 
                                            name="address1"
                                            value={address1}
                                            onChange={this.onChange}/>
                                        <input 
                                            type="text" 
                                            placeholder="" 
                                            className="scnd-add" 
                                            name="address2"
                                            placeholder="Address 2" 
                                            value={address2}
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-12  form-group">
                                    <input 
                                            type="text" 
                                            placeholder="" 
                                            className="scnd-add" 
                                            name="city"
                                            placeholder="City" 
                                            value={city}
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-12  form-group">
                                    <input 
                                            type="text" 
                                            placeholder="" 
                                            className="scnd-add" 
                                            name="state"
                                            placeholder="State" 
                                            value={state}
                                            onChange={this.onChange}/>
                                    </div>
                                    <div className="col-md-12 form-group">
                                        <select name="zipcode"  value={this.state.zipcode} onChange={this.onChange}>
                                            <option value="">Please choose a zipcode</option>
                                                                                            {
                                                this.props.user.zipKeywordList.data !=undefined &&
                                                    this.props.user.zipKeywordList.data.map((data,index)=> {
                                                        if(data.type=='zipcode'){
                                                            return(
                                                                <option key={index} value={data.value}>{data.value}</option>
                                                            )
                                                        }
                                                })
                                            }  
                                        </select>
                                    </div>
                                    <div className="form-btn">
                                        <button 
                                            className="save"
                                            type="submit">{this.context.translate('SAVE')}</button>
                                        <button type="button" className="cancel">{this.context.translate('CANCEL')}</button>
                                    </div>
                                </form>
                            </div> 
                            <div className="upload-pic">
                                <img src={ imageURL } alt="Upload"  width="120" height="120" />
                                <label className="btn-upload" >
                                    Browse
                                    <input 
                                        className="isVisuallyHidden" 
                                        id="upload" 
                                        type="file" 
                                        onChange={this.onFileUpload} />
                                </label>
                            </div>
                        </div>
                    </div>
                </section>
                <ResetModal />
                <Footer />
            </div>
        );
    }
}
Profile.contextTypes = {
  translate: PropTypes.func
}
const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Profile);