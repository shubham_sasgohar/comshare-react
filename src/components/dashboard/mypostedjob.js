import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';

import moment from 'moment';
import store from '../../store';
import { getUserByToken } from '../../actions/user.action';
import { getPostedJob, getAppliedJob } from '../../actions/job.action';
import { UnsetNotification } from '../../actions/notification.action';
import ResetModal  from '../auth/resetModal';

import Header from '../Header';
import Footer from '../Footer';
import ProfileSidebar from './profileSidebar';

class MyPostedJob extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userId: '',
            imageURL: '',
            firstname: '',
            lastname: '',
            postedJobs: [],
            rating:'',
            job_title:'',
            job_status:'',
            job_description:'',
            client_user:'',
            client_user_email:'',
            client_rating:'',
            client_img:''
        }
        this.showpopup = this.showpopup.bind(this);
    }

    componentDidMount() {
        document.title = "Comshare | MyPostedJob Page";

        let token = JSON.parse(localStorage.getItem('token'));
        if(token) {
            store.dispatch(getUserByToken(token));
        } else {
            this.props.router.push('/login');
        }
        
        if(this.props.user.isLogin.user) {
            store.dispatch(getPostedJob(this.props.user.isLogin.user._id)).then(
                res => {
                    
                    this.setState({
                        postedJobs: res.data
                    });
                }
            );
        } else {
            this.props.router.push('/profile');
        }
    }

    componentDidUpdate(prevProps) {
    }

    componentWillReceiveProps(nextProps) {  
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
            }
            store.dispatch(UnsetNotification()); 
        }

        if(nextProps.user.isLogin) {
            this.setState({
                id: nextProps.user.isLogin.user._id || '',
                imageURL: nextProps.user.isLogin.user.profileImg || 'images/dummy-img.png',
                firstname: nextProps.user.isLogin.user.firstname || '',
                lastname: nextProps.user.isLogin.user.lastname || '',
                rating: nextProps.user.isLogin.user.rating || ''
            });
        }
    }

    showpopup(...data){
        this.setState({
                job_title: data[0],
                job_status: data[1],
                job_description:data[2],
                client_user:data[3],
                client_user_email:data[4],
                client_rating:parseInt(data[5]),
                client_img:data[6]
        });
         
    }

    render() {
        let parentObj = this;
        const { firstname, lastname, imageURL, postedJobs, appliedJobs,rating ,id} = this.state;
        return ( 
            <div className="main">
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <a className="breadcrumb-item" href="index.html">Home</a>
                            <span className="breadcrumb-item active">My Provided Jobs</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <section className="profile-content">
                    <div className="container">
                        <div className="outer">
                            <h2 className="profile-heading">
                                DASHBOARD
                                <button data-toggle="modal" className="mrt5" data-target="#buy-coin">BUY COINS</button>
                                <button className="reedme-vou">REDEEM VOUCHERS</button>
                            </h2> 
                             <ProfileSidebar 
                                image={ imageURL }
                                fName={ firstname }
                                lName={ lastname }
                                rating={ rating }
                                id={ id }
                                />
                              
                            
                            <div className="jobs-main">
                                <h5>My Posted Job</h5>
                                <div className="job-table">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">Sr. No</th>
                                            <th scope="col">Job Title</th>
                                            <th scope="col">Due date</th>
                                            <th scope="col">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { postedJobs &&
                                                postedJobs.map((data, index) => {
													
                                                    return (
                                                        <tr key={index} hidden={ data.status == "CLOSE" }>
                                                            <td scope="row"> { index + 1 }</td>
                                                            <td>{ data.title }</td>
                                                            <td>{ moment(data.dueDate).format('MMMM Do YYYY') }  </td>
                                                            <td>$ { data.price }  </td>
                                                            { data.hiredUserId && 

                                                                <td><span data-toggle="modal" onClick={()=>parentObj.showpopup(data.title,data.status,data.description,data.userinfo[0].username,data.userinfo[0].email,data.userinfo[0].rating,data.userinfo[0].profileImg)}   data-target="#create-job" className="badge badge-success active-badge">Active Job</span></td>
                                                            }
                                                            { !data.hiredUserId &&
                                                                <td className="view-button"><span className="badge badge-success active-badge">Open</span></td>
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </section>
                 <div className="modal fade" id="create-job">
                    <div className="modal-dialog modal-dialog-centered">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h4 className="modal-title">INFO</h4>
                          <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                        </div>
                        <div className="modal-body modal-body-job-info">
                        <div className="job_info">
                            <div className="alert alert-success ">
                                <strong>JOB DESCRIPTION</strong> 
                            </div>
                             <div className="jobinfo-inner-div">
                            <div>
                                <h6 className="camel_font">Job Title : { this.state.job_title }</h6>
                            </div>
                            <div className="task-detail1">
                            Description
                            <h6>{this.state.job_description}</h6>
                             </div>
                             <div className="task-detail1">
                            Status
                            <h6>{this.state.job_status}</h6>
                             </div>
                             <div className="task-detail1">
                              Coins
                                <h6>100 <img src="images/coin.png" alt="Cost"/></h6>
                             </div>
                             </div>
                        </div>
                         <div className="user_info">
                         <div className="alert alert-success ">
                                <strong>Client Info</strong> 
                            </div>
                          <div className="job-table jobtable-custom">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Rating</th>
                                            <th scope="col">Image</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                        <td className="camel_font">{this.state.client_user}</td>
                                        <td className="camel_font">{this.state.client_user_email}</td>
                                        <td >
                                        { this.state.client_rating == 5 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>								
                                        </span>
                                        }
                                        { this.state.client_rating== 4 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 3 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star" aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 2 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        { this.state.client_rating == 1 &&
                                        <span className="rating">
                                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star " aria-hidden="true"></i>
                                            <i className="fa fa-star"></i>									
                                        </span>
                                        }
                                        {
                                            this.state.client_rating == undefined || this.state.client_rating =='' || !this.state.client_rating &&
                                            <div className="rating four">
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                            </div>
                                        }
                                        </td>
                                        <td><img src={ (this.state.client_img == '' || !this.state.client_img ) ? 'images/dummy-img.png': this.state.client_img} height="50" width="50" alt="Cost"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>        
                      </div>
                    </div>
                  </div>
                <ResetModal />
                <Footer />
            </div>
            
        
  
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        jobs: store.jobReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(MyPostedJob);