import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import { UnsetNotification } from '../../actions/notification.action';
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import PropTypes from 'prop-types';
import store from '../../store';
import CreateListModal from "../popupmodal/newform/modal";

import Header from '../Header';
import Footer from '../Footer';

class Search extends Component {
    constructor(props,context) {
        super(props);
        this.context=context;
        this.state = {
            modal: false,
            query: ''
        };

        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps) {  
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',2000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',2000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',2000);
                break;
              default: 
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleChange() {
        this.setState({
            query: this.search.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        if(this.state.query.length > 1) {
            if(typeof this.state.query  === 'string') {
                let searchQuery = this.state.query.toLowerCase();
                this.props.router.push(`/services?query=${searchQuery}&type=HELP`);
            } else {
                this.props.router.push(`/services?query=${this.state.query}&type=HELP`);
            }
        } else {
            NotificationManager.error('Please enter query for search!','',3000);
        }
    }

    render() {
        return (
            <div className="main search-page">
                <Header />
                <div className="main-header">
                        <Link to="/"><img src={ "./images/logo.png" } alt="ComShare"/></Link>
                    </div>
                    <section className="banner">
                        <img src={ "./images/banner.jpg" } alt="ComShare" />
                        <div className="banner-text">
                            <div className="container">
                                <div className="row">
                                <div className="col-md-12">
                                    <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore <br/>
                                    Ut enim ad minim veniam quis nostrud exercitation ullamco laboris.</p>
                                    <div className="search-form">
                                        <form onSubmit={ this.onSubmit }>
                                            <input 
                                                type="text"
                                                ref={input => this.search = input} 
                                                placeholder={this.context.translate('Search by keyword or Zipcode')}
                                                onChange={this.handleChange}/>
                                            <button type="submit">{this.context.translate('Search')}</button>
                                        </form>
                                    </div>
                                    <div className="create-list">
                                        <Link to="" data-toggle="modal" data-target="#create-listing">{this.context.translate('create new listing')} </Link>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="how-it-work">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12 text-center">
                                    <h2 className="section-head">{this.context.translate('How it Works')}</h2>
                                    <p className="under-header">{this.context.translate('A simple process to get the Coupons you need')}</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="works-listing">
                                        <div className="work-img">
                                            <img src={ "./images/listing.png" } alt="Create Listing" />
                                            <span>1</span>
                                        </div>
                                        <h3>{this.context.translate('Create Listing')}</h3>
                                        <p>{this.context.translate('It`s easy Simply post a listing need completed and receive competitive coupons from us within minutes')}. </p>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="works-listing">
                                        <div className="work-img register">
                                            <img src={ "./images/register.png" } alt="Create Listing" />
                                            <span>2</span>
                                        </div>
                                        <h3>{this.context.translate('Register')}</h3>
                                        <p>{this.context.translate('By registering you confirm that you accept the Terms and Conditions and Privacy Policy')} </p>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="works-listing ">
                                        <div className="work-img recheck">
                                            <img src={ "./images/recheck.png" } alt="Create Listing" />
                                            <span>3</span>
                                        </div>
                                        <h3>{this.context.translate('Recheck')}</h3>
                                        <p>{this.context.translate('Find professionals you can trust by browsing their samples of previous work and reading their profile reviews')}. </p>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="works-listing">
                                        <div className="work-img pay">
                                            <img src={ "./images/pay.png" } alt="Create Listing" />
                                            <span>4</span>
                                        </div>
                                        <h3>Payment</h3>
                                        <p>{this.context.translate('With secure payments and thousands of reviewed professional to choose from comshare is the simplest and safest way to get work done online')}. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="about-us">
					<div className="container-fluid">
						<div className="row align-items-center">
							<div className="col-md-6 about-img" >
								<img src="images/about-us.jpg" alt="About US" />
							</div>
							<div className="col-md-6">
								<div className="content-text">
									<h4>{this.context.translate('About us')}</h4>
									<p>A simple process to get the Coupons you need</p>
									<p>The "About Us" or "Our Story" page should be about the passion driving your vision and the people that make it happen. Contrary to popular belief, this page’s content doesn't have to sound formal in order to gain credibility. Most people find it easier to connect with real human beings, so let the personality of your employees and the environment of your workplace shine through!</p>
			
									<p>We does a great job of showcasing the people behind the company and making them seem approachable to audiences, giving a more human-like quality to the brand.</p>
									<Link to="">VIEW MORE</Link>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="about-us vision">
					<div className="container-fluid">
						<div className="row align-items-center">
							
							<div className="col-md-6">
								<div className="content-text">
									<h4>{this.context.translate('Our Vision')}</h4>
									<p>A simple process to get the Coupons you need</p>
									<p>Our Vision page should be about the passion driving your vision and the people that make it happen. Contrary to popular belief, this page’s content doesn't have to sound formal in order to gain credibility. Most people find it easier to connect with real human beings, so let the personality of your employees and the environment of your workplace shine through!</p>
			
									<p>We does a great job of showcasing the people behind the company and making them seem approachable to audiences, giving a more human-like quality to the brand.</p>
									<Link to="">VIEW MORE</Link>
								</div>
							</div>
							<div className="col-md-6 about-img vision-img" >
								<img src={ "./images/vision.jpg" } alt="About US" />
							</div>
						</div>
					</div>
				</section>
                <CreateListModal />
                <Footer />
            </div>
        );
    }
}
Search.contextTypes = {
  translate: PropTypes.func
}

const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Search);