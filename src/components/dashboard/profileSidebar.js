import React, { Component } from 'react';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { UnsetNotification  } from '../../actions/notification.action';
import store from '../../store'; 
import { connect } from 'react-redux';
import {SavePayment} from '../../actions/voucher.action';

import { getUserByToken,getUserReview  } from '../../actions/user.action';
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import PropTypes from 'prop-types';
import PaypalExpressBtn from 'react-paypal-express-checkout';

class ProfileSidebar extends Component {
    constructor(props,context) {
        super(props);
        this.context=context;
        this.state = {
            imgUrl: '',
            firstname: '',
            lastname: '',
            rating: '',
            userId:'',
            usercoin:0,
            coinPayment:100
        }
         this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        let token = JSON.parse(localStorage.getItem('token'));
        if(token){
               store.dispatch(getUserByToken(token)).then(res => {
                   this.setState({
                       userId:res.data.user._id,
                       usercoin:res.data.user.coins
                   });
                   store.dispatch(getUserReview(res.data.user._id)); 
               });
        }
    }

    componentWillReceiveProps(nextProps) {  
        let oldProps = this.props;
        let newProps = nextProps;

        if(oldProps !== newProps) {
            this.setState({
                imgUrl: nextProps.image,
                firstname: nextProps.fName,
                lastname: nextProps.lName,
                rating: nextProps.rating
            });
        }
    }
    
    onChange(e) {
            var paymentcoin = e.target.value;
            var numberCoin = parseInt(paymentcoin);
            this.setState({ [e.target.name]: numberCoin });
        }

    render() {
           const onSuccess = (payment) => {
            // Congratulation, it came here means everything's fine!
            let data ={
                userId : this.state.userId,
                payeremail : payment.email,
                payerID : payment.payerID,
                paymentID : payment.paymentID,
                paymentToken : payment.paymentToken,
                paid : payment.paid,
                cancelled : payment.cancelled,
                amount : this.state.coinPayment,
                usercoin:this.state.usercoin
            }
            
             store.dispatch(SavePayment(data)).then(
                res1 => {
                NotificationManager.success("The payment was succeeded!", '', 2000);
                setTimeout(function(){ window.location.reload();  }, 1000);
                console.log("The payment was succeeded!", payment);
            })
           
           
            // You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data
        }		
        
        const onCancel = (data) => {
            // User pressed "cancel" or close Paypal's popup!
            console.log('The payment was cancelled!', data);
            NotificationManager.error("The payment was cancelled, Please try again later!", '', 2000);
            // You can bind the "data" object's value to your state or props or whatever here, please see below for sample returned data
        }	
        
        const onError = (err) => {
            // The main Paypal's script cannot be loaded or somethings block the loading of that script!
            console.log("Error!", err);
            NotificationManager.error("Something has been wrong, Please try again later!", '', 2000);
            // Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
            // => sometimes it may take about 0.5 second for everything to get set, or for the button to appear			
        }
        let env = 'sandbox'; // you can set here to 'production' for production
        let currency = 'USD'; // or you can set this value from your props or state  
        let total = this.state.coinPayment; // same as above, this is the total amount (based on currency) to be paid by using Paypal express checkout
        const client = {
            sandbox:    'AY4k993T8F5ozks8trNq6E4zJl9ckibcx6nB_5tcih6wL5jJvcI2vPtqzk7KRZBwMK-IWQ3jEepsTIkc',
            production: 'YOUR-PRODUCTION-APP-ID',
        }
        var pathname = window.location.pathname;
        const { firstname, lastname, imgUrl,rating} = this.state;
        return (
            <div>
                <div className="dashboard-sidebar">
                    <Link to={ `/userprofile?id=${this.state.userId}` }>
                    <div className="dashboard-pic">
                        <div className="profile-pic">
                            <img alt="profile" src={ imgUrl }  />        
                            <span className="online"></span>
                        </div>
                        <div className="profile-details">
                            <h6 className="camel_font">{ firstname + ' ' + lastname }</h6>
                            { rating == 5 &&
                            <div className="rating">
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>								
                            </div>
                            }
                            { rating== 4 &&
                            <div className="rating">
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star"></i>									
                            </div>
                            }
                            { rating == 3 &&
                            <div className="rating">
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star" aria-hidden="true"></i>
                                <i className="fa fa-star"></i>									
                            </div>
                            }
                            { rating == 2 &&
                            <div className="rating">
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star " aria-hidden="true"></i>
                                <i className="fa fa-star " aria-hidden="true"></i>
                                <i className="fa fa-star"></i>									
                            </div>
                            }
                            { rating == 1 &&
                            <div className="rating">
                                <i className="fa fa-star yellow" aria-hidden="true"></i>
                                <i className="fa fa-star " aria-hidden="true"></i>
                                <i className="fa fa-star " aria-hidden="true"></i>
                                <i className="fa fa-star " aria-hidden="true"></i>
                                <i className="fa fa-star"></i>									
                            </div>
                            }
                            {
                                rating == undefined || rating=='' &&
                                <div className="rating four">
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                </div>
                            }
                            {
                                this.props.user.user_reviews.length > 0 &&
                                    this.props.user.user_reviews.map((data,index)=> {
                                        return(<span key={index}>{data.myCount} {this.context.translate('Reviews')}</span>)
                                    })
                            }
                            
                        </div>
                    </div>
                    </Link>
                    <ul>
                        <li><Link to="/profile" activeClassName="msg-active"><i className="fa fa-user-circle"></i> {this.context.translate('MY PROFILE')} <img alt="profile" src="images/right.png" /></Link></li>
                        <li><Link data-toggle="modal" data-target="#resetpassword"><i className="fa fa-unlock"></i>{this.context.translate('Reset password')} <img alt="profile" src="images/right.png" /></Link></li>
                        <li><Link to="/myjobs" activeClassName="msg-active"><i className="fa fa-briefcase"></i> Job Preview<img alt="profile" src="images/right.png" /></Link></li>
                        <li>
                             <a href="/messages" className={pathname=='/messages' ? 'msg-active' : ''}>
                                <i className="fa fa-envelope-open"></i> Messages
                                <img alt="profile" src="images/right.png" />
                            </a> 
                        </li>
                        <li><Link to="/vouchers" activeClassName="msg-active"><i className="fa fa-tag"></i>{this.context.translate('vouchers')} <img alt="profile" src="images/right.png" /></Link></li>
                      
                    </ul>
                </div>
                 <div className="modal fade" id="buy-coin">
                    <div className="modal-dialog modal-dialog-centered">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h4 className="modal-title">Buy Coins</h4>
                          <button type="button" className="close" data-dismiss="modal"><img src="images/cross.png" alt="path ok" /></button>
                        </div>
                        <div className="modal-body">
                          <h2 className="pop-h">PAYMENT DETAILS</h2>
                          <p className="pop-p">Please enter your card details </p>
                          <form action="/action_page.php">
                            <div className="form-group select">
                               <select name="coinPayment"  onChange={this.onChange}>
                                <option value="100">100 Coins</option>
                                <option value="200">200 Coins</option>
                                <option value="300">300 Coins</option>
                              </select>
                            </div>
                            <div className="form-group forms-btn btns">
                             { <PaypalExpressBtn env={env} client={client} currency={currency} total={total} onError={onError} onSuccess={onSuccess} onCancel={onCancel} />}
                            </div>
                          </form>
                        </div>        
                      </div>
                    </div>
                   </div>
            </div>
        );
    }
} 
ProfileSidebar.contextTypes = {
  translate: PropTypes.func
}
const mapStateToProps = store => {
    return {
        user: store.userReducer
    };
};
export default connect(mapStateToProps)(ProfileSidebar);