import React, { Component } from "react";
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { setActiveLanguage } from 'react-localize-redux';
import { withTranslate, IntlActions } from 'react-redux-multilingual'
import PropTypes from 'prop-types'
import store from '../store';
import { getHeaderNotification , setHeaderReadMessage ,setHeaderCountReadMessage } from '../actions/listing.action';
import { getUserByToken } from '../actions/user.action';
import OutsideClickHandler from 'react-outside-click-handler';
import { getCmsPageData} from '../actions/user.action';
import cookie from 'react-cookies';

class Header extends Component {
    constructor(props,context) {
      super(props);
      this.context=context;
      this.state = {
           userId:'',
           isToggleOn: false,
           show: false,
           count_notif: '',
           username: '',
           imgurl: '',
           coins:''
        };
        this.openNotification = this.openNotification.bind(this);
        this.unreadNotification = this.unreadNotification.bind(this);
        this.changeLanguage = this.changeLanguage.bind(this);

    }
    
    
    componentWillMount(){
        let locale_cookie = cookie.load('localecookie');
        if(typeof locale_cookie == 'undefined'){
            store.dispatch(IntlActions.setLocale('en'));
        }else{
            store.dispatch(IntlActions.setLocale(locale_cookie));
        }
        store.dispatch(getCmsPageData());
        let token = JSON.parse(localStorage.getItem('token'));
         if(token){
            store.dispatch(getUserByToken(token)).then(res => {
                this.setState({
                    userId:res.data.user._id,
                    username:res.data.user.username,
                    coins:res.data.user.coins,
                    imgurl: (res.data.user.profileImg == '' || res.data.user.profileImg == undefined) ? 'images/dummy-img.png' :res.data.user.profileImg
                })
                    store.dispatch(getHeaderNotification({id:this.state.userId})).then(res => {
                         var count= 0;
                        res.map((res_data, index) => {
                            if(res_data.userId==this.state.userId && res_data.notificationStatus=='unread'  && res_data.countNotifStatus=='unread'){
                                count++;
                            }
                            if(res_data.jobInfo.hiredUserId==this.state.userId && res_data.jobInfo.notificationStatus=='unread' && res_data.jobInfo.status=='OPEN' && res_data.jobInfo.countNotifStatus=='unread'){
                                count++;
                            }
                            if(res_data.jobInfo.hiredUserId==this.state.userId && res_data.jobInfo.notificationStatus=='unread' && res_data.jobInfo.status=='CLOSE' && res_data.jobInfo.countNotifStatus=='unread'){
                                 count++;
                            }
                            
                        });
                        this.setState({count_notif:count})
                    });  
            });
        }
    }
    componentDidMount(){
        
        let token = JSON.parse(localStorage.getItem('token'));
         if(token){
         this.interval = setInterval(() => 
            store.dispatch(getUserByToken(token)).then(res => {
                
                this.setState({
                    userId:res.data.user._id,
                    username:res.data.user.username
                });
                    store.dispatch(getHeaderNotification({id:this.state.userId})).then(res => {
                         var count= 0;
                        res.map((res_data, index) => {
                            if(res_data.userId==this.state.userId && res_data.notificationStatus=='unread'  && res_data.countNotifStatus=='unread'){
                                count++;
                            }
                            if(res_data.jobInfo.hiredUserId==this.state.userId && res_data.jobInfo.notificationStatus=='unread' && res_data.jobInfo.status=='OPEN' && res_data.jobInfo.countNotifStatus=='unread'){
                                count++;
                            }
                            if(res_data.jobInfo.hiredUserId==this.state.userId && res_data.jobInfo.notificationStatus=='unread' && res_data.jobInfo.status=='CLOSE' && res_data.jobInfo.countNotifStatus=='unread'){
                                 count++;
                            }
                            
                        });
                        this.setState({count_notif:count})
                    });  
                    
            }),20000);
         
         
           
        }
        
    }
    
    openNotification(){
          this.setState(prevState => ({
            count_notif:'0'
        }));
        store.dispatch(setHeaderCountReadMessage({id:this.state.userId}))
  
    }
    unreadNotification(){
        
        this.setState(prevState => ({
            count_notif:'0'
        }));
        store.dispatch(setHeaderReadMessage({id:this.state.userId}))
    }
    
    componentWillReceiveProps(nextProps) {  
     
    }
   
   changeLanguage(locale){
        
        cookie.save('localecookie', locale, { path: '/' })
        store.dispatch(IntlActions.setLocale(locale));
    }

    render() {
        return (
            <div>
            <header>
                <div className="top-header">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-lg">
                          <Link className="navbar-brand" to="/">
                            <img src="images/small-logo.png" alt="ComShare" />
                          </Link>
                          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                            <span className="navbar-toggler-icon"></span>
                            <span className="navbar-toggler-icon"></span>
                          </button>
        
                          <div className="collapse navbar-collapse" id="navbarSupportedContent">
                           <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                             <i className="fa fa-times"></i>
                          </button>
                         
                          <ul className="navbar-nav mr-auto justify-content-end w100">
                            <li>
                                <Link activeClassName="current-item" to="/services">{this.context.translate('Market place')}</Link>
                            </li>
                            
                            {
                                this.props.user.cms_page_data != undefined  &&
                                        this.props.user.cms_page_data.data.map((cms_data,index)=>{
                                            
                                        if(cms_data.header==true){
                                            return <li  className="camel_font" key={index}><Link to={cms_data.page_type}>{cms_data.title}</Link></li>;
                                        }  
                                })
                            }
                            { !localStorage.getItem('token') &&
                                <li>
                                    <Link activeClassName="current-item" to="/register">Register</Link>
                                </li>
                            }
                            <li className="post-a-job">
                                <Link to="/vendor">{this.context.translate('REDEEM')}</Link>
                            </li>
                            <li  className="login">
                              { !localStorage.getItem('token') && 
                                <Link to="/login">{this.context.translate('Login')}</Link>
                              }
                            </li>
                            
                            { localStorage.getItem('token') &&
                            <ul className="nav navbar-nav navbar-right">
                            <li className='dropdown'>
                                 
                              <a href="#" onClick={this.openNotification} data-toggle="dropdown" className="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded=""><i className="fa fa-bell-o"></i> <b className="red">{this.state.count_notif}</b></a>
                                <ul className='dropdown-menu notify-drop'>
                                <div className="notify-drop-title">
                                  <div className="row">
                                    <div className="col-md-6 col-sm-6 col-xs-6">Notifications (<b>{this.state.count_notif}</b>)</div>
                                    <div className="col-md-6 col-sm-6 col-xs-6 text-right"><a href="#" onClick={this.unreadNotification} className="rIcon allRead"  data-placement="bottom">&times;</a></div>
                                  </div>
                                </div>
                                <div className="drop-content">
                                
                                {
                                    this.props.listing.header_notification.length != undefined && this.props.listing.header_notification.length > 0 &&
                                        this.props.listing.header_notification.map((notifications,index)=> {
                                            
                                        

                                                {
                                                    if(notifications.userId==this.state.userId && notifications.notificationStatus=='unread' ){
                                                        return(
                                                            <li key={index}>
                                                                <div>
                                                                    <p>{notifications.message}</p>
                                                                </div>
                                                            </li>
                                                        )  
                                                    }
                                                }
                                                {
                                                    if(notifications.jobInfo.hiredUserId==this.state.userId && notifications.jobInfo.notificationStatus=='unread' && notifications.jobInfo.status=='OPEN'){
                                                        return(
                                                            <li key={index}>
                                                                <div>
                                                                    <p>{notifications.jobInfo.notificationMessage}</p>
                                                                </div>
                                                            </li>
                                                        )  
                                                    }
                                                }
                                                {
                                                    if(notifications.jobInfo.hiredUserId==this.state.userId && notifications.jobInfo.notificationStatus=='unread' && notifications.jobInfo.status=='CLOSE'){
                                                        return(
                                                            <li key={index}>
                                                                <div>
                                                                    <p>{notifications.jobInfo.notificationMessage}</p>
                                                                </div>
                                                            </li>
                                                        )  
                                                    }
                                                }
                                          
                                    })      
                                }
                                    </div>
                                    <div className="notify-drop-footer text-center">

                                    </div>
                                  </ul>
                                </li>
                                </ul>
                            }
                        
                                { localStorage.getItem('token') &&
                                <span className="user-info">       
                                    <li className="no-coin"><img src="images/coin.png" alt="Cost" /> <span>{this.state.coins}</span></li>
                                    <li className="no-p"><a href="/profile"><img className="avtar" src={this.state.imgurl} /></a></li>
                                    <li className="dropdown">
                                        <a className="nav-link dropdown-toggle username-anchor" href="#" id="navbardrop" data-toggle="dropdown">
                                        {this.state.username} 
                                        </a>
                                        <div className="dropdown-menu">
                                        <Link className="dropdown-item" to="/profile">{this.context.translate('My Profile')}</Link>
                                        <Link className="dropdown-item" to="/myjobs">Job Preview</Link>
                                        <Link className="dropdown-item" to="/messages">Messages</Link>
                                        <Link className="dropdown-item" to="/vouchers">Vouchers</Link>
                                        <Link className="dropdown-item" to="/logout">{this.context.translate('Logout')}</Link>
                                        </div>
                                    </li>
                                </span>
                                }
                           
                                    <li className="white pointer" >
                                        <span onClick={()=>this.changeLanguage('en')} >EN |</span>
                                        <span onClick={()=>this.changeLanguage('de')} > DR</span>
                                    </li>

                            {/*<li>
                                    
                                    <img onClick={() => {store.dispatch(IntlActions.setLocale('en'))}} className="flag ml-5px"  width="20" src="usa.jpg" alt="USA Flag"/>
                                    <img onClick={() => {store.dispatch(IntlActions.setLocale('de'))}} className="flag margin6x"  width="20" src="germany.png" alt="Germany Flag"/>
                                <div className="languageDiv">
                                    <p><img onClick={() => {store.dispatch(IntlActions.setLocale('en'))}} className="flag ml-5px"  width="20" src="usa.jpg" alt="USA Flag"/></p>
                                    <p><img onClick={() => {store.dispatch(IntlActions.setLocale('de'))}} className="flag margin6x"  width="20" src="germany.png" alt="Germany Flag"/></p>
                                </div>
                            </li>*/}
                            </ul>
                            
                          </div>
                        </nav>
                    </div>
                </div>
             
            </header>
            </div>
        );
    }
}
Header.contextTypes = {
  translate: PropTypes.func
}
const mapStateToProps = store => {
  return {
      user: store.userReducer,
      locale: store.locale,
      notification: store.notification,
      listing: store.listingReducer
  };
};


export default connect(mapStateToProps)(Header);