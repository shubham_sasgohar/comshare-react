import React, { Component } from "react";
import { withTranslate, IntlActions } from 'react-redux-multilingual';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import store from '../store';
import { getCmsPageData} from '../actions/user.action';

class Footer extends Component {
    
    constructor(props,context) {
      super(props);
      this.context=context;
    }
    componentWillMount(){
        store.dispatch(getCmsPageData());
    }
    render() {
        return (
            <div>
                <footer>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="footer-logo">
                                    <a href="index.html"><img src="images/footer-logo.jpg" alt="Footer US" /></a>
                                    <p>&copy; 2018 ComShare. {this.context.translate('All Rights Reserved')}.</p>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="footer-links">
                                    <h5>{this.context.translate('Company')}</h5>
                                    <ul>
                                        {
                                            this.props.user.cms_page_data != undefined  &&
                                                this.props.user.cms_page_data.data.map((cms_data,index)=>{
                                                if(cms_data.footer==true && cms_data.footer_type == 'company'){
                                                    return <li  className="camel_font" key={index}><Link to={cms_data.page_type}>{cms_data.title}</Link></li>;
                                                }  
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="footer-links">
                                    <h5>{this.context.translate('Resources')}</h5>
                                    <ul>
                                        {
                                            this.props.user.cms_page_data != undefined  &&
                                                    this.props.user.cms_page_data.data.map((cms_data,index)=>{
                                                        
                                                    if(cms_data.footer==true  && cms_data.footer_type == 'resource'){
                                                        return <li  className="camel_font" key={index}><Link to={cms_data.page_type}>{cms_data.title}</Link></li>;
                                                    }  
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="footer-links">
                                    <h5>{this.context.translate('Social')}</h5>
                                    <ul>
                                        <li><a href=""><i className="fa fa-facebook"></i>Facebook</a></li>
                                        <li><a href=""><i className="fa fa-instagram"></i>Instagram</a></li>
                                    </ul>
                                </div>
                            </div>				
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}
Footer.contextTypes = {
  translate: PropTypes.func
}
const mapStateToProps = store => {
  return {
      user: store.userReducer
  };
};
export default connect(mapStateToProps)(Footer);
