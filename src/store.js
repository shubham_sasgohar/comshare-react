import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import reducers from './reducers/index';
import { IntlReducer as Intl, IntlProvider } from 'react-redux-multilingual';
const defaultLocale = 'en';


export default createStore(
  reducers,
  { Intl: { locale: defaultLocale }},
  applyMiddleware(
    thunkMiddleware,
    routerMiddleware(browserHistory)
  )
);
