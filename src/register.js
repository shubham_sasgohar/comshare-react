import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import store from '../../store';
import { UnsetNotification } from '../../actions/notification.action';
import { signup } from '../../actions/user.action';

import Header from '../Header';
import Footer from '../Footer';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            firstname: '',
            lastname: '',
            zipcode: '',
            username: '',
            password: '',
            confirmPassword: '',
            form: 'register'
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {     
        if(nextProps.notification.Notification.hasOwnProperty('type')){
            switch (nextProps.notification.Notification.type) {
              case 'info':
                NotificationManager.info('Info message');
                break;
              case 'success':
                NotificationManager.success(nextProps.notification.Notification.message,'',3000);
                break;
              case 'warning':
                NotificationManager.warning(nextProps.notification.Notification.message,'',3000);
                break;
              case 'error':
                NotificationManager.error(nextProps.notification.Notification.message,'',3000);
                break;
              default:
                break;
            }
            store.dispatch(UnsetNotification()); 
        }
    }

    onSubmit(e) {
        e.preventDefault();
        const { email, firstname, lastname, zipcode, username, password, confirmPassword } = this.state;
        
        if(email === ''){
			
            NotificationManager.error('Please Enter email ','',3000);
			return;
		}
		if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
			
            NotificationManager.error('Please Enter valid email ','',3000);
			return;
        }

        if(firstname === ''){
			
            NotificationManager.error('Please Enter First Name ','',3000);
			return;
		}
        
        if(zipcode === ''){
			
			NotificationManager.error('Please Enter Zipcode ','',3000);
            return;
        }
        
        if(username === ''){
			
			NotificationManager.error('Please Enter username ','',3000);
            return;
        }

        if(password === ''){
			
			NotificationManager.error('Please Enter Password ','',3000);
            return;
        }

        if(confirmPassword === ''){
			
			NotificationManager.error('Please Confirm Password ','',3000);
            return;
        }

        if(password !== confirmPassword) {
            NotificationManager.error('Password and Confirm password not match', '', 3000);
            return;
        }

        
        if(email && username && password) {
            this.setState({ submitted: true });
            store.dispatch(signup(this.state));
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

      
    render() {
        const { email, firstname, lastname, zipcode, username, password, confirmPassword, submitted } = this.state;
        return (
            <div className="main">
            <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <nav className="breadcrumb">
                            <Link className="breadcrumb-item" to="/">Home</Link>
                            <span className="breadcrumb-item active">Register</span>
                            </nav>
                        </div>
                    </div>
                </div>

                <section className="profile-content rating-main">
                    <div className="container login-inner">
                    
                        <div className="register-main">
                        <h2 className="profile-heading">Register </h2>
                            <p>Please enter your details to Register</p>
                            <NotificationContainer/>     
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                <input 
                                    type="email" 
                                    name="email" 
                                    placeholder="Email address" 
                                    id="email" 
                                    className="form-control" 
                                    value={email}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                <input 
                                    type="firstname" 
                                    name="firstname" 
                                    placeholder="First Name" 
                                    id="firstname" 
                                    className="form-control" 
                                    value={firstname}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                <input 
                                    type="lastname" 
                                    name="lastname" 
                                    placeholder="Last Name" 
                                    id="lastname" 
                                    className="form-control" 
                                    value={lastname}
                                    onChange={this.onChange}/>
                                </div>
                                {/* <div className="form-group">
                                <textarea 
                                    placeholder="Personal information" 
                                    name="info"
                                    id="comment" 
                                    rows="5"
                                    className="form-control"
                                    value={info}
                                    onChange={this.onChange}></textarea>
                                </div>  */}
                                <div className="form-group">
                                <input 
                                    type="zipcode" 
                                    name="zipcode" 
                                    placeholder="Zipcode" 
                                    id="zipcode" 
                                    className="form-control" 
                                    value={zipcode}
                                    onChange={this.onChange}/>
                                </div>	
                                <div className="form-group">
                                <input 
                                    type="text"
                                    name="username" 
                                    placeholder="Username" 
                                    id="name" 
                                    className="form-control" 
                                    value={username}
                                    onChange={this.onChange}/>
                                </div>		
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="password" 
                                    placeholder="Password" 
                                    id="pwd" 
                                    className="form-control" 
                                    value={password}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group">
                                <input 
                                    type="password" 
                                    name="confirmPassword" 
                                    placeholder="Confirm password" 
                                    id="c-pwd" 
                                    className="form-control" 
                                    value={confirmPassword}
                                    onChange={this.onChange}/>
                                </div>
                                <div className="form-group btns">
                                    <button className="lgn-btn" type="submit">create account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            <Footer />
            </div>
        )
    }
}
const mapStateToProps = store => {
    return {
        user: store.userReducer,
        notification: store.notification
    };
};

export default connect(mapStateToProps)(Register);