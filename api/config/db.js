var CONFIG = require('./app.config');
var mongoose = require('mongoose');

mongoose.connect(process.env.MONGOLAB_URI || `mongodb://${CONFIG.DB_USERNAME}:${CONFIG.DB_PASSWORD}@ds241530.mlab.com:41530/comshare`);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('DB connected!');
});

