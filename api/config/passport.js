// load all the things we need
var LocalStrategy = require('passport-local').Strategy;

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const utils = require('../utils/index');
const Cmspages = require('../models/cmspages');

// load up the user model
var User = require('../models/user');


// expose this function to our app using module.exports
module.exports = function (passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    // used to deserialize the user
    passport.deserializeUser(function (user, done) {
        done(null, user);
    });


    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },
            function (req, username, password, done) {

                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                User.findOne({
                    $or: [{'email': username}, {'username': username}]
                }).exec(function (err, user) {
                    if (err)
                        return done(err);

                    if (!user) {
                        return done(null, false, req.flash('loginMessage', 'No user found.'));
                    }

                    // if the user is found but the password is wrong
                    if (!user.validPassword(password))
                        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

                    if (user.admin === false)
                        return done(null, false, req.flash('loginMessage', 'Oops! Wrong Credentials.')); // create the loginMessage and save it to session as flashdata


                    // all is well, return successful user
//                    Cmspages.find().sort([['order', 1]]).exec(function (err, docs) {
//                        if (err)
//                            console.log(err);
//
//                        var db_cms_pages = docs;
//                        var token = utils.generateToken(user);
//                        user = utils.getCleanUser(user, docs);
//
//                        return done(null, user);
//                    });
                    var token = utils.generateToken(user);
                    user = utils.getCleanUser(user);

                    return done(null, user);


                });
            }));



}