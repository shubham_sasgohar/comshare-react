const path = require('path');
const fs = require('fs');

const User = require('../models/user');
const List = require('../models/listing');
const utils = require('../utils/index');


const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectId;

// All User
module.exports.getAllUser = (req, res) => { 
    /*User.find({}).sort([['_id', 1]]).exec(function(err, docs) { 
        if(err)
            console.log(err);
        // Set users
        res.render('pages/users', {
            admin : req.user,
            users: docs
        });
    });*/
    User.aggregate([
        {
            $lookup:
                    {
                        from: "listings",
                        localField: "_id",
                        foreignField: "userId",
                        as: "jobsinfo"
                    }
        },
        {$sort: {_id: 1}}
    ], function (err, docs) {
        if (err)
            throw err;
       
        res.render('pages/users', {
            admin: req.user,
            users: docs
        });
    });
}


// All Jobs
module.exports.getAllJobs = (req, res) => { 
    List.find({}).sort([['_id', 1]]).exec(function(err, docs) { 
        if(err)
            console.log(err);

        // Set users
        res.render('pages/jobs', {
            admin : req.user,
            jobs: docs,
        });
    });
} 


// Edit user
module.exports.editUser = (req, res) => {
  let body = req.body;

  // Update query
  User.findOneAndUpdate({ _id: body.id },
    { 
      $set: {
        "updatedAt": new Date(),
        "active": body.status
      }
    }, { upsert : true }, function(err, user) {
        if (err) {
            res.status(500).send(err);
        } else {
            user = utils.getCleanUser(user);
            res.status(200).send("User Updated!");
        }
    });
}

// Delete User
module.exports.deleteUser = (req, res) => {
    let body = req.body;

    // Delete query
    User.findOneAndDelete({ _id: body.id }, function(err, doc) {
        if(err)
            res.status(500).send(err);
        else
            res.status(200).send("Deleted Successfully!");
    });
}

// Edit job
module.exports.editJob = (req, res) => {
    let body = req.body;

     // Update query
    List.findOneAndUpdate({ _id: body.id },
        { 
            $set: {
                "updatedAt": new Date(),
                "active": body.status
            }
        }, { upsert : true }, function(err, list) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send("Job Updated!");
            }
        });
}

// Delete job
module.exports.deleteJob = (req, res) => {
    let body = req.body;

     // Delete query
    List.findOneAndDelete({ _id: body.id }, function(err, doc) {
        if(err)
            res.status(500).send(err);
        else
            res.status(200).send("Deleted Successfully!");
    });
}


// Get Job Details
module.exports.getJobDetail = (req, res) => {
    let body = req.body;

    List.aggregate([
        {
            $match: {
                '_id': ObjectId(body.id)
            }
        },
        {
          $lookup:
            {
              from: "users",
              localField: "hiredUserId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
        { "$unwind": "$userinfo" },
        {
            $lookup: 
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientinfo"
            }
        },
        { "$unwind": "$clientinfo" }
     ], function(err, lists) {
       if(err) throw err;
       
        res.send(lists);
       
     });    

}

// Get User Details
module.exports.getUserDetails = (req, res) => {
    let body = req.body;

    List.aggregate([
        {
            $match: {
                'hiredUserId': ObjectId(body.id)
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "hiredUserId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": "$userInfo" },
        {
            $lookup: {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientInfo"
            }
        },
        { "$unwind": "$clientInfo" },
       
    ], function(err, lists) {
        if(err) throw err;
        
         res.send(lists);
        
    });
}


// Get User Posted Job
module.exports.getUserPostedJobs = ((req, res) => {
    let body = req.body;

    List.aggregate([
        {
            $match: {
                'userId': ObjectId(body.id)
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": "$userInfo" },
    ], function(err, lists) {
        if(err) throw err;

        res.send(lists);
    });
});