const path  =   require('path');
const fs    =   require('fs');
const user  =   require('../models/user');

const mongoose  =   require('mongoose');
const ObjectId  =   require('mongodb').ObjectId;
const User = require('../models/user');
const bcrypt = require('bcrypt');
var registerstaffmember = fs.readFileSync(path.join(__dirname, '../templates/registerstaffmember.html'), 'utf8');
const CONFIG = require('../config/app.config');


// Node Mailer Function
const nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    secureConnection: true, // use SSL
    port: 587, // port for secure SMTP
    transportMethod: 'SMTP',
    auth: {
        user: CONFIG.SEMAIL_ID,
        pass: CONFIG.SEMAIL_PASS
    }
});

module.exports.addStaffMember = (req,res) => {
    console.log(req.user)
    res.render('pages/addstaffmember', {
            admin : req.user
        });
}

//module.exports.editStaffMember = (req,res) => {
//    let id=res.user._id;
//    User.find({_id:id},function(err,data){
//       if(err)
//           return err;
//       
//        res.render('pages/')
//    });
//    
//    
//    res.render('pages/addstaffmember', {
//            admin : req.user
//        });
//}

module.exports.staffMemberList = (req,res) => {
  
    let id = req.user._id;
    User.find({ _id : { $ne:id},admin:true},function(err,data){
        if(err)
            return err;
                
        res.render('pages/staffmemberlist', {
            admin : req.user,
            data  : data
        });
        
    });
    
  
}


module.exports.saveStaffMember = (req,res) => {
    console.log(req.body)
    var body = req.body;
    isUserUnique(body, function (err) {
        if (err) {
            return res.json({
                error: true,
                message: err
            });
        }
        var hash = bcrypt.hashSync(body.password.trim(), 10);
        var user = new User({
            username: body.username.trim(),
            firstname: body.firstname,
            lastname: body.lastname,
            email: body.email.trim(),
            password: hash,
            admin: true,
            sub_admin: true,
            isEmailVerified: true
        });


        user.save(function (err, user) {
            if (err)
                console.log(err);

//            var token = utils.generateToken(user);
//            user = utils.getCleanUser(user);
//
//            // setup email data with unicode symbols
            let mailOptions = {
                from: 'brstdev12@gmail.com',
                to: body.email.trim(), // list of receivers
                subject: 'Account Created ✔', // Subject line
                html: registerstaffmember.replace('USERNAME', body.username.trim()).replace('PASSWORD', body.password.trim()).replace('HREF', `href="${CONFIG.API_URL}/login"`)
            };
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error)
                    throw error
            });

            res.json({
                succes:true,
                message: 'Staff member saved successfully!'
            });
        });

    });
}

//utility func
function isUserUnique(reqBody, cb) {
    console.log(reqBody)
    var username = reqBody.username.toLowerCase() ? reqBody.username.trim() : '';
    var email = reqBody.email.toLowerCase() ? reqBody.email.trim() : '';

    User.findOne({
        $or: [{
                'username': new RegExp(["^", username, "$"].join(""), "i")
            }, {
                'email': new RegExp(["^", email, "$"].join(""), "i")
            }]
    }, function (err, user) {
        if (err)
            throw err;

        if (!user) {
            cb();
            return;
        }

        var msg;
        if (user.username === username) {
            msg = {};
            msg = '' + username + ' username is already taken!';
        }
        if (user.email === email) {
            msg = msg ? msg : {};
            msg = '' + email + ' email is already taken!';
        }

        cb(msg);
    });
}
