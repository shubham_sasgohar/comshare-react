const user = require('../models/user');
const Cmspages = require('../models/cmspages');
const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectId;


module.exports.getCmsPage = (req, res) => {

    Cmspages.find().sort([['order', 1]]).exec(function (err, docs) {
        if (err)
            console.log(err);
        
        res.json({
            data:docs
        });

    });
}
module.exports.addCmsPage = (req, res) => {
    let page_type = req.query.type;
    if(page_type == 'header' || page_type == 'footer'){
        res.render('pages/addheaderpage', {
            admin: req.user,
            page_type:page_type
        });
    }else{
        res.send('Page Not found')
    }

}


module.exports.cmspage = (req, res) => {
    let page_type = req.query.page;
    console.log(page_type)
    Cmspages.find({page_type: page_type}, function (err, data) {
        if (err) {
            console.log('errroorororo')
            return err;
        } else {
            if (data.length > 0) {
                res.render('pages/aboutus', {
                    data: data[0],
                    admin: req.user,
                    query:req.query
                });
            } else {
                res.send('page not found')
            }
        }

    });
}

module.exports.saveCmsPagesData = (req, res) => {

    Cmspages.findOneAndUpdate({page_type: req.body.hidden_page_type},
            {
                $set: {
                    title: req.body.page_name,
                    content: req.body.content,
                    page_type: req.body.new_page_type,
                    footer_type:req.body.footer_type
                }
            },
            function (err, data) {
                if (err)
                    return err;

                res.json({
                    success: true
                })
            }
    )
};


module.exports.addNewCmsPage = (req, res) => {
    
    Cmspages.find().sort([['order', -1]]).limit(1).exec(function (err, docs) {
        if (err)
            console.log(err);
        if (docs.length == 0) {
            var order_no = 1;
        } else {
            var order_no = docs[0]['order'] + 1;
        }
        var new_Cmspages = new Cmspages({
            title: req.body.page_name,
            page_type: req.body.hidden_page_type,
            content: req.body.content,
            order: order_no,
            header:  req.body.header,
            footer:  req.body.footer,
            footer_type : req.body.footer_type
        });

        new_Cmspages.save(function (err, user) {
            if (err)
                return res.send(err);
            res.json({
                success: true
            })
        });



    });


};

 