const path = require('path');
const fs = require('fs');

const List = require('../models/listing');
const Notify = require('../models/notification');
const ObjectId = require('mongodb').ObjectId;

module.exports.getPostedJOb = function(req, res) {
    let body = req.body;
    List.aggregate([
        {
            $match: {
                'userId': ObjectId(body.id)
            }
        },
        {
          $lookup:
            {
              from: "users",
              localField: "hiredUserId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
//        { "$unwind": "$userinfo" },
        {
            $lookup: 
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientinfo"
            }
        },
//        { "$unwind": "$clientinfo" }
     ], function(err, lists) {
       if(err) throw err;
        res.send(lists);
       
     }); 

//    List.find({ userId: body.id })
//        .sort({ '_id': -1 })
//        .limit(5)
//        .exec(function(err, doc) {
//            if(err)
//                return err;
//            
//            res.send(doc);
//        });
};

module.exports.getAppliedJOb = function(req, res) {
    let body = req.body;

    Notify.aggregate([
        {
            $lookup:
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientInfo"
            }
        },
        {
            "$unwind": "$clientInfo"
        },
        {
            $lookup: 
            {
                from: "users",
                localField: "fromUserId",
                foreignField: "_id",
                as: "appliedUser"
            }
        },
        {
            "$unwind": "$appliedUser"
        },
        {
            $lookup:
            {
                from: "listings",
                localField: "jobId",
                foreignField: "_id",
                as: "jobInfo"
            }
        },
        {
            "$unwind": "$jobInfo"
        }
    ], function(err, data) {
        let result = [];

        data.forEach(element => {
            if(element.fromUserId == body.id) {
                result.push(element);
            }
        });
        res.send(result);  
    });  
};

module.exports.getAwardedJob = (req, res) => {
    let body = req.body;
    
     List.aggregate([
        {
            $match: {
                'hiredUserId': ObjectId(body.id),
                'status':'OPEN'
            }
        },
        {
          $lookup:
            {
              from: "users",
              localField: "hiredUserId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
        { "$unwind": "$userinfo" },
        {
            $lookup: 
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientinfo"
            }
        },
        { "$unwind": "$clientinfo" }
     ], function(err, lists) {
       if(err) throw err;
        res.send(lists);
       
     }); 
    
//    List.find({ hiredUserId: body.id, status: { $eq: 'OPEN' }})
//        .sort({ '_id': -1 })
//        .exec(function(err, doc) {
//            if(err)
//                return err;
//            
//            res.send(doc);
//        });
        
}

module.exports.getUserCompletedJob = (req, res) => {
    let body = req.body;
    
    List.aggregate([
        {
            $match: {
                'hiredUserId': ObjectId(body.id),
                'status':'CLOSE'
            }
        },
        {
          $lookup:
            {
              from: "users",
              localField: "hiredUserId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
        { "$unwind": "$userinfo" },
        {
            $lookup: 
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientinfo"
            }
        },
        { "$unwind": "$clientinfo" }
     ], function(err, lists) {
       if(err) throw err;
        res.send(lists);
       
     });

//    List.find({ hiredUserId: body.id, status: { $eq: 'CLOSE' }})
//        .sort({ '_id': -1 })
//        .exec(function(err, doc) {
//            if(err)
//                return err;
//            
//            res.send(doc);
//        });
        
}


module.exports.getClientCompletedJob = (req, res) => {
    let body = req.body;
    
    List.aggregate([
        {
            $match: {
                'userId': ObjectId(body.id),
                'status':'CLOSE'
            }
        },
        {
          $lookup:
            {
              from: "users",
              localField: "hiredUserId",
              foreignField: "_id",
              as: "userinfo"
            }
        }, 
        { "$unwind": "$userinfo" },
        {
            $lookup: 
            {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientinfo"
            }
        },
        { "$unwind": "$clientinfo" }
     ], function(err, lists) {
       if(err) throw err;
        res.send(lists);
       
     });

//    List.find({ userId: body.id, status: { $eq: 'CLOSE' }})
//        .sort({ '_id': -1 })
//        .exec(function(err, doc) {
//            if(err)
//                return err;
//            
//            res.send(doc);
//        });
//        
}