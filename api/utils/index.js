var jwt = require('jsonwebtoken');
const CONFIG = require('../config/app.config');

function generateToken(user) {
  //Dont use password and other sensitive fields
  //Use fields that are useful in other parts of the app/collections/models
  var u = {
    firstname: user.firstname,
    username: user.username,
    admin: user.admin,
    _id: user._id.toString(),
    image: user.image,
    isEmailVerified: user.isEmailVerified //used to prevent creating posts w/o verifying emails
  };

  return token = jwt.sign(u, process.env.JWT_SECRET || CONFIG.JWT_SECRET, {
    expiresIn: 60 * 60 * 24 // expires in 24 hours
  });
}

function validateSignUpForm(values, callback) {
  var errors = {};
  var hasErrors = false;

  if (!values.username || values.username.trim() === '') {
    errors.username = 'Enter username';
    hasErrors = true;
  }
  if (!values.email || values.email.trim() === '') {
    errors.email = 'Enter email';
    hasErrors = true;
  }
  if (!values.password || values.password.trim() === '') {
    errors.password = 'Enter password';
    hasErrors = true;
  }

  if (callback) {
    callback(hasErrors && errors);
  } else {
    return hasErrors && errors;
  }
}

//strips internal fields like password and verifyEmailToken etc
function getCleanUser(user) {
  if(!user) return {};

  var u = user.toJSON();
  return {
    _id: u._id,
    username: u.username,
    email: u.email,
    admin: u.admin,
    sub_admin:u.sub_admin,
    createdAt: u.createdAt,
    updatedAt: u.updatedAt,
    firstname: u.firstname,
    lastname: u.lastname,
    profileImg: u.profileImg,
    bio: u.bio,
    zipcode: u.zipcode,
    address1: u.address1,
    address2: u.address2,
    city: u.city,
    state: u.state,
    phone: u.phone,
    rating: u.rating,
    coins:u.coins

//    db_cms_pages: docs
  }
}


module.exports = {
  validateSignUpForm: validateSignUpForm,
  getCleanUser: getCleanUser,
  generateToken: generateToken
}