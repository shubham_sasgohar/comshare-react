﻿/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * This file was added automatically by CKEditor builder.
 * You may re-use it at any time to build CKEditor again.
 *
 * If you would like to build CKEditor online again
 * (for example to upgrade), visit one the following links:
 *
 * (1) http://ckeditor.com/builder
 *     Visit online builder to build CKEditor from scratch.
 *
 * (2) http://ckeditor.com/builder/0992461aa82621c8a98294329c6593fd
 *     Visit online builder to build CKEditor, starting with the same setup as before.
 *
 * (3) http://ckeditor.com/builder/download/0992461aa82621c8a98294329c6593fd
 *     Straight download link to the latest version of CKEditor (Optimized) with the same setup as before.
 *
 * NOTE:
 *    This file is not used by CKEditor, you may remove it.
 *    Changing this file will not change your CKEditor configuration.
 */

var CKBUILDER_CONFIG = {
	skin: 'moono-lisa',
	preset: 'basic',
	ignore: [
		'.DS_Store',
		'.bender',
		'.editorconfig',
		'.gitattributes',
		'.gitignore',
		'.idea',
		'.jscsrc',
		'.jshintignore',
		'.jshintrc',
		'.mailmap',
		'.travis.yml',
		'bender-err.log',
		'bender-out.log',
		'bender.ci.js',
		'bender.js',
		'dev',
		'gruntfile.js',
		'less',
		'node_modules',
		'package.json',
		'tests'
	],
	plugins : {
		'about' : 1,
		'autogrow' : 1,
		'autolink' : 1,
		'basicstyles' : 1,
		'clipboard' : 1,
		'codesnippet' : 1,
		'colorbutton' : 1,
		'colordialog' : 1,
		'easyimage' : 1,
		'enterkey' : 1,
		'entities' : 1,
		'floatingspace' : 1,
		'font' : 1,
		'image2' : 1,
		'imageresize' : 1,
		'indentlist' : 1,
		'justify' : 1,
		'link' : 1,
		'list' : 1,
		'pbckcode' : 1,
		'sourcedialog' : 1,
		'tableresize' : 1,
		'tableresizerowandcolumn' : 1,
		'toolbar' : 1,
		'undo' : 1,
		'uploadfile' : 1,
		'wysiwygarea' : 1
	},
	languages : {
		'en' : 1
	}
};