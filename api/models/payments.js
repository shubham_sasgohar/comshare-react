const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const PaymentSchema = new mongoose.Schema({

    userId: mongoose.Schema.ObjectId,
    paid: String,
    cancel: String,
    payerId: String,
    paymentId: String,
    paymentToken: String,
    payerEmail: String,
    amount: String
});

PaymentSchema.plugin(timestamps);
const Paymentuser   =   mongoose.model('Paymentuser',PaymentSchema);
module.exports      = exports = Paymentuser;
