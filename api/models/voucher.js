const mongoose  =   require('mongoose');
const bcrypt    =   require('bcrypt');
const timestamps=   require('mongoose-timestamp');

const VoucherSchema = new mongoose.Schema({
    
    name:{
            type: String,
            trim: true,
            index: true
    },
    zipcode:{
            type: String,
            trim: true,
            index: true
    },
    desc:{
            type: String,
            trim: true,
    },
    points:{
         type: Number,
         default: 0
    },
    startDate: {
            trim: true,
            type: Date
    },
    endDate: {
            trim: true,
            type: Date
    },
    image:{
        type: String
    },
    active: {
        type: Boolean,
        default: true
    },
    redeemed: {
        type: Boolean,
        default: false
    }
   
    
});

VoucherSchema.plugin(timestamps);
VoucherSchema.index({  name: 1, zipcode: 1});

const Voucher = mongoose.model('Voucher', VoucherSchema);
module.exports = exports = Voucher;