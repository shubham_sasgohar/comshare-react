const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const ListTypeSchema = new mongoose.Schema({
    
});


ListTypeSchema.plugin(timestamps);

const Listing = mongoose.model('Listing', ListTypeSchema);
module.exports = exports = Listing;
