
const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const ListingSchema = new mongoose.Schema({
		userId: {
			type:  mongoose.Schema.ObjectId
		},
		title: {
			type: String,
			trim: true,
			required: true,
			index: true
		},
		keywords: {
			type: Array,
			required: true,
			index: true
		},
		description: {
			type: String,
			required: true,
			bcrypt: true
		},
		price: {
			type: Number
                },
                pincode: {
			type: String,
			index: true
		},
		dueDate: {
			type: Date
		},
		startDate: {
			type: Date
		},
		endDate: {
			type: Date
		},
		type: {
			type: String,
			enum : ['HELP', 'JOB'],
			default: 'HELP'
		},
		hiredUserId: mongoose.Schema.ObjectId,
		feedback: String,
		rating: Number,
		status: {
			type: String,
			enum : ['OPEN', 'CLOSE'],
			default: 'OPEN'
		},
                notificationMessage: {
                    type: String
		},
                
                notificationStatus: {
                    type: String,
                    enum : ['unread', 'read'],
                    default: 'read'
		},
                countNotifStatus: {
                    type: String,
                    enum: ['unread', 'read'],
                    default: 'unread'
                },
		active: {
			type: Boolean,
			default: true
		}
	
});


ListingSchema.plugin(timestamps);
ListingSchema.index({  title: 1, pincode: 1, keywords: 1 });

const Listing = mongoose.model('Listing', ListingSchema);
module.exports = exports = Listing;
