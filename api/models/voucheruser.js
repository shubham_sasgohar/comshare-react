const mongoose  =   require('mongoose');
const bcrypt    =   require('bcrypt');
const timestamps=   require('mongoose-timestamp');

const VoucheruserSchema = new mongoose.Schema({

    voucherId:mongoose.Schema.ObjectId,
    userId:mongoose.Schema.ObjectId,
    code:String,
    redeemed: {
        type: Boolean,
        default: false
    },
    redeemedStatus: {
        type: Boolean,
        default: false
    }
   
    
});

VoucheruserSchema.plugin(timestamps);
const Voucheruser = mongoose.model('Voucheruser', VoucheruserSchema);
module.exports = exports = Voucheruser;