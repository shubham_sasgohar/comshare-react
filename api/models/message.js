const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const MsgSchema = new mongoose.Schema({
		MsgId: {
			type: mongoose.Schema.ObjectId
		},
		clientId: {
			type:  mongoose.Schema.ObjectId
		},
		userId: {
			type:  mongoose.Schema.ObjectId
		},
		status: {
			type: String
		}
});


MsgSchema.plugin(timestamps);
MsgSchema.index({ MsgId: 1, userId: 1 });

const Msg = mongoose.model('Msg', MsgSchema);
module.exports = exports = Msg;
