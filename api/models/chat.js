const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');


const ChatSchema = new mongoose.Schema({
		userId: {
			type:  mongoose.Schema.ObjectId
		},
		clientId: {
			type:  mongoose.Schema.ObjectId
		},
		jobId: {
			type:  mongoose.Schema.ObjectId
		},
		msg: {
			type: String
		},
                image: {
			type: String
		},
                msg_status: {
                        type: String,default:'unread'
		},
		status: {
			type: String
		}
});


ChatSchema.plugin(timestamps);
ChatSchema.index({ MsgId: 1, userId: 1 });

const Chat = mongoose.model('Chat', ChatSchema);
module.exports = exports = Chat;
