const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const JobSchema = new mongoose.Schema({
		jobId: {
			type: mongoose.Schema.ObjectId
		},
		ownerId: {
			type:  mongoose.Schema.ObjectId
		},
		users: [
			{ id: mongoose.Schema.ObjectId, date: Date}
		],
		status: {
			type: String
		}
});


JobSchema.plugin(timestamps);
JobSchema.index({ jobId: 1, userId: 1 });

const Job = mongoose.model('Job', JobSchema);
module.exports = exports = Job;
