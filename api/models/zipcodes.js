const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const timestamps = require('mongoose-timestamp');

const ZipcodeSchema = new mongoose.Schema({

    value: {
        type: String,
        trim: true
    },
    type: {
        type: String,
        enum: ['zipcode', 'keyword']
    },

})

ZipcodeSchema.plugin(timestamps);
const zipcode = mongoose.model('zipcode', ZipcodeSchema);
module.exports = exports = zipcode;