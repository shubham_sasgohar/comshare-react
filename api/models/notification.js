const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const NotificationSchema = new mongoose.Schema({
    userId: mongoose.Schema.ObjectId,
    jobId: mongoose.Schema.ObjectId,
    fromUserId: mongoose.Schema.ObjectId,
    message: String,
    status: String,
    hired: String,
    msg_status: Number,
    job_applied_status: {
        type:Number,
        default: 0
    },
    notificationMessage: {
        type: String
    },
    notificationStatus: {
        type: String,
        enum: ['unread', 'read'],
        default: 'unread'
    },
    countNotifStatus: {
        type: String,
        enum: ['unread', 'read'],
        default: 'unread'
    },
    close_request_status:{
            type: Boolean,
            default: false
    },
    
   
});


NotificationSchema.plugin(timestamps);
NotificationSchema.index({userId: 1, fromUserId: 1});

const Notification = mongoose.model('Notification', NotificationSchema);
module.exports = exports = Notification;
