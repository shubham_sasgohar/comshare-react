const mongoose      =   require('mongoose');
const timestamps    =   require('mongoose-timestamp');

const CmspageSchema = new mongoose.Schema({
    
        title: {
                type: String,
                trim: true
        },
        content:{
            type : String
        },
        page_type:{
            type : String
        },
        order:{
            type : Number
        },
        header:{
            type: Boolean,
            default: false
        },
        footer:{
            type: Boolean,
            default: false
        },
        footer_type:{
            type : String
        }
    
});
CmspageSchema.plugin(timestamps);
        
const Cmspage = mongoose.model('Cmspage', CmspageSchema);
module.exports = exports = Cmspage;