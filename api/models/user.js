
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const timestamps = require('mongoose-timestamp');

const UserSchema = new mongoose.Schema({
		email: {
			type: String,
			lowercase: true,
			trim: true,
			index: true,
			unique: true,
			required: true
		},
		username: {
			type: String,
			lowercase: true,
			trim: true,
			index: true,
			unique: true,
			required: true
		},
		password: {
			type: String,
			required: true,
			bcrypt: true
		},
		firstname: {
			type: String,
			required: true,
			trim: true,
        },
        lastname: {
			type: String,
			trim: true,
		},
		profileImg: {
			type: String
		},
                city: {
			type: String,
			trim: true
		},
		state: {
			type: String,
			trim: true
		},
		bio: {
			type: String,
			trim: true
		},
		address1: {
			type: String,
			trim: true
		},
		address2: {
			type: String,
			trime: true,
		},
		phone: {
			type: Number,
			trim: true
		},
		zipcode: {
			type: String,
			trim: true,
		},
		active: {
			type: Boolean,
			default: true
		},
		isEmailVerified: {
			type: String
		},
		coins: {
			type: Number,
			default: 100
		},
                rating: {
			type: Number,
			default: 0
		},
		admin: {
			type: Boolean,
			default: false
		},
		sub_admin: {
			type: Boolean,
			default: false
		}
});

// checking if password is valid
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};


//UserSchema.plugin(bcrypt);
UserSchema.plugin(timestamps);
UserSchema.index({ email: 1, username: 1 });

const User = mongoose.model('User', UserSchema);
module.exports = exports = User;
