const express = require("express");
const router = express.Router();
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');

const CONFIG = require('../config/app.config');

const expressJwt = require('express-jwt');
const ObjectId = require('mongodb').ObjectId;

// Node Mailer Function
const nodemailer = require('nodemailer');
const Chat = require('../models/chat');

// save msg api
router.post('/chat/savemsg', function(req, res, next) {
    var body = req.body;
    console.log(body)
    let data = new Chat(body);

    data.save(function(err, list) {
        if(err)
            console.log(err);
        res.json({
            data: list
        });

    });
});

// Get msg api
router.post('/chat/getmessages', function(req, res) {
       Chat.find({
		    $or: [ 
                    { 
                        userId: req.body.userId,
                        clientId: req.body.clientId,
                        jobId: req.body.jobId
                    },
                    {
                        userId: req.body.clientId,
                        clientId: req.body.userId,
                        jobId: req.body.jobId
                    } 
            ]
	   })
	   .sort({'createdAt': 1})
	   .exec(function(err, docs) { 
		 if (err) {
            return res.status(200).json({success: false});
        } else {
            console.log(docs)
             return res.json({
                data: docs
            });
         }		
	   });

        /* Chat.find(
        {
            $or: [ 
                    { 
                        userId: req.body.userId,
                        clientId: req.body.clientId,
                        jobId: req.body.jobId
                    },
                    {
                        userId: req.body.clientId,
                        clientId: req.body.userId,
                        jobId: req.body.jobId
                    } 
            ]
        },
       
     function(err, lists) {
        
        if (err) {
            return res.status(200).json({success: false});
        } else {
            console.log(lists)
             return res.json({
                data: lists
            });
         }					
     }); */
});

// Get getunreadmsgcount api
router.post('/chat/getunreadmsgcount', function(req, res) {
    
       Chat.aggregate(
            [
                {$match: {msg_status: "unread"}},
                {
                    $group: {
                        _id:
                            { 
                                jobId: "$jobId",
                                userId: "$userId"
                            },
                    count: {$sum: 1}}
                }
            ]
 
        ).exec(function (err, data) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                return res.json({
                    data: data
                });
            }
        });
});

// Set setMessageStatus api
router.post('/chat/setMessageStatus', function(req, res) {
    
        
        
        Chat.update(
            {
                jobId: req.body.jobId,
                userId: req.body.clientId,
        
            },
            {
                $set:
                    {
                        msg_status: 'read',
                    }
            },
            {multi: true}
        ).exec(function (err, data) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                return res.json({
                    data: data
                });
            }
        });
//        
//       Chat.aggregate(
//            [
//                {$match: {msg_status: "unread"}},
//                {$group: {_id: "$jobId", count: {$sum: 1}}}
//            ]
//
//        ).exec(function (err, data) {
//            if (err) {
//                return res.status(200).json({success: false});
//            } else {
//                return res.json({
//                    data: data
//                });
//            }
//        });
});


module.exports = router;

