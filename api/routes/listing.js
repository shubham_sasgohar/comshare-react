const express = require("express");
const router = express.Router();

const path = require('path');
const fs = require('fs');

const ObjectId = require('mongodb').ObjectID;

var applyJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/applyjob.html'), 'utf8');
var requestCloseTemplate = fs.readFileSync(path.join(__dirname, '../templates/requestClose.html'), 'utf8');
//var acceptJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/acceptowner.html'), 'utf8');
var acceptUserTemplate = fs.readFileSync(path.join(__dirname, '../templates/acceptuser.html'), 'utf8');
var hireJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/hire.html'), 'utf8');
var userHiredJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/userHired.html'), 'utf8');
var hireJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/hire.html'), 'utf8');
var closeJobTemplate = fs.readFileSync(path.join(__dirname, '../templates/closeOwner.html'), 'utf8');
var closeUserTemplate = fs.readFileSync(path.join(__dirname, '../templates/closeUser.html'), 'utf8');


const CONFIG = require('../config/app.config');
const List = require('../models/listing');
const User = require('../models/user');
const Notify = require('../models/notification');

// Node Mailer Function
const nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    secureConnection: true, // use SSL
    port: 587, // port for secure SMTP
    transportMethod: 'SMTP',
    auth: {
        user: CONFIG.SEMAIL_ID,
        pass: CONFIG.SEMAIL_PASS
    }
});
/*For current Date To check old jobs are not display in listing*/
let date = new Date();
var year = date.getFullYear();
var month = date.getMonth();
var day = date.getDate();
// Create Listing
router.post('/list/createList', (req, res) => {
    let body = req.body;

    let list = new List(body);

    list.save(function (err, list) {
        if (err)
            return res.send(err);

        return res.json({
            list: list
        });

    });
});

// Get all Listing
router.get('/list/getAll', (req, res) => {

    List.aggregate([
        {
            $match: {
                status: "OPEN",
                hiredUserId: null,
                active: true,
                $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {"$unwind": "$userinfo"},

        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
        {$sort: {'createdAt': -1}},
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            let review_data = [];
            count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                data.sort(function (a, b) {
                                    return b.createdAt - a.createdAt
                                });
                                res.json({
                                    lists: data
                                });
                            }
                        });
                    }
                });
            } else {
                res.json({
                    lists: data
                });
            }

        }
    });
});


// Get list by type
router.get('/list/getByType/:TYPE', (req, res) => {
    let type = req.params.TYPE;

    List.aggregate([
        {
            $match: {
                type,
                status: "OPEN",
                hiredUserId: null,
                $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {"$unwind": "$userinfo"},
        {$sort: {'_id': -1}},
        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            var count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                res.json({
                                    lists: data
                                });
                            }
                        });
                    }
                });
            } else {
                res.json({
                    lists: data
                });
            }

        }
    });
});


router.get('/list/getByQueryOnly', (req, res) => {
    let query = req.query.query;

    if (!query) {
        return res.send({
            error: true,
            message: "search input is blank"
        });
    }

    List.aggregate([
        {
            $match: {
                status: "OPEN",
                hiredUserId: null,
                $or: [
                    {title: {$regex: query, $options: 'i'}},
                    {pincode: {$regex: query, $options: 'i'}},
                    {keywords: {$regex: query, $options: 'i'}},
//                    {pincode: query},
//                    {keywords: query},
//                    {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                ]
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {"$unwind": "$userinfo"},
        {$sort: {'_id': -1}},
        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            var count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                res.json({
                                    lists: data
                                });
                            }
                        });
                    }
                });
            } else {
                res.json({
                    lists: data
                });
            }

        }
    });
})

// Get list by query
router.get('/list/getByQuery', (req, res) => {
    let query = req.query.query;
    let type = req.query.type;

    if (!query) {
        return res.send({
            error: true,
            message: "search input is blank"
        });
    }

    List.aggregate([
        {
            $match: {
                status: 'OPEN',
                hiredUserId: null,

                $or: [
                    {title: {$regex: query, $options: 'i'}},
                    {pincode: {$regex: query, $options: 'i'}},
                    {keywords: {$regex: query, $options: 'i'}},
//                    {pincode: query},
//                    {keywords: query},
//                    {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                ],
            }
        },
        {$match: {"type": type}},
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {"$unwind": "$userinfo"},
        {$sort: {'_id': -1}},
        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            var count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                res.json({
                                    lists: data
                                });
                            }
                        });
//                    data.push(list);
                    }
                });
            } else {
                res.json({
                    lists: data
                });
            }
        }
    });
});

// Filter Lisitng
router.get('/list/getAllByFilter/:filter', (req, res) => {
    let filter = req.params.filter;
    let orderBy = -1;

    if (filter == "old") {
        orderBy = 1;
    } else if (filter == "new") {
        orderBy = -1;
    }

    List.aggregate([
        {
            $match: {
                status: "OPEN",
                hiredUserId: null,
                $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {"$unwind": "$userinfo"},
        {$sort: {'_id': orderBy}},
        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            var count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                res.json({
                                    lists: data
                                });
                            }
                        });
                    }
                });
            } else {
                res.json({
                    lists: data
                });
            }
        }
    });
});

router.get('/list/getAllByFilterType', (req, res) => {
    let filter = req.query.change;
    let type = req.query.type;

    let orderBy = -1;
    if (filter == "old") {
        orderBy = 1;
    } else if (filter == "new") {
        orderBy = -1;
    }

    List.aggregate([
        {
            $match: {
                status: "OPEN",
                hiredUserId: null,
                $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userinfo"
                    }
        },
        {$match: {"type": type}},
        {"$unwind": "$userinfo"},
        {$sort: {'_id': orderBy}},
        {
            $lookup:
                    {
                        from: "notifications",
                        localField: "_id",
                        foreignField: "jobId",
                        as: "notifications"
                    }
        },
    ], function (err, lists) {
        if (err) {
            return res.status(200).json({success: false});
        } else {
            let data = [];
            var count = 0;
            if (lists.length > 0) {
                lists.map((list) => {
                    if (list.userinfo.isEmailVerified == "true") {
                        List.aggregate([
                            {
                                $match: {
                                    'hiredUserId': ObjectId(list.userinfo._id),
                                    'status': 'CLOSE'
                                },
                            },
                            {$group: {_id: null, myCount: {$sum: 1}}},
                        ], function (err, reviews_data) {
                            bool = true;
                            reviews_data.map((reviews) => {
                                list['mycount'] = reviews.myCount;
                            });
                            data.push(list);
                            count++;
                            if (lists.length == count) {
                                res.json({
                                    lists: data
                                });
                            }
                        });
                    }
                });
            } else {

                res.json({
                    lists: data
                });
            }

        }
    });
});

// seach by filter type query
router.get('/list/getAllByFilterTypeQuery', (req, res) => {
    let query = req.query.query;
    let type = req.query.type;
    let filter = req.query.change;

    let orderBy = -1;
    if (filter == "old") {
        orderBy = 1;
    } else if (filter == "new" || filter == "recent") {
        orderBy = -1;
    }

    if (query && filter && type == '') {
        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {$match: {"status": "OPEN", hiredUserId: null}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }

            }
        });

    } else {
        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {$match: {"type": type, "status": "OPEN", hiredUserId: null}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    }
});


// Filter by keywords
router.get('/list/getAllByKeywords', (req, res) => {
    let keywords = req.query.keywords;
    let type = req.query.type ? req.query.type : '';
    let query = req.query.query ? req.query.query : '';
    let filter = req.query.filterBy ? req.query.filterBy : '';


    if (keywords && type === '' && query === '' && filter === '') {
        List.aggregate([
            {
                $match: {
                    'keywords.value': {$eq: keywords},
                    'status': 'OPEN',
                    hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
                }
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': -1}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }


            }
        });
    } else if (keywords && type && query === '' && filter === '') {
        console.log(keywords)
        console.log(type)
        List.aggregate([
            {
                $match: {
                    'keywords.value': {$eq: keywords},
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]
                }
            },
            {$match: {"type": type, "status": "OPEN", hiredUserId: null}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': -1}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    console.log('here')
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }

            }
        });
    } else if (keywords && type && query && filter === '') {
        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'keywords.value': {$eq: keywords}, 'status': 'OPEN', hiredUserId: null}
            },
            {$match: {"type": type}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': -1}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }



            }
        });
    } else if (keywords && type && query && filter) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'keywords.value': {$eq: keywords}, 'status': 'OPEN', hiredUserId: null}
            },
            {$match: {"type": type}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }



            }
        });
    } else if (keywords && type === '' && query === '' && filter) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'keywords.value': {$eq: keywords}, 'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }



            }
        });
    } else if (keywords && type && query == '' && filter) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'keywords.value': {$eq: keywords}, 'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {$match: {"type": type}},
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }

            }
        });
    } else if (keywords && type == '' && query && filter) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'keywords.value': {$eq: keywords}, 'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true") {
//                        data.push(list);
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    }


});



// Get List By Rating Filter 
router.get('/list/getAllByRating', (req, res) => {
    let rating = req.query.rating;
    let type = req.query.type ? req.query.type : '';
    let query = req.query.query ? req.query.query : '';
    let filter = req.query.filterBy ? req.query.filterBy : '';
    let keywords = req.query.keywords ? req.query.keywords : '';

    if (rating && type && query && filter == '' && keywords == '') {
        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'type': type, 'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': -1}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type && query && filter && keywords == '') {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'type': type, 'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type && query && filter && keywords) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'keywords.value': {$eq: keywords}}
            },
            {
                $match: {'type': type, 'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type == '' && query && filter == '' && keywords == '') {

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': -1}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type && query == "" && filter && keywords) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'keywords.value': {$eq: keywords}}
            },
            {
                $match: {'type': type, 'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type && query == "" && filter && keywords == '') {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'type': type, 'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type == "" && query == "" && filter && keywords) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'keywords.value': {$eq: keywords}}
            },
            {
                $match: {'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    } else if (rating && type == "" && query == "" && filter == "" && keywords) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {'keywords.value': {$eq: keywords}}
            },
            {
                $match: {'status': 'OPEN', hiredUserId: null,
                    $or: [{endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}]}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }

            }
        });
    }
    /*Add new condtion*/
    else if (rating && type == '' && query && filter && keywords) {
        let orderBy = -1;
        if (filter == "old") {
            orderBy = 1;
        } else if (filter == "new" || filter == "recent") {
            orderBy = -1;
        }

        List.aggregate([
            {
                $match: {
                    $or: [
                        {title: {$regex: query, $options: 'i'}},
                        {pincode: query},
                        {keywords: query},
                        {endDate: {$gte: new Date(year, month, day)}}, {dueDate: {$gte: new Date(year, month, day)}}
                    ]
                }
            },
            {
                $match: {'keywords.value': {$eq: keywords}}
            },
            {
                $match: {'status': 'OPEN', hiredUserId: null}
            },
            {
                $lookup:
                        {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userinfo"
                        }
            },
            {"$unwind": "$userinfo"},
            {$sort: {'_id': orderBy}},
            {
                $lookup:
                        {
                            from: "notifications",
                            localField: "_id",
                            foreignField: "jobId",
                            as: "notifications"
                        }
            },
        ], function (err, lists) {
            if (err) {
                return res.status(200).json({success: false});
            } else {
                let data = [];
                var count = 0;
                if (lists.length > 0) {
                    lists.map((list) => {
                        if (list.userinfo.isEmailVerified == "true" && list.userinfo.rating >= rating) {
                            List.aggregate([
                                {
                                    $match: {
                                        'hiredUserId': ObjectId(list.userinfo._id),
                                        'status': 'CLOSE'
                                    },
                                },
                                {$group: {_id: null, myCount: {$sum: 1}}},
                            ], function (err, reviews_data) {
                                bool = true;
                                reviews_data.map((reviews) => {
                                    list['mycount'] = reviews.myCount;
                                });
                                data.push(list);
                                count++;
                                if (lists.length == count) {
                                    res.json({
                                        lists: data
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        lists: data
                    });
                }
            }
        });
    }


});


// Apply JOb

// Apply JOb
router.post('/list/applyJob', (req, res) => {


    let body = req.body;
    let userId = body.userId;
    let userName = body.userName;
    let jobId = body.jobId;
    let msg = body.msg;
    let job_title = body.job_title;
    let msg_status = body.msg_status;

    /*parameter close job request*/
    let hiredId = body.hiredId;
    let appliedId = body.appliedId;
    let close_request_jobId = body.jobId;
    let close_request_status = body.close_request_status;
    let close_request_msg = body.close_request_msg;
    let close_request_username = body.username;
    let close_request_email = body.email;
    let close_job_title = body.close_job_title;
    let call_type = body.call_type;

    if (call_type == 'close_request') {

        let notify = new Notify({
            userId: hiredId,
            jobId: close_request_jobId,
            fromUserId: appliedId,
            //message: `${userName} applied for job!`,
            message: close_request_msg,
            close_request_status: close_request_status,
            status: 'unread',
        });

        notify.save((err, doc) => {
            if (err)
                return err;

            if (doc) {
                // setup email data with unicode symbols
                let mailOptions = {
                    from: 'brstdev12@gmail.com',
                    to: body.email, // list of receivers
                    subject: `Close Job Request`, // Subject line
//                    html: `close job request!`,
                    html: requestCloseTemplate.replace('USERNAME', body.username).replace('JOBNAME', body.close_job_title)
                };

                smtpTransport.sendMail(mailOptions, function (error, response) {
                    if (error)
                        throw error;
                });
                res.send(doc);
            }
        });
    } else {
        // send notification
        let notify = new Notify({
            userId: body.ownerId,
            jobId: jobId,
            fromUserId: userId,
            //message: `${userName} applied for job!`,
            message: msg,
            status: 'unread',
            msg_status: msg_status,
            job_applied_status: body.job_applied_status
        });

        Notify.find({
            userId: body.ownerId,
            fromUserId: userId,
            jobId: jobId,
            job_applied_status: 1,

        }, (err, doc) => {
            console.log(doc)
            if (err)
                return err;

            if (doc) {

                if (doc.length > 0) {
                    let id = doc[0]['_id'];
                    let db_msg_status = doc[0]['msg_status'];

                    Notify.updateOne({
                        _id: id,
                    },
                            {
                                $set: {
                                    msg_status: 0,
                                    countNotifStatus: 'unread',
                                    message: msg

                                }
                            }, (err, doc_update) => {
                        if (err)
                            return err;
                        res.send(doc_update);
                    });

                } else {

                    Notify.find({
                        userId: body.ownerId,
                        fromUserId: userId,
                        jobId: jobId
                    }, (err, result) => {
                        if (err)
                            return err;

                        if (result) {

                            if (result.length > 0) {
                                let id = result[0]['_id'];
                                Notify.updateOne({
                                    _id: id,
                                },
                                        {
                                            $set: {
                                                msg_status: 0,
                                                countNotifStatus: 'unread',
                                                message: msg,
                                                job_applied_status: 1

                                            }
                                        }, (err, doc_update) => {
                                    if (err)
                                        return err;

                                    if (body.job_applied_status == '1') {
                                        let mailOptions = {
                                            from: 'brstdev12@gmail.com',
                                            to: body.ownerEmail, // list of receivers
                                            subject: `Apply job Notification`, // Subject line
//                                        html: `${body.userName} applied for your job post!`,
                                            html: applyJobTemplate.replace('USERNAME', body.userName).replace('JOBNAME', body.job_title).replace('HREF', `href="${CONFIG.WEB_URL}/login?redirect=messages"`)
                                        };

                                        smtpTransport.sendMail(mailOptions, function (error, response) {
                                            if (error)
                                                throw error;
                                        });
                                    }
                                    res.send(doc_update);
                                });
                            } else {
                                notify.save((err, doc) => {
                                    if (err)
                                        return err;

                                    if (doc) {
                                        // setup email data with unicode symbols
                                        if (body.job_applied_status == '1') {
                                            let mailOptions = {
                                                from: 'brstdev12@gmail.com',
                                                to: body.ownerEmail, // list of receivers
                                                subject: `Apply job Notification`, // Subject line
//                                            html: `${body.userName} applied for your job post!`,
                                                html: applyJobTemplate.replace('USERNAME', body.userName).replace('JOBNAME', body.job_title).replace('HREF', `href="${CONFIG.WEB_URL}/login?redirect=messages"`)
                                            };

                                            smtpTransport.sendMail(mailOptions, function (error, response) {
                                                if (error)
                                                    throw error;
                                            });

                                        }

                                        res.send(doc);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }


});


// Get user Notification
router.post('/getNotification', (req, res) => {
    let body = req.body;

    Notify.aggregate([
        {
            $lookup:
                    {
                        from: "users",
                        localField: "fromUserId",
                        foreignField: "_id",
                        as: "appliedUser"
                    }
        },
        {
            "$unwind": "$appliedUser"
        },
        {
            $lookup:
                    {
                        from: "listings",
                        localField: "jobId",
                        foreignField: "_id",
                        as: "jobInfo"
                    }
        },
        {
            "$unwind": "$jobInfo"
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userPost"
                    }
        },
        {
            "$unwind": "$userPost"
        },
        {$sort: {'_id': -1}}
    ], function (err, doc) {
        let data = [];

//        doc.map((elem) => {
//            if(elem.userId == body.id) 
//                data.push(elem);
//        });
        if (doc.length > 0) {
            doc.map((elem) => {
                if (elem.userId == body.id || elem.fromUserId == body.id)
                    data.push(elem);
            });
            res.send(data);
        } else {
            res.send(data);
        }
    });

});

// New Router to check status
router.post('/getApplyStatus', (req, res) => {
    let body = req.body;
    Notify.find({fromUserId: body.id, jobId: body.jobId}, (err, doc) => {
        if (err)
            return err;

        if (doc) {
            res.send(doc)
            //    res.json({
            //        errpr: true,
            //        message: 'Already Applied!'
            //    })
        }
    });
});


// Accept Notification
router.post('/notifyAction', (req, res) => {
    let body = req.body;



    Notify.aggregate([
        {
            $match: {
                _id: ObjectId(body.id),
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "ownerInfo"
                    }
        },
        {
            "$unwind": "$ownerInfo"
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "fromUserId",
                        foreignField: "_id",
                        as: "appliedUser"
                    }
        },
        {
            "$unwind": "$appliedUser"
        },
        {
            $lookup:
                    {
                        from: "listings",
                        localField: "jobId",
                        foreignField: "_id",
                        as: "jobInfo"
                    }
        },
        {
            "$unwind": "$jobInfo"
        }
    ], function (err, data) {
        if (err)
            return err;

        Notify.updateOne({
            _id: body.id
        },
                {
                    $set: {
                        status: body.action
                    }
                }, (err, doc) => {
            if (err)
                return err;

            let info = data[0];

            // setup email data with unicode symbols
            // Send Email to owner 
//            let ownerMailOptions = {
//                from: 'brstdev12@gmail.com',
//                to: info.ownerInfo.email, // list of receivers
//                subject: `Comshare`, // Subject line
//                //html: `You have accepted proposal from user <b>${info.appliedUser.username}</b> for job ${info.jobInfo.title}`,
//                html: acceptJobTemplate.replace('USERNAME', info.appliedUser.username).replace('JOBTITLE', info.jobInfo.title).replace('ACTION', body.action)
//            };
//
//            smtpTransport.sendMail(ownerMailOptions, function (error, res) {
//                if (error)
//                    return error;
//                else
//                    console.log(`Message sent: ${res.response} ${info.ownerInfo.email}`);
//            });

            let userMailOptions = {
                from: 'brstdev12@gmail.com',
                to: info.appliedUser.email, // list of receivers
                subject: `Comshare Job Notifcation`, // Subject line
                //html: `Congratulations you proposal accepted for job <b>${info.jobInfo.title}</b>, please contact with user <b>${info.ownerInfo.username}</b>`,
                html: acceptUserTemplate.replace('USERNAME', info.ownerInfo.username).replace('JOBTITLE', info.jobInfo.title).replace('ACTION', body.action)
            };

            smtpTransport.sendMail(userMailOptions, function (error, res) {
                if (error)
                    return error;
                else
                    console.log(`Message sent: ${res.response} ${info.appliedUser.email}`);
            });



            if (body.action == "accept") {
                res.json({
                    action: body.action,
                    error: false,
                    message: 'Successfully Accepted!'
                })
            } else if (body.action == "decline") {
                res.json({
                    action: body.action,
                    error: false,
                    message: 'Successfully Declined!'
                });
            }

        });

    });
});


router.post('/list/hireUser', (req, res) => {
    let body = req.body;
    var user_id = body.userId;
    var job_id = body.jobId;
    List.aggregate([
        {
            $match: {
                _id: ObjectId(body.jobId)
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "ownerInfo"
                    }
        },
        {"$unwind": "$ownerInfo"}
    ], function (err, list) {

        if (err)
            return err;
        else
            var message = list[0].ownerInfo.username + ' Hired you for ' + list[0].title + ' Job!';
        List.findOneAndUpdate({_id: body.jobId},
                {
                    $set: {
                        "hiredUserId": body.userId,
                        "notificationStatus": "unread",
                        "notificationMessage": message,
                        "countNotifStatus": "unread"
                    }
                }, {upsert: true, 'new': true}, function (err, doc) {

            if (err) {
                res.send(err);
            }
            let info = list[0];

            User.findOne({_id: body.userId}, (err, doc) => {
                if (err)
                    return err;

                if (doc) {
                    let user_email = doc.email;

                    let ownerMailOptions_user = {
                        from: 'brstdev12@gmail.com',
                        to: user_email, // list of receivers
                        subject: `Hired Job Notification`, // Subject line
                        html: userHiredJobTemplate.replace('JOBTITLE', info.title)
                    };

                    smtpTransport.sendMail(ownerMailOptions_user, function (error, response) {
                        if (error)
                            throw error;
                    });

                }
            });

            Notify.findOneAndUpdate({
                fromUserId: user_id,
                jobId: job_id
            },
                    {
                        $set: {
                            hired: 'hired'
                        }
                    }, {upsert: true}, function (err, doc) {
                if (err)
                    return err;
            });

            res.json({
                error: false,
                message: 'Successfully Hired!'
            });

        });
    });


    // res.json(body);
    // List.findOneAndUpdate({ _id: body.jobId },
    //     { 
    //     $set: {
    //         "hiredUserId": body.userId
    //     }
    //     }, { upsert : true, 'new': true }, function(err, doc) {
    //         if (err) {
    //             res.send(err);
    //         } else {

    //             res.json({ 
    //                 error: false,
    //                 message: 'Successfully Hired!'
    //             });
    //         }
    //     });
});


router.post('/list/closeJob', (req, res) => {
    let body = req.body;

    List.aggregate([
        {
            $match: {
                _id: ObjectId(body.jobId)
            }
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "ownerInfo"
                    }
        },
        {"$unwind": "$ownerInfo"},
        {
            $lookup:
                    {
                        from: "users",
                        localField: "hiredUserId",
                        foreignField: "_id",
                        as: "applicantInfo"
                    }
        },
        {"$unwind": "$applicantInfo"},
    ], function (err, list) {
        if (err)
            return err;
        else
            var message = list[0].ownerInfo.username + ' close your ' + list[0].title + ' Job!';
        List.findOneAndUpdate({_id: body.jobId},
                {
                    $set: {
                        "feedback": body.feedback,
                        "rating": body.rating,
                        "status": "CLOSE",
                        "notificationMessage": message,
                        "notificationStatus": "unread",
                        "countNotifStatus": "unread"

                    }
                }, {upsert: true, 'new': true}, function (err, doc) {
            if (err) {
                return err;
            }
            let info = list[0];
            var applicant_id = info.applicantInfo._id;
            var sum_rating = 0;
            var sum_count = 0;
            var user_rating = 0;

            List.aggregate([
                {
                    $match: {
                        'hiredUserId': ObjectId(applicant_id),
                        'status': 'CLOSE'
                    },

                }
            ], function (err, data) {
                if (err)
                    throw err;
                data.map((elem) => {
                    sum_rating += parseFloat(elem.rating);
                    sum_count++;

                });
                console.log(sum_rating);
                console.log(sum_count);
                user_rating = Math.trunc(sum_rating / sum_count);

                User.update(
                        {_id: body.current_userfrom_id},
                        {$inc: {coins: body.jobcoins}}
                , function (err, doc) {
                    if (err) {
                        return err;
                    }
                });
                User.update(
                        {_id: body.current_user_id},
                        {$inc: {coins: -body.jobcoins}}
                , function (err, doc) {
                    if (err) {
                        return err;
                    }
                });
               
                User.findOneAndUpdate({_id: applicant_id},
                        {
                            $set: {
                                "rating": user_rating,
                                $inc: {coins: body.jobcoins}
                            }
                        }, {upsert: true, 'new': true}, function (err, doc) {
                    if (err, data) {
                        return err;
                        console.log(data);
                    }
                });
            });

            let ownerMailOptions = {
                from: 'brstdev12@gmail.com',
                to: info.ownerInfo.email, // list of receivers
                subject: `Close job Notification`, // Subject line
                html: closeJobTemplate.replace('FEEDBACK', body.feeback).replace('RATING', body.rating).replace('JOBTITLE', info.title)
            };

            smtpTransport.sendMail(ownerMailOptions, function (error, response) {
                if (error)
                    throw error;
            });

            let userMailOptions = {
                from: 'brstdev12@gmail.com',
                to: info.applicantInfo.email, // list of receivers
                subject: `Close job Notification`, // Subject line
                html: closeUserTemplate.replace('FEEDBACK', body.feeback).replace('RATING', body.rating).replace('JOBTITLE', info.title)
            };

            smtpTransport.sendMail(userMailOptions, function (error, response) {
                if (error)
                    throw error;
            });
            res.json({
                error: false,
                message: 'Successfully Closed!'
            });

        });
    });

});

/*Get Header Notification*/
router.post('/getHeaderNotification', (req, res) => {
    let body = req.body;
    let data = [];
    Notify.aggregate([
        {
            $lookup:
                    {
                        from: "users",
                        localField: "fromUserId",
                        foreignField: "_id",
                        as: "appliedUser"
                    }
        },
        {
            "$unwind": "$appliedUser"
        },
        {
            $lookup:
                    {
                        from: "listings",
                        localField: "jobId",
                        foreignField: "_id",
                        as: "jobInfo"
                    }
        },
        {
            "$unwind": "$jobInfo"
        },
        {
            $lookup:
                    {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userPost"
                    }
        },
        {
            "$unwind": "$userPost"
        },
        {$sort: {'_id': -1}}
    ], function (err, doc) {

//        doc.map((elem) => {
//            if (elem.userId == body.id && elem.notificationStatus == 'unread')
//                data.push(elem);
//        });

        res.send(doc);
    });

});

/*Set Header Read message*/
router.post('/setHeaderReadMessage', (req, res) => {
    let body = req.body;
    let data = [];


    Notify.update({
        userId: body.id
    },
            {
                $set: {
                    notificationStatus: 'read'
                }
            }, {multi: true}, function (err, doc) {
        if (err)
            return err;

        List.update({
            hiredUserId: body.id
        },
                {
                    $set: {
                        notificationStatus: 'read'
                    }
                }, {multi: true}, function (err, doc1) {
            if (err)
                return err;

            res.send(doc);

        });

    });

});


/*Set Header Message Count Read*/
router.post('/setHeaderCountReadMessage', (req, res) => {
    let body = req.body;
    let data = [];


    Notify.update({
        userId: body.id
    },
            {
                $set: {
                    countNotifStatus: 'read'
                }
            }, {multi: true}, function (err, doc) {
        if (err)
            return err;

        List.update({
            hiredUserId: body.id
        },
                {
                    $set: {
                        countNotifStatus: 'read'
                    }
                }, {multi: true}, function (err, doc1) {
            if (err)
                return err;

            res.send(doc);

        });

    });

});

module.exports = router;

