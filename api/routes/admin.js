// Admin Controller
const adminController = require("../controllers/admin");
const staffMemberController = require("../controllers/staffmember");
const cmsController = require("../controllers/cms");
const fileUpload = require('express-fileupload');
const path = require('path');
const moment = require('moment');
const Voucher = require('../models/voucher');
const User = require('../models/user');
const Zipcodes = require('../models/zipcodes');
const session = require('express-session');
const Cmspages = require('../models/cmspages');
const bcrypt = require('bcrypt');
var generator = require('generate-password');
const ObjectId = require('mongodb').ObjectId;
const utils = require('../utils/index');


module.exports = function (app, passport) {

    // All Login Process
    app.get('/', isLoggedIn, function (req, res) {
        res.redirect("/pages/index");
    });

    app.get('/login', function (req, res) {
        res.render('pages/index', {message: req.flash('loginMessage')}); // load the index.ejs file
    });


    app.post('/login', function (req, res, next) {
        passport.authenticate('local-login', function (err, user, info) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect('/login');
            }
            req.logIn(user, function (err) {
                if (err) {
                    return next(err);
                }

                return res.redirect('/dashboard');
            });
        })(req, res, next);
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/profile', function (req, res) {

        User.findOne({
            _id: ObjectId(req.user._id),
        }, function (err, user) {
            if (err)
                throw err;
            
            res.render('pages/profile', {admin: user}); // load the index.ejs file
            
        });
        
    });

    app.post('/updateprofile', isLoggedIn, function (req, res) {
        var body = req.body;
        console.log(req.query)
        if (req.query.validation == 'true') {
            if (body) {
                isUserUnique(body, function (err) {
                    if (err) {
                        return res.json({
                            error: true,
                            message: err
                        });
                    } else {
                        return res.json({
                            error: false,
                            message: 'success'
                        });
                    }
                });
            }

        } else {
            var data='';
            if(body.password != ''){
                console.log('pas');
                var hash = bcrypt.hashSync(body.password.trim(), 10);
                data ={
                    "firstname": body.firstname,
                    "lastname": body.lastname,
                    "username": body.username,
                    "email": body.email,
                    "password":hash,
                    "updatedAt": new Date()
                }  
               
            }else{
                
                data ={
                    "firstname": body.firstname,
                    "lastname": body.lastname,
                    "username": body.username,
                    "email": body.email,
                    "updatedAt": new Date()
                } 
            }
             console.log(data)
            User.findOneAndUpdate({_id: ObjectId(body.userid)},
                    {
                        $set: data
                    }, {upsert: true, 'new': true}, function (err, user) {
                if (err) {
                    res.send(err);
                } else {
                    user = utils.getCleanUser(user);
                    res.redirect('/profile');
                }
            });

        }
    });

    /*Vochers*/
    app.get('/addvoucher', isLoggedIn, function (req, res) {
        var message = '';
        if (req.session.voucher_success == 'success') {
            message = 'Voucher Add Successfully';
            req.session.voucher_success = '';
        }
        res.render('pages/addvoucher', {
            admin: req.user,
            message: message
        });
    });

    app.get('/editvoucher/:id', isLoggedIn, function (req, res) {
        var id = req.params.id;
        Voucher.find({
            _id: id
        }).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);

            // Set users
            console.log(docs)
            res.render('pages/editvoucher', {
                admin: req.user,
                vouchers: docs,
                moment: moment,
                message: ''

            });
        });

    });

    app.post('/updatevoucher', isLoggedIn, function (req, res) {
        var body = req.body;
        var imageFile = req.files.file;
        const dir = path.join(__dirname, '../public/vouchers_images/');
        let voucher_date_range = body.voucher_date_range.split("-");
        let date_range = body.voucher_date_range.split("-");
        let start_date = date_range[0].trim();
        let end_date = date_range[1].trim();
        if (imageFile != undefined) {
            let image_name = req.files.file.name;
            var final_image_name = moment().unix() + '_' + image_name.replace(/\s/g, "");
            imageFile.mv(`${dir}/${final_image_name}`, function (err) {
                if (err) {
                    return res.send(err);
                }
            });
        } else {
            var final_image_name = body.image_name;
        }
        // Update query
        Voucher.findOneAndUpdate({_id: body.voucher_id},
                {
                    $set: {
                        name: body.voucher_name,
                        zipcode: body.voucher_zip,
                        points: body.voucher_points,
                        desc: body.desc,
                        startDate: start_date,
                        endDate: end_date,
                        image: final_image_name,
                        updatedAt: new Date()
                    }
                }, {upsert: true}, function (err, list) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.redirect('/voucherlist');

            }
        });
    });

    app.post('/savevoucher', isLoggedIn, function (req, res, next) {
        var body = req.body;
        var imageFile = req.files.file;
        const dir = path.join(__dirname, '../public/vouchers_images/');
        let voucher_date_range = body.voucher_date_range.split("-");
        let date_range = body.voucher_date_range.split("-");
        let start_date = date_range[0].trim();
        let end_date = date_range[1].trim();
        let image_name = req.files.file.name;
        var final_image_name = moment().unix() + '_' + image_name.replace(/\s/g, "");

        var newvoucher = new Voucher({
            name: body.voucher_name.trim(),
            zipcode: body.voucher_zip.trim(),
            points: body.voucher_points,
            desc: body.desc.trim(),
            startDate: start_date,
            endDate: end_date,
            image: final_image_name
        });
        imageFile.mv(`${dir}/${final_image_name}`, function (err) {
            if (err) {
                return res.send(err);
            }
        });
        newvoucher.save(function (err, user) {
            if (err)
                return res.send(err);

            req.session.voucher_success = 'success';
            res.redirect('/voucherlist');
        });

    });

    app.get('/voucherlist', isLoggedIn, function (req, res) {


        Voucher.find({}).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);

            // Set users
            res.render('pages/voucherlist', {
                admin: req.user,
                vouchers: docs,
                moment: moment
            });
        });

    });

    app.post('/editStatusVoucherModal', function (req, res) {
        let body = req.body;
        // Update query
        Voucher.findOneAndUpdate({_id: body.id},
                {
                    $set: {
                        "updatedAt": new Date(),
                        "active": body.status
                    }
                }, {upsert: true}, function (err, list) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send("Voucher Updated!");
            }
        });
    });

    app.post('/delVoucherModal', function (req, res) {
        let body = req.body;

        // Delete query
        Voucher.findOneAndDelete({_id: body.id}, function (err, doc) {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send("Deleted Successfully!");
        });
    });
    /*End Vouchers List*/


    /*Starts Manage Zipcodes*/

    app.get('/addzipcode', isLoggedIn, function (req, res) {

        res.render('pages/addzipcode', {
            admin: req.user
        });
    });

    app.post('/savezipcode', isLoggedIn, function (req, res, next) {
        var body = req.body;

        var newzipcode = new Zipcodes({
            value: body.zipcode.trim(),
            type: 'zipcode'

        });
        newzipcode.save(function (err, user) {
            if (err)
                return res.send(err);
            res.redirect('/zipcodelist');
        });

    });

    app.get('/zipcodelist', isLoggedIn, function (req, res) {

        Zipcodes.find({
            type: 'zipcode'
        }).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);
            // Set users
            res.render('pages/zipcodelist', {
                admin: req.user,
                data: docs
            });
        });

    });

    app.get('/editzipcode/:id', isLoggedIn, function (req, res) {
        var id = req.params.id;
        Zipcodes.find({
            _id: id
        }).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);
            // Set users
            res.render('pages/editzipcode', {
                admin: req.user,
                data: docs

            });
        });

    });

    app.post('/updatezipcode', isLoggedIn, function (req, res) {
        var body = req.body;
        // Update query
        Zipcodes.findOneAndUpdate({_id: body.zipcode_id},
                {
                    $set: {
                        value: body.zipcode,
                        updatedAt: new Date()
                    }
                }, {upsert: true}, function (err, list) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.redirect('/zipcodelist');

            }
        });
    });

    app.post('/delzipcodeModal', function (req, res) {
        let body = req.body;

        // Delete query
        Zipcodes.findOneAndDelete({_id: body.id}, function (err, doc) {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send("Deleted Successfully!");
        });
    });


    /*End Manage Zipcodes*/

    /*Starts Manage Keywords*/

    app.get('/addkeyword', isLoggedIn, function (req, res) {

        res.render('pages/addkeyword', {
            admin: req.user
        });
    });

    app.post('/savekeyword', isLoggedIn, function (req, res, next) {
        var body = req.body;

        var newzipcode = new Zipcodes({
            value: body.keyword.trim(),
            type: 'keyword'

        });
        newzipcode.save(function (err, user) {
            if (err)
                return res.send(err);

            res.redirect('/keywordlist');
        });

    });

    app.get('/keywordlist', isLoggedIn, function (req, res) {

        Zipcodes.find({
            type: 'keyword'
        }).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);
            // Set users
            res.render('pages/keywordlist', {
                admin: req.user,
                data: docs
            });
        });

    });

    app.get('/editkeyword/:id', isLoggedIn, function (req, res) {
        var id = req.params.id;
        Zipcodes.find({
            _id: id
        }).sort([['_id', 1]]).exec(function (err, docs) {
            if (err)
                console.log(err);
            // Set users
            res.render('pages/editkeyword', {
                admin: req.user,
                data: docs

            });
        });

    });

    app.post('/updatekeyword', isLoggedIn, function (req, res) {
        var body = req.body;
        // Update query
        Zipcodes.findOneAndUpdate({_id: body.keyword_id},
                {
                    $set: {
                        value: body.keyword,
                        updatedAt: new Date()
                    }
                }, {upsert: true}, function (err, list) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.redirect('/keywordlist');

            }
        });
    });

    app.post('/delkeywordModal', function (req, res) {
        let body = req.body;

        // Delete query
        Zipcodes.findOneAndDelete({_id: body.id}, function (err, doc) {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send("Deleted Successfully!");
        });
    });


    /*End Manage Keywords*/


    app.get('/dashboard', isLoggedIn, function (req, res) {
        res.render('pages/dashboard', {
            admin: req.user
        });
    });


    // Admin CURD operation 
    app.get('/allusers', adminController.getAllUser);
    app.get('/alljobs', adminController.getAllJobs);

    // Edit user
    app.post('/editUser', adminController.editUser);
    app.post('/deleteUser', adminController.deleteUser);

    // Edit Job
    app.post('/editJob', adminController.editJob);
    app.post('/deleteJob', adminController.deleteJob);


    // Job Details 
    app.post('/getJobDetail', isLoggedIn, adminController.getJobDetail);
    app.post('/getUserDetails', isLoggedIn, adminController.getUserDetails);
    app.post('/getUserPostedJobs', isLoggedIn, adminController.getUserPostedJobs);

    /*Start Crud staff Member*/
    app.get('/addStaffMember', staffMemberController.addStaffMember);
    app.post('/saveStaffMember', staffMemberController.saveStaffMember);
    app.get('/staffMemberList', staffMemberController.staffMemberList);

    /*end Crud staff Member*/

    /*Manage Cms Pages*/
    app.get('/cmspage', cmsController.cmspage);
    app.post('/saveCmsPagesData', cmsController.saveCmsPagesData);
    app.get('/addCmsPage', cmsController.addCmsPage);
    app.post('/addNewCmsPage', cmsController.addNewCmsPage);
    app.get('/getCmsPage', cmsController.getCmsPage);

    /*End Manage Cms Pages*/

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
}

function isUserUnique(reqBody, cb) {
    var username = reqBody.username.toLowerCase() ? reqBody.username.trim() : '';
    var email = reqBody.email.toLowerCase() ? reqBody.email.trim() : '';
    var userid = reqBody.id ? reqBody.id : '';
    console.log('userid')
    console.log(userid)
    console.log('userid')
    User.findOne({
        _id: {$ne: ObjectId(userid)},
        $or: [{
                'username': new RegExp(["^", username, "$"].join(""), "i")
            }, {
                'email': new RegExp(["^", email, "$"].join(""), "i")
            }]
    }, function (err, user) {
        if (err)
            throw err;

        if (!user) {
            cb();
            return;
        }

        var msg;
        if (user.username === username) {
            msg = {};
            msg.username = '' + username + ' is already taken!';
        }
        if (user.email === email) {
            msg = msg ? msg : {};
            msg.email = '' + email + ' is already taken!';
        }

        cb(msg);
    });
}
