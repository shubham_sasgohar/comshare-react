const express = require("express");
const router = express.Router();
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');

const CONFIG = require('../config/app.config');
var setupaccount = fs.readFileSync(path.join(__dirname, '../templates/register.html'), 'utf8');
var forgotpassword = fs.readFileSync(path.join(__dirname, '../templates/forgotpassword.html'), 'utf8');

const bcrypt = require('bcrypt');
var generator = require('generate-password');
const jwt = require('jsonwebtoken');
const utils = require('../utils/index');

const expressJwt = require('express-jwt');
const ObjectId = require('mongodb').ObjectId;

// Node Mailer Function
const nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    secureConnection: true, // use SSL
    port: 587, // port for secure SMTP
    transportMethod: 'SMTP',
    auth: {
        user: CONFIG.SEMAIL_ID,
        pass: CONFIG.SEMAIL_PASS
    }
});

const User = require('../models/user');
const List = require('../models/listing');
const Zipcodes = require('../models/zipcodes');
const CMSPAGE = require('../models/cmspages');

// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
/* router.use(function (req, res, next) {
 
 // check header or url parameters or post parameters for token
 var token = req.body.token || req.param('token') || req.headers['x-access-token'];
 
 // decode token
 if (token) {
 
 // verifies secret and checks exp
 jwt.verify(token, CONFIG.JWT_SECRET, function (err, decoded) {
 if (err) {
 return res.json({
 success: false,
 payload: '',
 error:
 {
 code: 403,
 message: 'Failed to authenticate token.'
 }
 });
 }
 else {
 // if everything is good, save to request for use in other routes
 req.decoded = decoded;
 next();
 }
 });
 
 } else {
 
 // if there is no token
 // return an error
 return res.json({
 success: false,
 payload: '',
 error:
 {
 code: 403,
 message: 'No Token Provided'
 }
 });
 
 }
 
 }); */

//utility func
function isUserUnique(reqBody, cb) {
    var username = reqBody.username.toLowerCase() ? reqBody.username.trim() : '';
    var email = reqBody.email.toLowerCase() ? reqBody.email.trim() : '';

    User.findOne({
        $or: [{
                'username': new RegExp(["^", username, "$"].join(""), "i")
            }, {
                'email': new RegExp(["^", email, "$"].join(""), "i")
            }]
    }, function (err, user) {
        if (err)
            throw err;

        if (!user) {
            cb();
            return;
        }

        var msg;
        if (user.username === username) {
            msg = {};
            msg.username = '' + username + ' is already taken!';
        }
        if (user.email === email) {
            msg = msg ? msg : {};
            msg.email = '' + email + ' is already taken!';
        }

        cb(msg);
    });
}

// user signin api
router.post('/users/signin', function (req, res) {
    User
            .findOne({
                sub_admin: {$ne: true},
                $or: [{'email': req.body.username}, {'username': req.body.username}]
            })
            .select({
                __v: 0,
                updatedAt: 0,
                createdAt: 0
            }) //make sure to not return password (although it is hashed using bcrypt)
            .exec(function (err, user) {
                if (err)
                    throw err;

                if (!user) {
                    return res.json({
                        error: true,
                        message: 'Username/Email or Password is Wrong'
                    });
                }

                if (user.isEmailVerified === "true") {
                    if (user.active === false || user.active === "false") {
                        return res.json({
                            error: true,
                            message: 'User is disabled, contact with admin!'
                        });
                    } else {
                        bcrypt.compare(req.body.password.trim(), user.password, function (err, valid) {
                            if (!valid) {
                                return res.json({
                                    error: true,
                                    message: 'Username/Email or Password is Wrong!'
                                });
                            }

                            //make sure to NOT pass password and anything sensitive inside token
                            //Pass anything tht might be used in other parts of the app
                            var token = utils.generateToken(user);
                            user = utils.getCleanUser(user);

                            res.json({
                                user: user,
                                token: token
                            });
                        });
                    }
                } else {
                    return res.json({
                        error: true,
                        message: 'Please verify your mail id!'
                    });
                }

            });
});

// user signup api
router.post('/users/signup', function (req, res, next) {
    var body = req.body;

    var errors = utils.validateSignUpForm(body);
    if (errors) {
        return res.json(errors);
    }

    isUserUnique(body, function (err) {
        if (err) {
            return res.json({
                error: true,
                message: err
            });
        }
        var hash = bcrypt.hashSync(body.password.trim(), 10);
        var user = new User({
            username: body.username.trim(),
            firstname: body.firstname,
            lastname: body.lastname,
            email: body.email.trim(),
            password: hash,
            zipcode: body.zipcode.trim(),
            admin: false,
            isEmailVerified: false
        });


        user.save(function (err, user) {
            if (err)
                console.log(err);

            var token = utils.generateToken(user);
            user = utils.getCleanUser(user);

            // setup email data with unicode symbols
            let mailOptions = {
                from: 'brstdev12@gmail.com',
                to: user.email, // list of receivers
                subject: 'Account Created ?', // Subject line
                html: setupaccount.replace('USERNAME', user.username).replace('HREF', `href="${CONFIG.WEB_URL}/verify?email=${user.email}&code=${token}"`)
            };

            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error)
                    throw error
            });

            res.json({
                user: user,
                token: token
            });
        });

    });
});

// Check username
router.post('/user/checkUsername', (req, res) => {
    let body = req.body;

    isUserUnique(body, function (err) {
        if (err) {
            return res.json({
                error: true,
                message: err
            });
        }

        return res.send({
            error: false,
            message: "Username available!"
        });
    });
});

// get current user from token api
router.get('/me/token', function (req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['Authorization'];
    if (!token) {
        return res.json({
            message: 'Must pass token'
        });
    }

    // decode token
    jwt.verify(token, process.env.JWT_SECRET || CONFIG.JWT_SECRET, function (err, user) {
        if (err) {
            res.json({
                token: 'token_expired',
                msg: err
            });
            //throw err;
        } else {
            //return user using the id from w/in JWTToken
            User.findById({
                '_id': user._id
            }, function (err, user) {
                if (err)
                    throw err;

                user = utils.getCleanUser(user); //dont pass password and stuff

                //note: you can renew token by creating new token(i.e. refresh it) w/ new expiration time at this point, but I'm passing the old token back.
                // var token = utils.generateToken(user);
                res.json({
                    user: user,
                    token: token
                });

            });
        }
    });
});

// Update profile user api
router.post('/user/update', function (req, res) {
    let body = req.body;

    // Update query
    User.findOneAndUpdate({_id: body.id},
            {
                $set: {
                    "firstname": body.firstname,
                    "lastname": body.lastname,
                    "bio": body.bio,
                    "zipcode": body.zipcode,
                    "address1": body.address1,
                    "address2": body.address2,
                    "city": body.city,
                    "state": body.state,
                    "phone": body.phone,
                    "updatedAt": new Date()
                }
            }, {upsert: true, 'new': true}, function (err, user) {
        if (err) {
            res.send(err);
        } else {
            user = utils.getCleanUser(user);
            res.json({
                user,
                message: 'User updated!'
            });
        }
    });
});

// Upload user profile picture api
router.post('/user/profileAvatar', function (req, res) {
    var body = req.body;
    var imageFile = req.files.file;

    const dir = path.join(__dirname, '../../public/uploads/', body._id);

    //console.log(dir);
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    imageFile.mv(`${dir}/${body.filename}`, function (err) {
        if (err) {
            return res.send(err);
        }
        User.findById(body._id, function (err, user) {
            if (err) {
                res.send(err);
            }

            if (user) {
                user.profileImg = `uploads/${body._id}/${body.filename}`;
                user.save(function (err) {
                    if (err) {
                        res.send(err);
                    } else {
                        res.json({
                            user,
                            message: 'Image Uploaded Successfully!'
                        });
                    }
                });
            }
        });
    });
});

// Logout api 
router.post('/user/logout', function (req, res) {
    res.send("ok");
});

// Forget password api  
router.post('/user/forgetpassword', function (req, res) {
    let body = req.body;

    User
            .findOne({
                email: body.email
            })
            .select({
                __v: 0,
                updatedAt: 0,
                createdAt: 0
            })
            .exec(function (err, user) {
                if (err)
                    throw err;

                if (!user) {
                    return res.json({
                        error: true,
                        message: 'Email not found!'
                    });
                }

                // password generate and hash
                var newPassword = generator.generate({
                    length: 10,
                    numbers: true
                });
                var newPasshash = bcrypt.hashSync(newPassword.trim(), 10);

                let utilUpdate = User.update({'_id': user._id}, {$set: {'password': newPasshash}}, {multi: false}, function (err, user) {
                    if (err)
                        throw err

                    user = user;
                });

                if (utilUpdate) {
                    // setup email data with unicode symbols
                    let mailOptions = {
                        from: 'brstdev12@gmail.com',
                        to: user.email, // list of receivers
                        subject: 'Account Password Reset ?', // Subject line
                        html: forgotpassword.replace('NEWPASSWORD', newPassword)
                    };

                    smtpTransport.sendMail(mailOptions, function (error, response) {
                        if (error)
                            res.send(error)

                        res.json({
                            error: false,
                            message: "Mail has been sent!"
                        });
                    });
                }


            });

});

// Reset password api
router.post('/user/resetpassword', function (req, res) {
    let body = req.body;

    //return user using the id 
    User.findById({
        '_id': body.id
    }, function (err, user) {
        if (err)
            throw err;

        bcrypt.compare(req.body.password.trim(), user.password, function (err, valid) {
            if (!valid) {
                return res.json({
                    error: true,
                    message: 'Old Password is Wrong!'
                });
            }


            var newPasshash = bcrypt.hashSync(body.newPassword.trim(), 10);

            let utilUpdate = User.update({'_id': user._id}, {$set: {'password': newPasshash}}, {multi: false}, function (err, user) {
                if (err)
                    throw err

                res.json({
                    error: false,
                    message: "Password Changed Successfully!"
                });
            });
        });

    });

});

// Email Verify api
router.post('/verify', function (req, res) {
    let body = req.body;

    let email = req.query.email;
    let code = req.query.code;

    // decode token
    jwt.verify(code, process.env.JWT_SECRET || 'abc', function (err, user) {
        if (err)
            throw err;

        if (user.isEmailVerified == "true") {
            res.json({
                code: 401,
                message: 'Email Already Verified!'
            });
        } else {
            User.findOne({
                email: email, isEmailVerified: 'true'
            }).exec(function (err, db_user) {
                if (db_user) {
                    return res.json({
                        code: 401,
                        message: 'Email Already Verified!'
                    });
                } else {
                    User.findOneAndUpdate({_id: user._id},
                            {
                                $set: {
                                    "isEmailVerified": "true",
                                }
                            }, {upsert: true, 'new': true}, function (err, user) {
                        if (err) {
                            res.send(err);
                        } else {
                            /*Start verfied login New code*/
                            User
                                    .findOne({
                                        sub_admin: {$ne: true},
                                        $or: [{'email': email}]
                                    })
                                    .select({
                                        __v: 0,
                                        updatedAt: 0,
                                        createdAt: 0
                                    }) //make sure to not return password (although it is hashed using bcrypt)
                                    .exec(function (err, user) {
                                        if (err)
                                            throw err;

                                        if (!user) {
                                            return res.json({
                                                error: true,
                                                message: 'Username/Email or Password is Wrong'
                                            });
                                        }
                                        if (user.isEmailVerified === "true") {
                                            if (user.active === false || user.active === "false") {
                                                return res.json({
                                                    error: true,
                                                    message: 'User is disabled, contact with admin!'
                                                });
                                            } else {
                                                //make sure to NOT pass password and anything sensitive inside token
                                                //Pass anything tht might be used in other parts of the app
                                                var token = utils.generateToken(user);
                                                user = utils.getCleanUser(user);

                                                res.json({
                                                    user: user,
                                                    token: token,
                                                    code: 200,
                                                    message: 'Email is verified!'
                                                });
                                            }
                                        } else {
                                            return res.json({
                                                error: true,
                                                message: 'Please verify your mail id!'
                                            });
                                        }
                                    });
                            /*end verfied login New code*/
                        }
                    });
                }
            });
        }
    });
});

router.post('/getUserByID', (req, res) => {
    let _id = req.query.id;

    if (!_id) {
        return res.send({
            error: true,
            message: "id not found!"
        });
    }


    User.findById({
        '_id': _id
    }, function (err, user) {
        if (err)
            throw err;

        user = utils.getCleanUser(user); //dont pass password and stuff

        res.json({
            user: user
        });

    });

});

router.post('/getUserByIDCloseJob', (req, res) => {
    let _id = req.query.id;

    if (!_id) {
        return res.send({
            error: true,
            message: "id not found!"
        });
    }


    List.aggregate([
        {
            $match: {
                'hiredUserId': ObjectId(_id),
                'status': 'CLOSE'
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "hiredUserId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        {"$unwind": "$userInfo"},
        {
            $lookup: {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "clientInfo"
            }
        },
        {"$unwind": "$clientInfo"},
    ], function (err, lists) {
        if (err)
            throw err;

        res.send(lists);

    });

});

router.post('/getzip_keywords', function (req, res) {
    Zipcodes.find().sort({'createdAt': 1})
            .exec(function (err, docs) {
                if (err) {
                    return res.status(200).json({success: false});
                } else {
                    return res.json({
                        data: docs
                    });
                }
            });
});

router.post('/getUserReviews', function (req, res) {
    let _id = req.body.id;
    List.aggregate([
        {
            $match: {
                'hiredUserId': ObjectId(_id),
                'status': 'CLOSE'
            },
        },
        {$group: {_id: null, myCount: {$sum: 1}}},
    ], function (err, reviews_data) {
        res.send(reviews_data)
    });
});


router.post('/getCmsPageData', function (req, res) {
    CMSPAGE.find().sort([['order', 1]]).exec(function (err, data) {
        if (err)
            return res.status(200).json({success: false});

        return res.json({
            data: data
        })

    });
});

module.exports = router;

