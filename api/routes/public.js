const express = require("express");
const router = express.Router();

const path = require('path');
const fs = require('fs');

router.get('/getAllCountries', (req, res) => {
    let rawdata = fs.readFileSync(path.join(__dirname, '/data/countries.json'));
    let countries = JSON.parse(rawdata);  

    res.json(countries);  
});

router.get('/getStateById', (req, res) => {
    let id = req.params.id;

    let rawdata = fs.readFileSync(path.join(__dirname, '/data/states.json'));
    let states = JSON.parse(rawdata);  

    res.send(states);  

    // if(states.id == id) {
    //     res.json(states);  
    // }
});


router.get('/getCityById/:id', (req, res) => {
    let id = req.params.id;

    let rawdata = fs.readFileSync(path.join(__dirname, '/data/cities.json'));
    let states = JSON.parse(rawdata);  

    res.json(states);  
});

module.exports = router;