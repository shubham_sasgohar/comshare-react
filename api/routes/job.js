const express = require("express");
const router = express.Router();
const jobContoller = require('../controllers/job');

router.post('/job/getPostedJob', jobContoller.getPostedJOb);
router.post('/job/getAppliedJob', jobContoller.getAppliedJOb);
router.post('/job/getAwardedJob', jobContoller.getAwardedJob);
router.post('/job/getUserCompletedJob', jobContoller.getUserCompletedJob);
router.post('/job/getClientCompletedJob', jobContoller.getClientCompletedJob);

module.exports =  router;