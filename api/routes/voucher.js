const express = require("express");
const router = express.Router();
const path = require('path');
const Voucher = require('../models/voucher');
const Voucheruser = require('../models/voucheruser');
const Payments = require('../models/payments');
const session = require('express-session');
var cc = require('coupon-code');
const fs = require('fs');

var couponTemplate = fs.readFileSync(path.join(__dirname, '../templates/coupon.html'), 'utf8');

const CONFIG = require('../config/app.config');

// Node Mailer Function
const nodemailer = require('nodemailer');
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    secureConnection: true, // use SSL
    port: 587, // port for secure SMTP
    transportMethod: 'SMTP',
    auth: {
        user: CONFIG.SEMAIL_ID,
        pass: CONFIG.SEMAIL_PASS
    }
});

// Get msg api
const User = require('../models/user');

router.post('/voucher/getvouchers', function (req, res) {
    let query = req.body.query;
    console.log(query)
    if (query == '' || query == undefined) {
        Voucher.aggregate(
                [
                    {
                        $match: {active: true}
                    }
                ]
                )
                .exec(function (err, data) {
                    if (err) {
                        return res.status(200).json({success: false});
                    } else {
                        return res.json({
                            data: data
                        });
                    }
                });
    } else {
        Voucher.aggregate(
                [
                    {
                        $match: {
                            active: true,
                            $or: [
                                {name: {$regex: query, $options: 'i'}},
                                {zipcode: {$regex: query, $options: 'i'}}
                            ]

                        }
                    }
                ]
                )
                .exec(function (err, data) {
                    if (err) {
                        return res.status(200).json({success: false});
                    } else {
                        return res.json({
                            data: data
                        });
                    }
                });
    }


});
router.post('/voucher/getCouponRedeemed', function (req, res) {

    Voucheruser.aggregate(
            [
                {
                    $match: {
                        code: req.body.code
                    }

                },
                {
                    $lookup:
                            {
                                from: "users",
                                localField: "userId",
                                foreignField: "_id",
                                as: "userinfo"
                            }
                },
                {"$unwind": "$userinfo"},
                {
                    $lookup:
                            {
                                from: "vouchers",
                                localField: "voucherId",
                                foreignField: "_id",
                                as: "voucherInfo"
                            }
                },
                {"$unwind": "$voucherInfo"},
            ],
            )
            .exec(function (err, data) {
                if (err) {
                    return res.status(200).json({success: false});
                } else {

                    if (data.length > 0) {
                        let curr_date = new Date();
                        let end_date = data[0]['voucherInfo'].endDate;
                        let start_date = data[0]['voucherInfo'].startDate;
                        if (curr_date >= start_date && curr_date <= end_date) {
                            data[0]["valid_status"] = "Valid";
                        } else {
                            data[0]["valid_status"] = "Not Valid";
                        }
                    }
                    return res.json({
                        data: data
                    });
                }
            });

});

router.post('/voucher/redeemvoucher', function (req, res) {
    let coupon_code = cc.generate();
    let voucher_id = req.body.voucher_id;
    let userid = req.body.userid;
    let email = req.body.email;
    let vouchername = req.body.vouchername;
    let voucher_avail = req.body.voucher_avail;
    let points = req.body.points;
    let parse_points = parseInt(points);


    var newvoucher = new Voucheruser({
        voucherId: voucher_id,
        userId: userid,
        code: coupon_code,
    });

    newvoucher.save(function (err, doc_update) {
        if (err)
            return res.send(err);
        
        User.update(
            {_id: userid},
            {$inc: {coins: -parse_points}},
            function (err, doc) {
                if (err) {
                    return err;
                }
        });
        
        let mailOptions = {
            from: 'brstdev12@gmail.com',
            to: email, // list of receivers
            subject: `Coupon Notification`, // Subject line
            html: couponTemplate.replace('CODE', coupon_code).replace('COUPON',vouchername).replace('VOUCHERDATE',voucher_avail)
        };

        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error)
                throw error;
        });
        res.send(doc_update);
    });
});



router.post('/voucher/changeRedeemStatus', function (req, res) {
    let code = req.body.code;
    console.log(code)
    Voucheruser.updateOne({
        code: code,
    },
            {
                $set: {
                    redeemedStatus: true,
                }
            }, (err, doc_update) => {
        if (err)
            return err;

        res.send(doc_update);
    });
});

router.post('/voucher/SavePayment', function (req, res) {


    var payment = new Payments({
        userId: req.body.userId,
        paid: req.body.paid,
        cancel: req.body.cancelled,
        payerId: req.body.payerID,
        paymentId: req.body.paymentID,
        paymentToken: req.body.paymentToken,
        payerEmail: req.body.payeremail,
        amount: req.body.amount

    });

    payment.save(function (err, doc_update) {
        if (err)
            return res.send(err);
        
        let total_coins = req.body.usercoin + req.body.amount; 
        User.findOneAndUpdate({_id: req.body.userId},
                {
                    $set: {
                        "coins": total_coins,
                    }
                }, function (err, user) {
            if (err) {
                res.send(err);
            } else {
                res.send(doc_update);
            }
        });
        
    });


});




module.exports = router;