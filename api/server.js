var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var cors = require('cors');
var db = require('./config/db');

var passport = require('passport');
var flash    = require('connect-flash');
var session = require('express-session');

const fileUpload = require('express-fileupload');
const port = process.env.PORT || 5013;

// Swagger Api
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

// routes
var users = require('./routes/users');
var listing = require('./routes/listing');
var public = require('./routes/public');
var job = require('./routes/job');
var chat = require('./routes/chat');
var voucher = require('./routes/voucher');

require('./config/passport')(passport);

// express
var app = express();

// template engine
var ejs = require("ejs");

/*Starts Socket*/

var createserver = require('http').createServer(app)
var io = module.exports.io = require('socket.io')(createserver)
const SocketManager = require('./SocketManager')
io.on('connection', SocketManager) 

/*End Socket*/

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Request-Headers", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(cors());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, './public')));

// midlleware for template engine
app.set('view engine', 'ejs');

// required for passport
app.use(session({ secret: 'supersecret' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());


app.use('/api', users);
app.use('/api', listing);
app.use('/api', job);
app.use('/pub', public);
app.use('/api', chat);
app.use('/api', voucher);
// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// error handlers
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//     console.dir(err);
//     res.status(err.status || 500);
//     if (err.status === 500) {
//       console.error(err.stack);
//       res.json({
//         error: 'Internal Server Error'
//       });
//     } else if (err.status === 404) {
//       res.send('error'); //render error page
//     } else {
//       res.json({
//         error: err.message
//       })
//     }
// });


require('./routes/admin')(app, passport);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


createserver.listen(port, () => {
    console.log(`App is running on port ${port}`);
});
//app.listen(port, () => {
//    console.log(`App is running on port ${port}`);
//});

  